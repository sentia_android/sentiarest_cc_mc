/*
package com.sentiacare.entities;

public class TransactionAudit {

    public final String requestId;
    public final String fileStatus;
    public final String dbStatus;
    public final String messageStatus;
    public final String comment;
    public final String logDateTime;

    public TransactionAudit(String requestId, String fileStatus, String messageStatus, String comment, String logDateTime) {
        this.requestId = requestId;
        this.fileStatus = fileStatus;
        this.dbStatus = null;
        this.messageStatus = messageStatus;
        this.comment = comment;
        this.logDateTime = logDateTime;
    }

    public TransactionAudit(String requestId, DBStatus dbStatus, String messageStatus, String comment, String logDateTime) {
        this.requestId = requestId;
        this.fileStatus = null;
        this.dbStatus = dbStatus == null ? null : dbStatus.toString();
        this.messageStatus = messageStatus;
        this.comment = comment;
        this.logDateTime = logDateTime;
    }

    public TransactionAudit(String requestId, FileStatus fs, DBStatus dbStatus, String messageStatus, String comment, String logDateTime) {
        this.requestId = requestId;
        this.fileStatus = fs == null ? null : fs.toString();
        this.dbStatus = dbStatus == null ? null : dbStatus.toString();
        this.messageStatus = messageStatus;
        this.comment = comment;
        this.logDateTime = logDateTime;
    }
}
*/
