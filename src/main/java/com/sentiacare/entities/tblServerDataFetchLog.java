package com.sentiacare.entities;

public class tblServerDataFetchLog {
    private String Id;
    private String userId;
    private String userType;
    private String requestId;
    private String requestSentAt;
    private String requestReceivedAt;
    private String restMethodName;
    private String fileName;
    private String fileSize;
    private String checkSum;
    private String fileGeneratedAt;
    private String fileCompletedAt;
    private String status;
    private String comments;
    private String lastFetchdateRequest;

    public String getLastFetchdateRequest() {
        return lastFetchdateRequest;
    }

    public void setLastFetchdateRequest(String lastFetchdateRequest) {
        this.lastFetchdateRequest = lastFetchdateRequest;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestSentAt() {
        return requestSentAt;
    }

    public void setRequestSentAt(String requestSentAt) {
        this.requestSentAt = requestSentAt;
    }

    public String getRequestReceivedAt() {
        return requestReceivedAt;
    }

    public void setRequestReceivedAt(String requestReceivedAt) {
        this.requestReceivedAt = requestReceivedAt;
    }

    public String getRestMethodName() {
        return restMethodName;
    }

    public void setRestMethodName(String restMethodName) {
        this.restMethodName = restMethodName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileGeneratedAt() {
        return fileGeneratedAt;
    }

    public void setFileGeneratedAt(String fileGeneratedAt) {
        this.fileGeneratedAt = fileGeneratedAt;
    }

    public String getFileCompletedAt() {
        return fileCompletedAt;
    }

    public void setFileCompletedAt(String fileCompletedAt) {
        this.fileCompletedAt = fileCompletedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }
}
