package com.sentiacare.entities;

import java.sql.Timestamp;

public class ServiceLog {

    public String userId;
    public String requestId;
    public Timestamp requestSentAt;
    public Timestamp requestReceivedAt;
    public String transStatus;
    public Timestamp transAt;
    public String restMethod;

}
