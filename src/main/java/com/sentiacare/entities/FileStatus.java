package com.sentiacare.entities;

public enum FileStatus {
    Decryption, FoundRequestId, Validation, SaveToFolder, NoDatabase, NA
}
