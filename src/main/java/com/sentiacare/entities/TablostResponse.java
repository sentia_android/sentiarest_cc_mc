package com.sentiacare.entities;

import com.sentiacare.dtos.FetchResponse;

public class TablostResponse {
    String msg;
    boolean isError;
    boolean isTabLostStatus;
    FetchResponse fetchResponse;

    public TablostResponse(String msg, boolean isError, boolean isTabLostStatus, FetchResponse fetchResponse) {
        this.msg = msg;
        this.isError = isError;
        this.isTabLostStatus = isTabLostStatus;
        this.fetchResponse = fetchResponse;
    }
}
