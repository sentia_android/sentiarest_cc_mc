package com.sentiacare.entities;

public enum StatementType {

    Insert, Update, Delete
}
