package com.sentiacare.entities;

public class BatchItem {

    public String statement;
   // public int updateCount;
    public StatementType statementType;

    public BatchItem(String statement, StatementType type) {
        this.statement = statement;
        this.statementType = type;
    }
}
