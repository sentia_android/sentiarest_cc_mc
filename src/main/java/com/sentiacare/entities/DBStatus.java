package com.sentiacare.entities;

public enum DBStatus {
    ConnectionOpen,
    TransactionCommit,
    TransactionRollback,
    ConnectionClose,
    NA,
    TransactionBegin,
    Insert,
    Update,
    Delete
}
