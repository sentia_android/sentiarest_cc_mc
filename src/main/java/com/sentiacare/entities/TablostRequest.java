package com.sentiacare.entities;

public class TablostRequest {
    public String userId;
    public String mobileNo;
    public String masterDB;
    public String requestId;
    public String userType;
    public String clientDB;
    public String sendAt;

    public String getSendAt() {
        return sendAt;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientDB() {
        return clientDB;
    }

    public void setClientDB(String clientDB) {
        this.clientDB = clientDB;
    }

    public String getUserId() {
        return userId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getMasterDB() {
        return masterDB;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getUserType() {
        return userType;
    }
}
