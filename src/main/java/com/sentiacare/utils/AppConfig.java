package com.sentiacare.utils;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppConfig {
    public String WebServerXSDFilePath;
    public String WebServerFolderName;
    public String WebServerProcessedFolderName;
    public String WebServerUnprocessedFolderName;
    public String WebServerInputFolderName;
    public String WebServerUploaded; //1-6-2021
    public String dbIp, dbUser, dbPassword;
    public String secretKey;
    public String logUploadsDirectory;
    public int javalinPort;

    public String version;
    public String builtOn;

    public String archivalSchemaName;

    public String unzipDirectory;
    public String exportDirectory;

    public boolean isTestingInternal; //21Jun2022 Bindu - Check for testing option - for data fetch - if testing - then retrieve all rec else prev midnight
    public int connTimeOut ;
    public int maxPoolSize;
    public int maxLifeTime;
    public int minimum_idle;
    public int validation_timeout;
    public int idle_timeout;

    public String syncDirectory;//3-Sep-2022 Ramesh


    public String currDbName;
    public boolean batchProcess;//23Jan2023 Arpitha
    public String achfActivityJsonFileDirectory; //18-July-2023 Rakesh
    public String WebServerActivityCalendarFilePath;//18Aug2023-Rakesh

    public void loadAppConfig()throws IOException {
        String propertyFileLocation = System.getenv("bcpcConfig");
        if(propertyFileLocation == null || propertyFileLocation.isEmpty()) {
            throw new RuntimeException("Environment variable bcpcConfig could not be found");
        }
        InputStream is = new FileInputStream(propertyFileLocation);
        Properties properties = new Properties();
        properties.load(is);

        // XSD Location
        WebServerXSDFilePath = properties.getProperty("WebServerXSDFilePath");

        // Directoriesmvn
        WebServerFolderName = properties.getProperty("WebServerFolderName");
        WebServerProcessedFolderName = properties.getProperty("WebServerProcessedFolderName");
        WebServerUnprocessedFolderName = properties.getProperty("WebServerUnprocessedFolderName");
        WebServerInputFolderName = properties.getProperty("WebServerInputFolderName");
        WebServerUploaded = properties.getProperty("WebServerUploaded");
        logUploadsDirectory = properties.getProperty("logUploadsDirectory");
        exportDirectory = properties.getProperty("exportDirectory");

        // Database Connection Parameters
        dbIp = properties.getProperty("dbIp");
        dbUser = properties.getProperty("dbUser");
        dbPassword = properties.getProperty("dbPassword");

        // Request XML Decryption Key
        secretKey = properties.getProperty("SecurityKey");

        // Javalin
        javalinPort = Integer.parseInt(properties.getProperty("javalinPort"));

        // API
        version = properties.getProperty("version");
        builtOn = properties.getProperty("builtOn");

        archivalSchemaName = properties.getProperty("archivalSchemaName");

        unzipDirectory = properties.getProperty("unzipDirectory");

        //13Jun2022 Bindu
        connTimeOut = Integer.parseInt(properties.getProperty("connTimeOut"));
        maxPoolSize = Integer.parseInt(properties.getProperty("maxPoolSize"));
        maxLifeTime = Integer.parseInt(properties.getProperty("maxLifeTime"));
        minimum_idle = Integer.parseInt(properties.getProperty("minimum_idle"));
        validation_timeout = Integer.parseInt(properties.getProperty("validation_timeout"));
        idle_timeout = Integer.parseInt(properties.getProperty("idle_timeout"));
        //21Jun2022 Bindu
        isTestingInternal = Boolean.parseBoolean(properties.getProperty("isTestingInternal"));

        syncDirectory = properties.getProperty("syncDirectory");
        currDbName = properties.getProperty("currDbName");
        batchProcess = Boolean.parseBoolean(properties.getProperty("batchProcess"));//23Jan2023 Arpitha
        achfActivityJsonFileDirectory = properties.getProperty("achfActivityJsonFileDirectory");//18July2023 Rakesh
        WebServerActivityCalendarFilePath = properties.getProperty("WebServerActivityCalendarFilePath");//18Aug2023-Rakesh
    }
}


/*
public class AppConfig {
    public String WebServLogFileName;
    public String WebServerXSDFilePath;
    public String WebServerFolderName;
    public String WebServerProcessedFolderName;
    public String WebServerUnprocessedFolderName;
    public String WebServerInputFolderName;
    public String WebServerUploaded; //1-6-2021
    public String dbIp, dbUser, dbPassword;
    public String secretKey;
    public String logUploadsDirectory;
    public int javalinPort;

    public String version;
    public String builtOn;

    public String exportDirectory;

    public String unzipDirectory;


    public void loadAppConfig()throws IOException {
        String propertyFileName = "appconfig.properties";
        InputStream is = getClass().getClassLoader().getResourceAsStream(propertyFileName);
        Properties properties = new Properties();
        if(is != null) {
            properties.load(is);
            WebServLogFileName = properties.getProperty("WebServLogFileName");
            WebServerXSDFilePath = properties.getProperty("WebServerXSDFilePath");
            WebServerFolderName = properties.getProperty("WebServerFolderName");
            WebServerProcessedFolderName = properties.getProperty("WebServerProcessedFolderName");
            WebServerUnprocessedFolderName = properties.getProperty("WebServerUnprocessedFolderName");
            WebServerInputFolderName = properties.getProperty("WebServerInputFolderName");
            WebServerUploaded = properties.getProperty("WebServerUploaded");
            dbIp = properties.getProperty("dbIp");
            dbUser = properties.getProperty("dbUser");
            dbPassword = properties.getProperty("dbPassword");
            secretKey = properties.getProperty("SecurityKey");
            logUploadsDirectory = properties.getProperty("logUploadsDirectory");
            javalinPort = Integer.parseInt(properties.getProperty("javalinPort"));
            version = properties.getProperty("version");
            builtOn = properties.getProperty("builtOn");
            exportDirectory = properties.getProperty("exportDirectory");
        } else {
            throw new FileNotFoundException("Property file " + propertyFileName + " not found");
        }

    }
}
*/
