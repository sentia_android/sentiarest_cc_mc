package com.sentiacare.utils;

import com.sentiacare.domain.SentiatendException;
import com.sentiacare.entities.FileStatus;
import com.sentiacare.entities.MessageStatus;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class FileUtils {

    public static File saveToFolder(String decryptedXML, String filename, String folderName) throws IOException {
        File f = new File(folderName + File.separator + filename + ".xml");
        FileWriter fw = new FileWriter(f);
        fw.write(decryptedXML);
        fw.flush();
        fw.close();
        return f;
    }

    public static Path saveToFolder2(String xml, String filename, String folderName) throws SentiatendException {
        try {
            return Files.write(Paths.get(folderName, filename + ".xml"),
                    xml.getBytes(StandardCharsets.UTF_8),
                    new StandardOpenOption[]{StandardOpenOption.CREATE_NEW});
        } catch (IOException e) {
            throw new SentiatendException(e.getMessage(), FileStatus.SaveToFolder, MessageStatus.Failure, String.format("Could not save xml file %s", filename), e);
        }
    }

    public static void move(Path source, boolean processed, Path target) throws SentiatendException {
        try {
            Files.move(source, target.resolve(source.getFileName()), REPLACE_EXISTING);
        } catch (IOException e) {
            throw new SentiatendException(String.format("File %s could not be moved to %s folder", source, processed ? "processed" : "unprocessed"), e);
        }
    }
}
