package com.sentiacare.utils;

import com.sentiacare.domain.SentiatendException;
import com.sentiacare.dtos.ResponseMessage;
import com.sentiacare.dtos.SyncParams;
import com.sentiacare.dtos.TableResult;
import com.sentiacare.entities.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class XMLUtils2 {

    private static final Logger LOGGER = LogManager.getLogger(XMLUtils.class);

    public static String serializeToString(Object obj) throws SentiatendException {
        try {
            StringWriter sw = new StringWriter();
            Marshaller jaxbMarshaller = XMLUtils.getMarshaller(ResponseMessage.class);
            jaxbMarshaller.marshal(obj, sw);
            return sw.toString();
        } catch (JAXBException e) {
            throw new SentiatendException(String.format("Could not serialize response message to xml. ... Cause: %s ##", e.getMessage()), e);
        }
    }

    public static TableResult GetResponseXMLForATran(String TranId, String TranAction, String Comment,
                                                     FileStatus fileStatus, DBStatus dBStatus, MessageStatus msgStatus, boolean isDbExists) {

        TableResult tabRes = new TableResult();
        tabRes.TranAction = TranAction;
        tabRes.TranId = TranId;
        tabRes.Comment = Comment;
        tabRes.DBStatus = dBStatus == null ? "" : dBStatus.name();
        tabRes.FileStatus = fileStatus == null ? "" : fileStatus.name();
        tabRes.Status = msgStatus == null ? "" : msgStatus.name();
        tabRes.DbExists = isDbExists;

        return tabRes;
    }

    public static SyncParams extract(String xml) throws SentiatendException {
        String requestId = "", dbname = "";
        try {
            XMLInputFactory xmlInputFactory = XMLUtils.getXMLInputFactory();
            XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(new StringReader(xml));
            while (xmlStreamReader.hasNext()) {
                int xmlEvent = xmlStreamReader.next();
                if (xmlEvent == XMLEvent.START_ELEMENT) {
                    String elementName = xmlStreamReader.getLocalName();
                    if (elementName.equalsIgnoreCase("requestid")) {
                        requestId = xmlStreamReader.getElementText();
                    }
                    if (elementName.equalsIgnoreCase("databaseid")) {
                        dbname = xmlStreamReader.getElementText();
                    }
                }
                if ((requestId != null && !requestId.isEmpty()) && (!dbname.isEmpty())) {
                    break;
                }
            }
        } catch (XMLStreamException e) {
            throw new SentiatendException(String.format("Sorry! there was an error encountered while reading xml ... Cause: %s ##", e.getMessage()), e);
        }
        return new SyncParams(requestId, dbname);
    }

    public static String responseXML(String requestId,
                                     boolean dbExists,
                                     String requestStatus,
                                     FileStatus fileStatus,
                                     MessageStatus messageStatus,
                                     String errorMessage) throws SentiatendException {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.RequestId = requestId;
        responseMessage.DBExists = dbExists;
        responseMessage.TableResults = new ArrayList<>();

        TableResult r = GetResponseXMLForATran("", "", errorMessage + requestStatus,
                fileStatus, DBStatus.NA, messageStatus, dbExists);
        responseMessage.TableResults.add(r);
        return serializeToString(responseMessage);
    }

    public static String responseXML2(String requestId,
                                      boolean dbExists,
                                      DBStatus dbStatus, String comment, MessageStatus msgStatus) throws SentiatendException {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.RequestId = requestId;
        responseMessage.DBExists = dbExists;
        responseMessage.TableResults = new ArrayList<>();

        TableResult r = GetResponseXMLForATran("", "", comment,
                FileStatus.NA, dbStatus, msgStatus, dbExists);
        responseMessage.TableResults.add(r);
        return serializeToString(responseMessage);
    }

    public static List<BatchItem> extractStatementsOrThrow(File file) throws SentiatendException {
        List<BatchItem> statementsToRun = new ArrayList<>();
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource(new FileInputStream(file));
            Document doc = documentBuilder.parse(is);
            NodeList tableNameNodes = doc.getElementsByTagName("tablename");
            for (int tableIndex = 0; tableIndex < tableNameNodes.getLength(); tableIndex++) {
                Node tableNameNode = tableNameNodes.item(tableIndex);
                Element tableNameParent = (Element) tableNameNode.getParentNode();

                String sqlAction = tableNameParent.getElementsByTagName("action").item(0).getTextContent().trim();
                switch (sqlAction) {
                    case "I": {
                        String sQuery = "Insert into " + tableNameNode.getTextContent();
                        sQuery = sQuery + " (";
                        String columnValue = " (";

                        NodeList childNodes = tableNameParent.getChildNodes();
                        for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++) {
                            Node childNode1 = childNodes.item(childIndex);
                            if (childNode1.getNodeType() == Node.ELEMENT_NODE) {
                                if (!childNode1.getNodeName().equals("tablename") && !childNode1.getNodeName().equals("action")) {
                                    sQuery += childNode1.getNodeName() + ",";
                                    columnValue = columnValue + "'" + childNode1.getTextContent() + "',";

                                }
                            }
                        }
                        sQuery = sQuery.substring(0, sQuery.length() - 1);
                        columnValue = columnValue.substring(0, columnValue.length() - 1);
                        sQuery = sQuery + ") values " + columnValue + ")";

                        statementsToRun.add(new BatchItem(sQuery, StatementType.Insert));
                        break;
                    }
                    case "U": {
                        String s = tableNameParent.getElementsByTagName("SQLSTATEMENT").item(0).getTextContent();

                        statementsToRun.add(new BatchItem(s, StatementType.Update));
                        break;
                    }
                }
            }
            return statementsToRun;
        } catch (ParserConfigurationException | SAXException | IOException e) {
            String message = String.format("Could not prepare DML statement in File %s ", file.getPath());
            LOGGER.error(message, e);
            throw new SentiatendException(message, e);
        }
    }

    public static void throwIfNotValid(String xml, Schema schema) throws SentiatendException {
        try {
            schema.newValidator().validate(new StreamSource(new StringReader(xml)));
        } catch (SAXException | IOException e) {
            throw new SentiatendException(e.getMessage(), FileStatus.Validation, MessageStatus.Failure, "Could not validate xml", e);
        }
    }
}
