package com.sentiacare.utils;

import com.sentiacare.dtos.TableResult;
import com.sentiacare.entities.DBStatus;
import com.sentiacare.entities.FileStatus;
import com.sentiacare.entities.MessageStatus;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

public class XMLUtils {
    public static Validator getValidator(String xsdPath) throws SAXException {
        SchemaFactory factory =
                SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(new File(xsdPath));
        return schema.newValidator();
    }


    public static void throwIfNotValid(String xml, Validator validator) throws SAXException, IOException {
        validator.validate(new StreamSource(new StringReader(xml)));
    }

    public static Marshaller getMarshaller(Class<?> c) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(c);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
        return jaxbMarshaller;
    }

    public static String serializeToString(Object obj, Marshaller jaxbMarshaller) throws JAXBException {
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(obj, sw);
        return sw.toString();
    }

    public static XMLInputFactory getXMLInputFactory() {
        return XMLInputFactory.newInstance();
    }

    public static TableResult GetResponseXMLForATran(String TranId, String TranAction, String Comment,
                                                     FileStatus fileStatus, DBStatus dBStatus, MessageStatus msgStatus, boolean isDbExists) {

        TableResult tabRes = new TableResult();
        tabRes.TranAction = TranAction;
        tabRes.TranId = TranId;
        tabRes.Comment = Comment;
        tabRes.DBStatus = dBStatus == null ? "" : dBStatus.name();
        tabRes.FileStatus = fileStatus == null ? "" : fileStatus.name();
        tabRes.Status = msgStatus == null ? "" : msgStatus.name();
        tabRes.DbExists = isDbExists;

        return tabRes;
    }

}
