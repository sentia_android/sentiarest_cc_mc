package com.sentiacare.utils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class CryptoUtils {
    public static String decrypt(String base64EncodedCipher, String secretKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {
        String xml = "";

        byte[] cipherAsBytes = Base64.getDecoder().decode(base64EncodedCipher.replaceAll("\\n", "").replaceAll("\\r", ""));
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] messageDigest = md.digest(secretKey.getBytes(StandardCharsets.UTF_8));
        md.reset();
        SecretKey myKey = new SecretKeySpec(messageDigest,"AES");

        cipher.init(Cipher.DECRYPT_MODE, myKey);
        byte[] decryptedPlainText = cipher.doFinal(cipherAsBytes);
        xml = new String(decryptedPlainText, StandardCharsets.UTF_8);
        return xml;
    }
}
