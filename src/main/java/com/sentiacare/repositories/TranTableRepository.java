/*
package com.sentiacare.repositories;

import com.sentiacare.domain.SentiatendException;
import com.sentiacare.entities.*;
import com.sentiacare.services.DBOpsStatus;
import com.sentiacare.utils.XMLUtils2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.List;

public class TranTableRepository {

    private static final Logger LOGGER = LogManager.getLogger(TranTableRepository.class);

    private final SentiaPooledDataSource ds;

    public TranTableRepository(SentiaPooledDataSource ds) {
        this.ds = ds;
    }

    public void throwIfRequestIdExists(String requestId, String schemaName) throws SentiatendException {
        try (Connection c = ds.getConnection(schemaName)) {
            PreparedStatement ps = c.prepareStatement("SELECT 1 FROM trantable WHERE requestid = ? LIMIT 1");
            ps.setString(1, requestId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int one = rs.getInt(1);
                if (one == 1) {
                    throw new SentiatendException(XMLUtils2.responseXML(requestId,
                            true,
                            "",
                            FileStatus.FoundRequestId,
                            MessageStatus.Failure,
                            "Request Id Already Found"),
                            FileStatus.FoundRequestId,
                            MessageStatus.Failure,
                            "Request Id Already Found");
                }
            }
        } catch (SQLException e) {
            throw new SentiatendException(e.getMessage(), e);
        }
    }

    public synchronized DBOpsStatus doBatch(List<BatchItem> batchItems, String requestId, String dbName) throws SentiatendException {
        if (batchItems.isEmpty()) {
            throw new SentiatendException(String.format("No insert/update statements found in xml. Request Id %s - Schema name %s", requestId, dbName));
        }
        Connection connection = null;
        Statement statement = null;
        boolean canCommit = true;
        int[] updateCounts = new int[0];
        try {
            connection = ds.getConnection(dbName);
            connection.setAutoCommit(false);

            statement = connection.createStatement();
            for (BatchItem batchItem : batchItems) {
                statement.addBatch(batchItem.statement);
            }
            try {
                updateCounts = statement.executeBatch();
            } catch (BatchUpdateException e) {
                LOGGER.error(e.getMessage(), e);
                canCommit = false;
                updateCounts = e.getUpdateCounts();
            }
            if (canCommit) {
                connection.commit();
            } else {
                connection.rollback();
            }
            return new DBOpsStatus(canCommit ? DBStatus.TransactionCommit : DBStatus.TransactionRollback, canCommit);
        } catch (SQLException ex) {
            throw new SentiatendException(String.format("Could not get connection to schema %s", dbName), ex);
        } finally {
            try {
                if (statement != null) {
                    statement.clearBatch();
                }
                if (connection != null) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
                new Thread(new DoTheRest2(batchItems, requestId, canCommit, updateCounts)).start();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
    }

    public void saveEvents(List<TransactionAudit> audits, String dbname, String requestId) throws SentiatendException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ds.getConnection(dbname);
            c.setAutoCommit(false);
            ps = c.prepareStatement("INSERT INTO trantable " +
                    "(requestid, comment, logdatetime, filestatus, dbstatus, msgstatus) " +
                    "values (?,?,?,?,?,?)");
            for (TransactionAudit a : audits) {
                ps.setString(1, a.requestId);
                ps.setString(2, a.comment);
                ps.setString(3, a.logDateTime);
                ps.setString(4, a.fileStatus);
                ps.setString(5, a.dbStatus);
                ps.setString(6, a.messageStatus);

                ps.addBatch();
            }

            ps.executeBatch();
            c.commit();
        } catch (SQLException e) {
            throw new SentiatendException(String.format("Could not save events for request id %s", requestId), e);
        } finally {
            try {
                if (ps != null) {
                    ps.clearParameters();
                    ps.clearBatch();
                }
                if (c != null) {
                    c.setAutoCommit(true);
                    c.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Exception in finally", e);
            }
        }
    }

    public void saveServiceRequestLog(ServiceLog log, String schemaName) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ds.getConnection(schemaName);
            ps = c.prepareStatement("INSERT INTO servicelog " +
                    "(userId, requestId, requestSentAt, requestReceivedAt, restMethod) " +
                    "VALUES (?,?,?,?,?)");
            ps.setString(1, log.userId);
            ps.setString(2, log.requestId);
            ps.setTimestamp(3, log.requestSentAt);
            ps.setTimestamp(4, log.requestReceivedAt);
            ps.setString(5, log.restMethod);

            ps.execute();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            try {
                if (ps != null) {
                    ps.clearParameters();
                    ps.clearBatch();
                }
                if (c != null) {
                    c.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Exception in finally", e);
            }
        }
    }

    public void updateServiceRequestLog(ServiceLog log, String schemaName) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ds.getConnection(schemaName);
            ps = c.prepareStatement("UPDATE servicelog " +
                    "SET transStatus = ?, transAt = ? WHERE requestId = ?");
            ps.setString(1, log.transStatus);
            ps.setTimestamp(2, log.transAt);
            ps.setString(3, log.requestId);

            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            try {
                if (ps != null) {
                    ps.clearParameters();
                    ps.clearBatch();
                }
                if (c != null) {
                    c.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Exception in finally", e);
            }
        }
    }
}
*/
