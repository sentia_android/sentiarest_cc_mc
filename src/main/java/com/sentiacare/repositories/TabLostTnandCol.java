package com.sentiacare.repositories;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TabLostTnandCol {
    public final List<String> tablostTables_BC = new ArrayList<>();
    public final List<String> tablostTables_PC = new ArrayList<>();
    public final List<String> tablostTables_masterDb = new ArrayList<>();
    public final List<String> tablostSkipColumns = new ArrayList<>();
    public final List<String> tablostSkipColumns_insttn = new ArrayList<>();
    public Map<String, String> groupByForTable = new HashMap<>();
    public final List<String> tablostInstClientColumns = new ArrayList<>();

    {



        tablostTables_BC.add("tbluseractivity");
        tablostTables_BC.add("tblstocksupplytracker");
        tablostTables_BC.add("tblcasemgmt");

        tablostTables_PC.add("tblcasemgmt");

        tablostTables_masterDb.add("tblinstusers");
        tablostTables_masterDb.add("tblinstitutiondetails");

        tablostSkipColumns.add("regjsy");
        tablostSkipColumns.add("vishstatus");
        tablostSkipColumns.add("recordvieweddate");
        tablostSkipColumns.add("serviceorderid");
        tablostSkipColumns.add("imagepath");
        tablostSkipColumns.add("servercreateddate");
        tablostSkipColumns.add("serverupdateddate");
        tablostSkipColumns.add("serverreceiveddate");
        tablostSkipColumns.add("modulestobeenabled");

        tablostSkipColumns_insttn.add("phcname");
        tablostSkipColumns_insttn.add("scname");
        tablostSkipColumns_insttn.add("panchayathname");


        groupByForTable.put("tblchildinfo", "GROUP BY chlId, transId ");
        groupByForTable.put("tblchildvisitdetails", "GROUP BY ChildId, VisitNum, visDSymptoms ");
        groupByForTable.put("tblchlparentdetails", "GROUP BY chlParentId, transId ");
       // groupByForTable.put("tblcomplicationmgmt", "GROUP BY WomanId, VisitNum ");
        groupByForTable.put("tbldeliveryinfo", "GROUP BY WomanId, transId ");
       // groupByForTable.put("tblhrpcomplicationmgmt", "GROUP BY WomanId,VisitNum ");
        groupByForTable.put("tblregisteredwomen", "GROUP BY WomanId ");
        groupByForTable.put("tblvisitchildheader", "GROUP BY WomanId,ChildId, VisitNum ");
        groupByForTable.put("tblvisitdetails", "GROUP BY WomanId, VisitNum,visDSymptoms ");
        groupByForTable.put("tblvisitheader", "GROUP BY WomanId,VisitNum ");
        groupByForTable.put("tbladolreg", "GROUP BY adolId ");
        groupByForTable.put("tbladolvisitheader", "GROUP BY AdolId, VisitId ");
        groupByForTable.put("tbladolpregnant", "GROUP BY AdolId, pregnantCount ");
        groupByForTable.put("tbladolvisitdetails", "GROUP BY AdolId, VisitId ");
        groupByForTable.put("tblcovidtestdetails", "GROUP BY WomenId, VisitNum");
        groupByForTable.put("tblcovidvaccinedetails", "GROUP BY BeneficiaryId, CovidVaccinatedNo");
        groupByForTable.put("tblchildgrowth", "GROUP BY chlId, chlGMVisitId");
        groupByForTable.put("tblmansimitratracker", "");
        groupByForTable.put("tblimagestore", "");
        groupByForTable.put("tblbirthpreparedness", " GROUP BY WomanId, bpVisitDate "); //Ramesh 8-6-2022
        groupByForTable.put("tbleligiblecouplereg", " GROUP BY CoupleId ");
        groupByForTable.put("tblechomevisit", " GROUP BY CoupleId, cplVisId ");


        tablostInstClientColumns.add("lastwomennumber");
        tablostInstClientColumns.add("lastactivitynumber");
        tablostInstClientColumns.add("lasttransnumber");
        tablostInstClientColumns.add("lastrequestnumber");
        tablostInstClientColumns.add("lastdangernumber");
        tablostInstClientColumns.add("lastparentnumber");
        tablostInstClientColumns.add("lastadolnumber");
    }
}
