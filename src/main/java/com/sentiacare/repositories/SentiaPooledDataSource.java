package com.sentiacare.repositories;

import com.sentiacare.utils.AppConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class SentiaPooledDataSource {

    private static SentiaPooledDataSource db;
    private final AppConfig appConfig;

    private SentiaPooledDataSource(AppConfig appConfig) {
        this.appConfig = appConfig;
    }


    private static final Map<String, HikariDataSource> dataSourceForName = new HashMap<>();
    private HikariDataSource hikariDataSource;


    /*private SentiaPooledDataSource() {
    }*/
//    private SentiaPooledDataSource(AppConfig appConfig) {
//        this.appConfig = appConfig;
//    }


   /* public static SentiaPooledDataSource getInstance() {
        if (db == null) {
            db = new SentiaPooledDataSource();
        }
        return db;
    }*/

    public static SentiaPooledDataSource getInstance(AppConfig appConfig) {
        if (db == null) {
            db = new SentiaPooledDataSource(appConfig);
        }
        return db;
    }

   /* public synchronized Connection getConnection(String dbName) throws SQLException {
        HikariDataSource ds = dataSourceForName.get(dbName);
        if (ds == null) {
            HikariConfig hikariConfig = new HikariConfig();
            hikariConfig.setUsername("root");
            hikariConfig.setPassword("bc@AS!21O");
            hikariConfig.setJdbcUrl("jdbc:mysql://localhost:3306/" + dbName);
            hikariConfig.setMaximumPoolSize(10);
            hikariConfig.setMaxLifetime(28500000); // 7.9 hours < 8 hours (default) set for MySQL idle_timeout

            ds = new HikariDataSource(hikariConfig);
            dataSourceForName.put(dbName, ds);
        }
        return ds.getConnection();
    }*/

    public synchronized Connection getConnection(String dbName) throws SQLException {
         hikariDataSource = dataSourceForName.get(dbName);
        if (hikariDataSource == null) {
            HikariConfig hikariConfig = new HikariConfig();
            hikariConfig.setUsername(appConfig.dbUser);
            hikariConfig.setPassword(appConfig.dbPassword);
//            hikariConfig.setJdbcUrl("jdbc:mysql://" + appConfig.dbIp + "/" + dbName); //old
            hikariConfig.setJdbcUrl("jdbc:mysql://" + appConfig.dbIp + "/" + dbName +"?sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE");

           /* hikariConfig.setUsername("ukstendadmusr_live");
            hikariConfig.setPassword("17Nov17@Kan_Kasturi_uklive");
            hikariConfig.setJdbcUrl("jdbc:mysql://148.72.244.241/"+dbName);*/

            hikariConfig.setMaximumPoolSize(appConfig.maxPoolSize);
            hikariConfig.setConnectionTimeout(appConfig.connTimeOut);
            hikariConfig.setMaxLifetime(appConfig.maxLifeTime); // 7.9 hours < 8 hours (default) set for MySQL idle_timeout

//            23Jan2023 Arpitha
            hikariConfig.setMinimumIdle(appConfig.minimum_idle);
            hikariConfig.setValidationTimeout(appConfig.validation_timeout);
            hikariConfig.setIdleTimeout(appConfig.idle_timeout);
            //fixme When Internal Testing and Dev Time. Set this Value to 10000 - Will helps to find where connection is leaking, 0 - means disabled
            hikariConfig.setLeakDetectionThreshold(0);//23Jan2023 Arpitha

            hikariDataSource = new HikariDataSource(hikariConfig);
            dataSourceForName.put(dbName, hikariDataSource);
        }
        return hikariDataSource.getConnection();
    }

    public HikariDataSource getHikariDataSource(){
        return hikariDataSource;
    }
}
