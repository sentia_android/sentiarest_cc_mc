/*
package com.sentiacare.repositories;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ArchivalMarker implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(ArchivalMarker.class);

    private final String requestId;
    private final SentiaPooledDataSource ds;
    private final String institutionSchema;

    public ArchivalMarker(String requestId, String institutionSchema, SentiaPooledDataSource ds) {
        this.requestId = requestId;
        this.institutionSchema = institutionSchema;
        this.ds = ds;
    }

    @Override
    public void run() {
        if(requestId == null || ds == null) {
            LOGGER.warn("Cannot run ArchivalMarker. Either requestId or dbname is null, given requestId {}, dbname {}", requestId, institutionSchema);
            return;
        }
        try (Connection connection = ds.getConnection(institutionSchema)) {

            int userId = Integer.parseInt(requestId.substring(16, 21));

            PreparedStatement ps1 = connection.prepareStatement("SELECT UserId FROM tranusers WHERE userId = ?");
            ps1.setInt(1, userId);

            ResultSet rs1 = ps1.executeQuery();
            if (rs1.isBeforeFirst()) {
                PreparedStatement ps2 = connection.prepareStatement("UPDATE tranusers SET requestId = ? WHERE userId = ?");
                ps2.setString(1, requestId);
                ps2.setInt(2, userId);
                ps2.executeUpdate();
            } else {
                PreparedStatement ps2 = connection.prepareStatement("INSERT INTO tranusers (userId, requestId) VALUES (?,?)");
                ps2.setInt(1, userId);
                ps2.setString(2, requestId);
                ps2.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
*/
