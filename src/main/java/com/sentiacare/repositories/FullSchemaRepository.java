package com.sentiacare.repositories;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;
import com.sentiacare.dtos.ExportRequest;
import com.sentiacare.dtos.ExportResponse;
import com.sentiacare.dtos.GetListOfTables;
import com.sentiacare.utils.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

public class FullSchemaRepository {

    private static final Logger LOGGER = LogManager.getLogger(FullSchemaRepository.class);
    private final SentiaPooledDataSource ds;
    private final Gson gson = new Gson();
    private final SchemaExclusions schemaExclusions = new SchemaExclusions();
    private final SchemaInclusions schemaInclusions = new SchemaInclusions();
    private final SchemaMansiMitraInclusions schemaMansiMitraInclusions = new SchemaMansiMitraInclusions();

    public FullSchemaRepository(SentiaPooledDataSource ds) {
        this.ds = ds;
    }

    public void export(JsonWriter jw, ExportRequest r, AppConfig appConfig) throws Exception {
        Connection c = null;
        Connection c1 = null;
        try {
            c = ds.getConnection(r.schemaName);
            String institutionSchema = getOrThrow(r, c);
            List<String> reporters = getReportersOrThrow(r, c);
            c1 = ds.getConnection(institutionSchema);
            if (r.lastSyncDate == null || r.lastSyncDate.length() == 0) { //8-6-2022 Ramesh
                exportFull(jw, reporters, c1, r, appConfig);
            } else {
                exportSubSet(r, jw, reporters, c1, appConfig);
            }
        } catch (SQLException ex) {
            ExportResponse e = new ExportResponse();
            e.type = "/errors/sql";
            e.title = "We are experiencing technical difficulties, please contact support staff.";
            e.status = 500;
            e.detail = ex.getMessage();
            e.instance = String.format("/export/%s/%s/%s", r.schemaName, r.lastSyncDate, r.mcId);
            throw new Exception(gson.toJson(e));
        } finally {
            try {
                if (c != null) {
                    c.close();
                }
                if (c1 != null) {
                    c1.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Connection close threw exception", e);
            }
        }
    }

    //21Jun2022 Bindu add appconfig
    public void exportMansiMitra(JsonWriter jw, ExportRequest r, AppConfig appConfig) throws Exception {
        Connection c = null;
        Connection c1 = null;
        try {
            c = ds.getConnection(r.schemaName);
            String institutionSchema = getOrThrow(r, c);
            List<String> reporters = getReportersOrThrow(r, c);
            c1 = ds.getConnection(institutionSchema);
            if (r.lastSyncDate == null) {
                exportMansiMitraFull(jw, reporters, c1);
            } else {
                exportSubSet(r, jw, reporters, c1, appConfig);
            }
        } catch (SQLException ex) {
            ExportResponse e = new ExportResponse();
            e.type = "/errors/sql";
            e.title = "We are experiencing technical difficulties, please contact support staff.";
            e.status = 500;
            e.detail = ex.getMessage();
            e.instance = String.format("/export/%s/%s/%s", r.schemaName, r.lastSyncDate, r.mcId);
            throw new Exception(gson.toJson(e));
        } finally {
            try {
                if (c != null) {
                    c.close();
                }
                if (c1 != null) {
                    c1.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Connection close threw exception", e);
            }
        }
    }

    public String getOrThrow(ExportRequest r, Connection c) throws Exception {
        PreparedStatement ps0 = c.prepareStatement("SELECT Inst_Database FROM tblinstusers WHERE UserMobileNumber = ?");
        ps0.setString(1, r.mcId);
        ResultSet rs0 = ps0.executeQuery();
        if (!rs0.isBeforeFirst()) {
            ExportResponse e = new ExportResponse();
            e.type = "/errors/missing-institution-schema";
            e.title = "You are not registered with an institution";
            e.status = 500;
            e.detail = "Could not find institution schema for given " + r.userType + "Id";
            e.instance = String.format("/export/%s/%s/%s", r.schemaName, r.lastSyncDate, r.mcId);
            throw new Exception(gson.toJson(e));
        }
        rs0.next();
        return rs0.getString(1);
    }

    private List<String> getReportersOrThrow(ExportRequest r, Connection c) throws Exception {
        String sb = "";
        ResultSet reportersRS = null;
        if (r.userType.equals("BC")) {
            sb = "Select distinct UserId from tblfacilitydetails where CC_Id = ? and UserId IN (Select UserId from tblinstusers where UserRole='ACHF'  and isDeactivated!=1 ) order by userid asc";
            PreparedStatement ps1 = c.prepareStatement(sb);
            ps1.setString(1, r.mcId);
            reportersRS = ps1.executeQuery();
        } else if (r.userType.equals("PC")) {
            sb = "SELECT DISTINCT fd.UserId FROM tblfacilitydetails fd" +
                    " JOIN tblinstusers iu" +
                    " ON fd.UserId = iu.UserId" +
                    " WHERE fd.MC_Id = ?" +
                    " AND iu.isValidated = 1 " +
                    "union " +
                    "select distinct CC_SysUID from tblfacilitydetails where MC_Id = ?";
            PreparedStatement ps1 = c.prepareStatement(sb);
            ps1.setString(1, r.mcId);
            ps1.setString(2, r.mcId);
            reportersRS = ps1.executeQuery();
        }
        List<String> reporters = new ArrayList<>();
        if (reportersRS != null && !reportersRS.isBeforeFirst()) {
            ExportResponse e = new ExportResponse();
            e.type = "/errors/empty-reporters";
            e.title = "You do not have any reporters";
            e.status = 500;
            e.detail = "Could not find reporters for given " + r.userType;
            e.instance = String.format("/export/%s/%s/%s", r.schemaName, r.lastSyncDate, r.mcId);
            throw new Exception(gson.toJson(e));
        }
        while (reportersRS.next()) {
            reporters.add(reportersRS.getString(1));
        }
        return reporters;
    }

    private void exportFull(JsonWriter jw, List<String> reporters, Connection c1, ExportRequest r, AppConfig appConfig) throws Exception {
        Statement stat1 = c1.createStatement();
        jw.beginArray();
        String prevDateTime = getPreviousDate(); //21Jun2022 Bindu get prev mid night date from curr date
        for (String reporter : reporters) {
            for (String tn : schemaInclusions.tables) {
                StringBuilder sqlBuilder = new StringBuilder();
                if (tn.equalsIgnoreCase("tblcasemgmt")) {
//                    sqlBuilder.append(String.format("SELECT * FROM %s WHERE MMUserId = %s and hcmCreatedByUserType != '"+r.userType+"'", tn, reporter));
                    if (!appConfig.isTestingInternal) { //21Jun2022 Bindu Add servercreateddate < prev date
                        String sql = "SELECT * FROM " + tn + " WHERE MMUserId = '" + reporter + "'  and hcmCreatedByUserType != '" + r.userType + "' AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s')";
                        sqlBuilder.append(sql);
                    } else {
                        sqlBuilder.append(String.format("SELECT * FROM %s WHERE MMUserId = %s and hcmCreatedByUserType != '" + r.userType + "'", tn, reporter));
                    }
                } else {
//                    sqlBuilder.append(String.format("SELECT * FROM %s WHERE UserId = %s", tn, reporter));
                    if (!appConfig.isTestingInternal) { //21Jun2022 Bindu Add servercreateddate < prev date
                        String sql = "SELECT * FROM " + tn + " WHERE UserId = '" + reporter + "' AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s')";
                        sqlBuilder.append(sql);
                    } else
                        sqlBuilder.append(String.format("SELECT * FROM %s WHERE UserId = %s", tn, reporter));
                }
                sqlBuilder.append(" ");
                String groupBy = schemaInclusions.groupByForTable.get(tn);
                sqlBuilder.append(groupBy == null ? "" : groupBy);

                ResultSet tableRS = stat1.executeQuery(sqlBuilder.toString());
                ResultSetMetaData rsm = tableRS.getMetaData();
                int columnCount = rsm.getColumnCount();
                while (tableRS.next()) {
                    JsonObject row = new JsonObject();
                    row.addProperty("tn", tn);
                    for (int i = 1; i <= columnCount; i++) {
                        String cn = rsm.getColumnName(i);
                        if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                            continue;
                        }
                        int ct = rsm.getColumnType(i);
                        boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                        if (numericVal) {
                            row.addProperty(cn, tableRS.getInt(i));
                        } else {
                            row.addProperty(cn, tableRS.getString(i));
                        }
                    }
                    gson.toJson(row, jw);
                }
            }
        }
        jw.endArray();
    }


    private void exportSubSet(ExportRequest r, JsonWriter jw, List<String> reporters, Connection c1, AppConfig appConfig) throws Exception {
        jw.beginArray();
        String prevDateTime = getPreviousDate(); //21Jun2022 Bindu get prev mid night date from curr date

        for (String reporter : reporters) {
            for (String tn : schemaInclusions.tables) {
                StringBuilder sqlBuilder = new StringBuilder();
                if (tn.equalsIgnoreCase("tblcasemgmt")) {
                    sqlBuilder.append("SELECT * FROM ").append(tn)
                            .append(" WHERE MMUserId = ? and hcmCreatedByUserType != '" + r.userType + "' AND servercreateddate > ")
                            .append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");
                    if (!appConfig.isTestingInternal) //21Jun2022 Bindu Add servercreateddate < prev date
                        sqlBuilder.append(" AND servercreateddate <= ").append("str_to_date(?,'%Y-%m-%d %H:%i:%s')");
                }
                /*else if (tn.equalsIgnoreCase("tblinstusers")) {
                    sqlBuilder.append("SELECT * FROM ").append(tn)
                            .append(" WHERE UserId = ?  AND datecreated > ").append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");
                            //AND servercreateddate > ")
                            //.append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");
                }*/ // this condition was not there in MC so commented here //Ramesh 26-Jun-2022
                else {
                    sqlBuilder.append("SELECT * FROM ").append(tn)
                            .append(" WHERE UserId = ? AND servercreateddate > ")
                            .append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");
                    if (!appConfig.isTestingInternal) //21Jun2022 Bindu Add servercreateddate < prev date
                        sqlBuilder.append(" AND servercreateddate <= ").append("str_to_date(?,'%Y-%m-%d %H:%i:%s')");
                }
                sqlBuilder.append(" ");
                String groupBy = schemaInclusions.groupByForTable.get(tn);
                sqlBuilder.append(groupBy == null ? "" : groupBy);

                PreparedStatement ps0 = c1.prepareStatement(sqlBuilder.toString());
                ps0.setString(1, reporter);
                ps0.setString(2, r.lastSyncDate);

                if (!appConfig.isTestingInternal) //21Jun2022 Bindu
                    ps0.setString(3, prevDateTime); // Bindu 21Jun2022 Pass Prev Datetime

                ResultSet tableRS = ps0.executeQuery();
                ResultSetMetaData rsm = tableRS.getMetaData();
                int columnCount = rsm.getColumnCount();
                while (tableRS.next()) {
                    JsonObject row = new JsonObject();
                    row.addProperty("tn", tn);
                    for (int i = 1; i <= columnCount; i++) {
                        String cn = rsm.getColumnName(i);
                        if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                            continue;
                        }
                        int ct = rsm.getColumnType(i);
                        boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                        if (numericVal) {
                            row.addProperty(cn, tableRS.getInt(i));
                        } else {
                            row.addProperty(cn, tableRS.getString(i));
                        }
                    }
                    gson.toJson(row, jw);
                }
            }
            addAuditTrailRecords(reporter, jw, r.lastSyncDate, c1, appConfig, prevDateTime);
        }
        jw.endArray();
    }

    private void addAuditTrailRecords(String reporterId, JsonWriter jw, String lastSyncDate, Connection c1, AppConfig appConfig, String prevDateTime) throws SQLException {
        String columnName;
        Gson gsonTemp = new GsonBuilder().serializeNulls().create();
        for (int i = 0; i < schemaInclusions.editableTables.size(); i++) {
            String tn = schemaInclusions.editableTables.get(i);
            if (tn.equalsIgnoreCase("tbladolreg")) {
                columnName = "AdolID";
            } else if (tn.equalsIgnoreCase("tblchildinfo")) {
                columnName = "chlID";
            } else if (tn.equalsIgnoreCase("tblEligibleCoupleReg")) {
                columnName = "CoupleId";
            } else if (tn.equalsIgnoreCase("tblActivityJsonStore")) {
                columnName = "activityId";
            } else {
                columnName = "WomanId";
            }

            /*String sqlBuilder = "Select * from tblaudittrail " + " where   UserId = '" +
                    reporterId +
                    "' and TblName = '" +
                    tn +
                    "' and ServerReceivedDate >= " +
                    " str_to_date('" +
                    lastSyncDate +
                    "', '%Y-%m-%d %H:%i:%s') " +
                    " and  WomanId NOT IN (Select " +
                    columnName +
                    " from " +
                    tn +
                    " where" +
                    " ServerCreatedDate >= " +
                    " str_to_date('" +
                    lastSyncDate +
                    "', '%Y-%m-%d %H:%i:%s') )";*/
            String sqlBuilder;
            if (!appConfig.isTestingInternal) { //21Jun2022 Bindu Add servercreateddate < prev date
                sqlBuilder = "Select * from tblaudittrail " + " where   UserId = '" +
                        reporterId +
                        "' and TblName = '" +
                        tn +
                        "' and ServerReceivedDate >= " +
                        " str_to_date('" +
                        lastSyncDate +
                        "', '%Y-%m-%d %H:%i:%s') " +
                        " and ServerReceivedDate <= " + // start 21Jun2022 Bindu add prev date time
                        " str_to_date('" +
                        prevDateTime +
                        "', '%Y-%m-%d %H:%i:%s') " + //End
                        " and  WomanId NOT IN (Select " +
                        columnName +
                        " from " +
                        tn +
                        " where" +
                        " ServerCreatedDate >= " +
                        " str_to_date('" +
                        lastSyncDate +
                        "', '%Y-%m-%d %H:%i:%s') " +
                        " and ServerCreatedDate <= " + // start 21Jun2022 Bindu add prev date time
                        " str_to_date('" +
                        prevDateTime +
                        "', '%Y-%m-%d %H:%i:%s') " + // End
                        ")";
            } else {
                sqlBuilder = "Select * from tblaudittrail " + " where   UserId = '" +
                        reporterId +
                        "' and TblName = '" +
                        tn +
                        "' and ServerReceivedDate >= " +
                        " str_to_date('" +
                        lastSyncDate +
                        "', '%Y-%m-%d %H:%i:%s') " +
                        " and  WomanId NOT IN (Select " + columnName + " from " + tn + " where" + " ServerCreatedDate >= " + " str_to_date('" + lastSyncDate + "', '%Y-%m-%d %H:%i:%s') " + ")";
            }

            ResultSet rs = c1.createStatement().executeQuery(sqlBuilder);
            ResultSetMetaData rsm = rs.getMetaData();
            int columnCount = rsm.getColumnCount();
            while (rs.next()) {
                JsonObject row = new JsonObject();
                row.addProperty("tn", "tblaudittrail");
                for (int x = 1; x <= columnCount; x++) {
                    String cn = rsm.getColumnName(x);
                    if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                        continue;
                    }
                    int ct = rsm.getColumnType(x);
                    boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                    if (numericVal) {
                        row.addProperty(cn, rs.getInt(x));
                    } else {
                        row.addProperty(cn, rs.getString(x));
                    }
                }
                gsonTemp.toJson(row, jw);
            }
        }
    }

    private void exportMansiMitraFull(JsonWriter jw, List<String> reporters, Connection c1) throws Exception {
        Statement stat1 = c1.createStatement();
        jw.beginArray();
        for (String reporter : reporters) {
            for (String tn : schemaMansiMitraInclusions.tables) {
                StringBuilder sqlBuilder = new StringBuilder();
                if (tn.equalsIgnoreCase("tblcasemgmt")) {
                    sqlBuilder.append(String.format("SELECT * FROM %s WHERE MMUserId = %s and hcmCreatedByUserType != 'PC'", tn, reporter));
                } else {
                    sqlBuilder.append(String.format("SELECT * FROM %s WHERE tblmmUniqueId = %s", tn, reporter));
                }
                sqlBuilder.append(" ");
                String groupBy = schemaInclusions.groupByForTable.get(tn);
                sqlBuilder.append(groupBy == null ? "" : groupBy);

                ResultSet tableRS = stat1.executeQuery(sqlBuilder.toString());
                ResultSetMetaData rsm = tableRS.getMetaData();
                int columnCount = rsm.getColumnCount();
                while (tableRS.next()) {
                    JsonObject row = new JsonObject();
                    row.addProperty("tn", tn);
                    for (int i = 1; i <= columnCount; i++) {
                        String cn = rsm.getColumnName(i);
                        if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                            continue;
                        }
                        int ct = rsm.getColumnType(i);
                        boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                        if (numericVal) {
                            row.addProperty(cn, tableRS.getInt(i));
                        } else {
                            row.addProperty(cn, tableRS.getString(i));
                        }
                    }
                    gson.toJson(row, jw);
                }
            }
        }
        jw.endArray();
    }

    public SentiaPooledDataSource getDataSource() {
        return ds;
    }

    //21Jun2022 Bindu
    private String getPreviousDate() throws Exception {
        String strPrevDate = "";

        // Millseconds in a day
        final long ONE_DAY_MILLI_SECONDS = 24 * 60 * 60 * 1000;

        // date format
        final String DATE_FORMAT = "yyyy-MM-dd";

        Date d = new Date();

        // Simple date formatter
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        String currDate = dateFormat.format(d);
        Date date = dateFormat.parse(currDate);

        // Getting the previous day and formatting into 'YYYY-MM-DD'
        long previousDayMilliSeconds = date.getTime() - ONE_DAY_MILLI_SECONDS;
        Date previousDate = new Date(previousDayMilliSeconds);
        strPrevDate = dateFormat.format(previousDate);

        strPrevDate = strPrevDate + " 23:59:59";

        // System.out.println("Given Date : " + currDate);
        // System.out.println("Previous Date : " + strPrevDate);

        return strPrevDate;
    }


    public void exportReallocation(JsonWriter jw, ExportRequest r, AppConfig appConfig) throws Exception {
        Connection c = null;
        Connection c1 = null;
        try {
            c = ds.getConnection(r.schemaName);
            String institutionSchema = getOrThrow(r, c);
            List<String> reporters = getReportersOrThrowReallocation(r, c);
            c1 = ds.getConnection(institutionSchema);
            if (r.lastSyncDate == null || r.lastSyncDate.length() == 0) { //8-6-2022 Ramesh
                exportFullReallocationNew(jw, reporters, c1, r, appConfig);
            } else {
                exportSubSetReallocationNew(r, jw, reporters, c1, appConfig);
            }
        } catch (SQLException ex) {
            ExportResponse e = new ExportResponse();
            e.type = "/errors/sql";
            e.title = "We are experiencing technical difficulties, please contact support staff.";
            e.status = 500;
            e.detail = ex.getMessage();
            e.instance = String.format("/export/%s/%s/%s", r.schemaName, r.lastSyncDate, r.mcId);
            throw new Exception(gson.toJson(e));
        } finally {
            try {
                if (c != null) {
                    c.close();
                }
                if (c1 != null) {
                    c1.close();
                }
//                System.out.println("Error");
            } catch (SQLException e) {
                LOGGER.error("Connection close threw exception", e);
            }
        }
    }

    //  10Feb2025 Bindu - rework on the mtd for proper naming convention and opt
    private void exportFullReallocation(JsonWriter jw, List<String> listVillages, Connection c1, ExportRequest r, AppConfig appConfig) {
        try {
            Statement stat1 = c1.createStatement();
            jw.beginArray();
            String prevDateTime = getPreviousDate(); //21Jun2022 Bindu get prev mid night date from curr date

            List<GetListOfTables> listOfTablesPrimary = getTableNamesForFetchPrimary(stat1);
            List<GetListOfTables> listOfTablesSecondary = getTableNamesForFetchSecondary(stat1);

//            Iterate by villages to get data from primary tables
            for (String strVillageID : listVillages) {
                String beneficiaryIds = "";
//              Fetch records from primary table
                for (int k = 0; k < listOfTablesPrimary.size(); k++) {
                    String tn = listOfTablesPrimary.get(k).getTableName();
                    String villageId = listOfTablesPrimary.get(k).getVillageIdColumnName();
                    StringBuilder sqlBuilder = new StringBuilder();

                    if (!appConfig.isTestingInternal) { //21Jun2022 Bindu Add servercreateddate < prev date
                        String sql = "SELECT * FROM " + tn + " WHERE " + villageId + " = '" + strVillageID + "' AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s')";
                        sqlBuilder.append(sql);
                    } else
                        sqlBuilder.append(String.format("SELECT * FROM %s WHERE " + villageId + " = %s", tn, strVillageID));

                    sqlBuilder.append(" ");

                    String groupBy = listOfTablesPrimary.get(k).getGroupby();

                    sqlBuilder.append(groupBy == null ? "" : " group by " + groupBy); // 06Jan2025 Bindu - Add Group by tag

//                    System.out.println("Sql Query: " + sqlBuilder.toString());//08Sep24 Arpitha

                    ResultSet tableRS = stat1.executeQuery(sqlBuilder.toString());
                    ResultSetMetaData rsm = tableRS.getMetaData();
                    int columnCount = rsm.getColumnCount();
                    while (tableRS.next()) {
                        JsonObject row = new JsonObject();
                        row.addProperty("tn", tn);
                        for (int i = 1; i <= columnCount; i++) {
                            String cn = rsm.getColumnName(i);
                            if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                                continue;
                            }
                            int ct = rsm.getColumnType(i);
                            boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                            if (numericVal) {
                                row.addProperty(cn, tableRS.getInt(i));
                            } else {
                                row.addProperty(cn, tableRS.getString(i));
                            }

                            if (beneficiaryIds != null && beneficiaryIds.trim().length() > 0)
                                beneficiaryIds = beneficiaryIds + ",'" + tableRS.getString(listOfTablesPrimary.get(k).beneIdColumnName) + "'";
                            else
                                beneficiaryIds = "'" + tableRS.getString(listOfTablesPrimary.get(k).beneIdColumnName) + "'";

                        }
                        gson.toJson(row, jw);
                    }
                }


//                fetch secondary table data
                if (beneficiaryIds != null && beneficiaryIds.trim().length() > 0) {
                    for (int k = 0; k < listOfTablesSecondary.size(); k++) {
                        String tn = listOfTablesSecondary.get(k).getTableName();
                        StringBuilder sqlBuilder = new StringBuilder();
                        if (tn.equalsIgnoreCase("tblcasemgmt")) {
                            if (!appConfig.isTestingInternal) { //21Jun2022 Bindu Add servercreateddate < prev date
                                String sql = "SELECT * FROM " + tn + " WHERE (BeneficiaryID IN (Select womanId from tblregisteredwomen where regVillage = " + strVillageID + " ) or " +
                                        " BeneficiaryID IN  (Select chlId from tblchildinfo where chlTribalHamlet = " + strVillageID + " ) or " +
                                        " BeneficiaryID IN  (Select adolId from tbladolreg where regAdolFacilities = " + strVillageID + " ) )  and hcmCreatedByUserType != '" + r.userType + "' AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s')";
                                sqlBuilder.append(sql);
                            } else {
                                sqlBuilder.append(String.format("SELECT * FROM %s WHERE   (BeneficiaryID IN (Select womanId from tblregisteredwomen where regVillage = " + strVillageID + " ) or " +
                                        " BeneficiaryID IN  (Select chlId from tblchildinfo where chlTribalHamlet = " + strVillageID + " ) or " +
                                        "  BeneficiaryID IN  (Select adolId from tbladolreg where regAdolFacilities = " + strVillageID + " ) )   and hcmCreatedByUserType != '" + r.userType + "'", tn, strVillageID));
                            }
                        } else {
                            if (!appConfig.isTestingInternal) { //21Jun2022 Bindu Add servercreateddate < prev date
                                String sql = "SELECT * FROM " + tn + " WHERE " + listOfTablesSecondary.get(k).getBeneIdColumnName() + " IN ( " + beneficiaryIds + " ) AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s')";
                                sqlBuilder.append(sql);
                            } else {
                                String sql = "SELECT * FROM " + tn + "  WHERE " + listOfTablesSecondary.get(k).getBeneIdColumnName() + " IN  ( " + beneficiaryIds + " ) ";
                                sqlBuilder.append(sql);

                            }
                        }
                        sqlBuilder.append(" ");
                        String groupBy = listOfTablesSecondary.get(k).getGroupby();
                        sqlBuilder.append(groupBy == null ? "" : " group by " + groupBy); // 06Jan2025 Bindu - Add Group by tag

//                        System.out.println("Sql Query: " + sqlBuilder.toString());//08Sep24 Arpitha

                        ResultSet tableRS = stat1.executeQuery(sqlBuilder.toString());
                        ResultSetMetaData rsm = tableRS.getMetaData();
                        int columnCount = rsm.getColumnCount();
                        while (tableRS.next()) {
                            JsonObject row = new JsonObject();
                            row.addProperty("tn", tn);
                            for (int i = 1; i <= columnCount; i++) {
                                String cn = rsm.getColumnName(i);
                                if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                                    continue;
                                }
                                int ct = rsm.getColumnType(i);
                                boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                                if (numericVal) {
                                    row.addProperty(cn, tableRS.getInt(i));
                                } else {
                                    row.addProperty(cn, tableRS.getString(i));
                                }

                            }
                            gson.toJson(row, jw);
                        }
                    }
                }
            }
            jw.endArray();
        } catch (Exception e) {
            LOGGER.error("exportFullReallocation", e);
            e.printStackTrace();
        }
    }


    private void exportSubSetReallocation(ExportRequest r, JsonWriter jw, List<String> reporters, Connection c1, AppConfig appConfig) {
        try {
            jw.beginArray();
            String prevDateTime = getPreviousDate(); //21Jun2022 Bindu get prev mid night date from curr date

            Statement stat1 = c1.createStatement();
            List<GetListOfTables> listOfTables = getTableNamesForFetchPrimary(stat1);
            List<GetListOfTables> listOfTablesSecondary = getTableNamesForFetchSecondary(stat1);

            for (String reporter : reporters) {
                String beneficiaryIds = "";
//            for (String tn : schemaInclusions.tables) {
                for (int k = 0; k < listOfTables.size(); k++) {

                    String tn = listOfTables.get(k).getTableName();
                    String villageId = listOfTables.get(k).getVillageIdColumnName();

                    StringBuilder sqlBuilder = new StringBuilder();
                    if (tn.equalsIgnoreCase("tblcasemgmt")) {
                        sqlBuilder.append("SELECT * FROM ").append(tn)
//                            .append(" WHERE MMUserId = ? "+
                                .append(" (BeneficiaryID IN (Select womanId from tblregisteredwomen where regVillage = ? ) or " +
                                        " BeneficiaryID IN  (Select chlId from tblchildinfo where chlTribalHamlet = " + reporter + " ) or " +
                                        " BeneficiaryID IN  (Select adolId from tbladolreg where regAdolFacilities =  " + reporter + " ) )"
                                        + " and hcmCreatedByUserType != '" + r.userType + "' AND servercreateddate > ")
                                .append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");
                        if (!appConfig.isTestingInternal) //21Jun2022 Bindu Add servercreateddate < prev date
                            sqlBuilder.append(" AND servercreateddate <= ").append("str_to_date(?,'%Y-%m-%d %H:%i:%s')");
                    }
                /*else if (tn.equalsIgnoreCase("tblinstusers")) {
                    sqlBuilder.append("SELECT * FROM ").append(tn)
                            .append(" WHERE UserId = ?  AND datecreated > ").append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");
                            //AND servercreateddate > ")
                            //.append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");
                }*/ // this condition was not there in MC so commented here //Ramesh 26-Jun-2022
                    else {
                   /* sqlBuilder.append("SELECT * FROM ").append(tn)
                            .append(" WHERE UserId = ? AND servercreateddate > ")
                            .append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");*/

                        sqlBuilder.append("SELECT * FROM ").append(tn)
                                .append(" WHERE " + villageId + " = ? AND servercreateddate > ")
                                .append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");

                        if (!appConfig.isTestingInternal) //21Jun2022 Bindu Add servercreateddate < prev date
                            sqlBuilder.append(" AND servercreateddate <= ").append("str_to_date(?,'%Y-%m-%d %H:%i:%s')");
                    }
                    sqlBuilder.append(" ");
//                String groupBy = schemaInclusions.groupByForTable.get(tn);
                    String groupBy = listOfTables.get(k).getGroupby();
                    sqlBuilder.append(groupBy == null ? "" : " group by " + groupBy);

//                    System.out.println(sqlBuilder.toString() + "\n");
                    PreparedStatement ps0 = c1.prepareStatement(sqlBuilder.toString());
                    ps0.setString(1, reporter);
                    ps0.setString(2, r.lastSyncDate);

                    if (!appConfig.isTestingInternal) //21Jun2022 Bindu
                        ps0.setString(3, prevDateTime); // Bindu 21Jun2022 Pass Prev Datetime

//                    System.out.println(sqlBuilder.toString() + "\n");

                    ResultSet tableRS = ps0.executeQuery();
                    ResultSetMetaData rsm = tableRS.getMetaData();
                    int columnCount = rsm.getColumnCount();
                    while (tableRS.next()) {
                        JsonObject row = new JsonObject();
                        row.addProperty("tn", tn);
                        for (int i = 1; i <= columnCount; i++) {
                            String cn = rsm.getColumnName(i);
                            if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                                continue;
                            }
                            int ct = rsm.getColumnType(i);
                            boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                            if (numericVal) {
                                row.addProperty(cn, tableRS.getInt(i));
                            } else {
                                row.addProperty(cn, tableRS.getString(i));
                            }

                            /*if (beneficiaryIds != null && beneficiaryIds.trim().length() > 0)
                                beneficiaryIds = beneficiaryIds + ",'" + tableRS.getString(listOfTables.get(k).beneIdColumnName) + "'";
                            else
                                beneficiaryIds = "'" + tableRS.getString(listOfTables.get(k).beneIdColumnName) + "'";*/ // 05Jan2025 Bindu - Comment this and create below to fetch ben ids

                        }
                        gson.toJson(row, jw);
                    }
                }

                //      getbeneficiaryIdsToFetchFromSecondaryTables - Bindu - 05JAn2025
                for (int k = 0; k < listOfTables.size(); k++) {

                    String tn = listOfTables.get(k).getTableName();
                    String villageId = listOfTables.get(k).getVillageIdColumnName();

                    StringBuilder sqlBuilder = new StringBuilder();
                    if (!tn.equalsIgnoreCase("tblcasemgmt")) {
                        sqlBuilder.append("SELECT * FROM ").append(tn)
                                .append(" WHERE " + villageId + " = ? ");
                        //.append("str_to_date(?, '%Y-%m-%d %H:%i:%s')");

                        /*if (!appConfig.isTestingInternal) //21Jun2022 Bindu Add servercreateddate < prev date
                            sqlBuilder.append(" AND servercreateddate <= ").append("str_to_date(?,'%Y-%m-%d %H:%i:%s')");*/
                    }
                    sqlBuilder.append(" ");
//                String groupBy = schemaInclusions.groupByForTable.get(tn);
                    String groupBy = listOfTables.get(k).getGroupby();
                    sqlBuilder.append(groupBy == null ? "" : " group by " + groupBy);
//                    System.out.println(sqlBuilder.toString() + "\n");

                    PreparedStatement ps0 = c1.prepareStatement(sqlBuilder.toString());
                    ps0.setString(1, reporter);
//                    ps0.setString(2, r.lastSyncDate);

                    if (!appConfig.isTestingInternal) //21Jun2022 Bindu
                        ps0.setString(3, prevDateTime); // Bindu 21Jun2022 Pass Prev Datetime

//                    System.out.println(sqlBuilder.toString() + "\n");

                    ResultSet tableRS = ps0.executeQuery();
                    ResultSetMetaData rsm = tableRS.getMetaData();
                    int columnCount = rsm.getColumnCount();
                    while (tableRS.next()) {
                        /*JsonObject row = new JsonObject();
                        row.addProperty("tn", tn);*/
                        for (int i = 1; i <= columnCount; i++) {
                            String cn = rsm.getColumnName(i);
                            if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                                continue;
                            }
                            /*int ct = rsm.getColumnType(i);
                            boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                            if (numericVal) {
                                row.addProperty(cn, tableRS.getInt(i));
                            } else {
                                row.addProperty(cn, tableRS.getString(i));
                            }*/

                            if (beneficiaryIds != null && beneficiaryIds.trim().length() > 0)
                                beneficiaryIds = beneficiaryIds + ",'" + tableRS.getString(listOfTables.get(k).beneIdColumnName) + "'";
                            else
                                beneficiaryIds = "'" + tableRS.getString(listOfTables.get(k).beneIdColumnName) + "'";

                        }
                        // gson.toJson(row, jw);
                    }
                }

                //                fetch secondary table data
                if (beneficiaryIds != null && beneficiaryIds.trim().length() > 0) {
                    for (int k = 0; k < listOfTablesSecondary.size(); k++) {
                        String tn = listOfTablesSecondary.get(k).getTableName();
                        String villageId = listOfTablesSecondary.get(k).getVillageIdColumnName();
                        StringBuilder sqlBuilder = new StringBuilder();
                        if (tn.equalsIgnoreCase("tblcasemgmt")) {
//                    sqlBuilder.append(String.format("SELECT * FROM %s WHERE MMUserId = %s and hcmCreatedByUserType != '"+r.userType+"'", tn, reporter));
                            if (!appConfig.isTestingInternal) { //21Jun2022 Bindu Add servercreateddate < prev date
//                        String sql = "SELECT * FROM " + tn + " WHERE MMUserId = '"+reporter+"'  and hcmCreatedByUserType != '"+r.userType+"' AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s')";
                                String sql = "SELECT * FROM " + tn + " WHERE (BeneficiaryID IN (Select womanId from tblregisteredwomen where regVillage = " + reporter + " ) or " +
                                        " BeneficiaryID IN  (Select chlId from tblchildinfo where chlTribalHamlet = " + reporter + " ) or " +
                                        " BeneficiaryID IN  (Select adolId from tbladolreg where regAdolFacilities = " + reporter + " ) )  and hcmCreatedByUserType != '" + r.userType + "' AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s')  AND servercreateddate >  str_to_date('" + r.lastSyncDate + "', '%Y-%m-%d %H:%i:%s')  ";
                                sqlBuilder.append(sql);
                            } else {
//                        sqlBuilder.append(String.format("SELECT * FROM %s WHERE MMUserId = %s and hcmCreatedByUserType != '"+r.userType+"'", tn, reporter));
                                String sql = "SELECT * FROM " + tn + " WHERE   (BeneficiaryID IN (Select womanId from tblregisteredwomen where regVillage =  " + reporter + "  ) or " +
                                        " BeneficiaryID IN  (Select chlId from tblchildinfo where chlTribalHamlet = " + reporter + " ) or " +
                                        "  BeneficiaryID IN  (Select adolId from tbladolreg where regAdolFacilities = " + reporter + " ) )   and hcmCreatedByUserType != '" + r.userType + "' AND servercreateddate >  str_to_date('" + r.lastSyncDate + "', '%Y-%m-%d %H:%i:%s') ";

//                                System.out.println(sql);

                                sqlBuilder.append(sql);
                            }
                        } else {
//                    sqlBuilder.append(String.format("SELECT * FROM %s WHERE UserId = %s", tn, reporter));
                            if (!appConfig.isTestingInternal) { //21Jun2022 Bindu Add servercreateddate < prev date
//                        String sql = "SELECT * FROM " + tn + " WHERE UserId = '"+reporter+"' AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s')";
                                String sql = "SELECT * FROM " + tn + " WHERE " + listOfTablesSecondary.get(k).getBeneIdColumnName() + " IN ( " + beneficiaryIds + " ) AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s')  AND servercreateddate >  str_to_date('" + r.lastSyncDate + "', '%Y-%m-%d %H:%i:%s' ) ";

                                sqlBuilder.append(sql);
                            } else {

                                String sql = "SELECT * FROM " + tn + "  WHERE " + listOfTablesSecondary.get(k).getBeneIdColumnName() + " IN  ( " + beneficiaryIds + " ) AND servercreateddate >  str_to_date('" + r.lastSyncDate + "', '%Y-%m-%d %H:%i:%s')  ";  //AND servercreateddate > ")
                                //.append("str_to_date(?, '%Y-%m-%d %H:%i:%s')

                                sqlBuilder.append(sql);

                            }
//                        sqlBuilder.append(String.format("SELECT * FROM %s WHERE "+listOfTablesSecondary.get(k).getBeneIdColumnName()+" IN %s", tn, beneficiaryIds));

//                        sqlBuilder.append(String.format("SELECT * FROM %s WHERE UserId = %s", tn, reporter));

                        }
                        sqlBuilder.append(" ");
//                String groupBy = schemaInclusions.groupByForTable.get(tn);
                        String groupBy = listOfTablesSecondary.get(k).getGroupby();

//                        sqlBuilder.append(groupBy == null ? "" : groupBy);
                        sqlBuilder.append(groupBy == null ? "" : " group by " + groupBy); //07Jan2025 Bindu - Add Group By

//                    System.out.println("Sql Query: " + sqlBuilder.toString());//08Sep24 Arpitha

//                        System.out.println(sqlBuilder.toString() + ";\n");


                        ResultSet tableRS = stat1.executeQuery(sqlBuilder.toString());
                        ResultSetMetaData rsm = tableRS.getMetaData();
                        int columnCount = rsm.getColumnCount();
                        while (tableRS.next()) {
                            JsonObject row = new JsonObject();
                            row.addProperty("tn", tn);
                            for (int i = 1; i <= columnCount; i++) {
                                String cn = rsm.getColumnName(i);
                                if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                                    continue;
                                }
                                int ct = rsm.getColumnType(i);
                                boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                                if (numericVal) {
                                    row.addProperty(cn, tableRS.getInt(i));
                                } else {
                                    row.addProperty(cn, tableRS.getString(i));
                                }

//                        beneficiaryIds.add(tableRS.getString(listOfTables.get(k).beneIdColumnName));
//                        System.out.println("ben: "+tableRS.getString(listOfTables.get(k).beneIdColumnName));
                            }
                            gson.toJson(row, jw);
                        }
                    }
                    //  }
                    //}


                    addAuditTrailRecords(reporter, jw, r.lastSyncDate, c1, appConfig, prevDateTime);
                }

            }
            jw.endArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportSubSetReallocationNew(ExportRequest r, JsonWriter jw, List<String> listVillages, Connection c1, AppConfig appConfig) {
        try {
            jw.beginArray();
            String prevDateTime = getPreviousDate(); //21Jun2022 Bindu get prev mid night date from curr date

            Statement stat1 = c1.createStatement();
            List<GetListOfTables> listOfTablesPrimary = getTableNamesForFetchPrimary(stat1);
            List<GetListOfTables> listOfTablesSecondary = getTableNamesForFetchSecondary(stat1);

            for (String strVillageID : listVillages) {
                String currentFetchBeneficiaryId = fetchPrimaryTableDataSubSet(stat1, jw, listOfTablesPrimary, strVillageID, prevDateTime, appConfig, r.lastSyncDate);
                String beneficiaryIds = fetchPrimaryTableDataBenIds(stat1, listOfTablesPrimary, strVillageID, prevDateTime, appConfig);
                if (beneficiaryIds != null && !beneficiaryIds.trim().isEmpty()) {
                    fetchSecondaryTableDataSubSet(stat1, jw, listOfTablesSecondary, strVillageID, beneficiaryIds, prevDateTime, appConfig, r);
                }
//                System.out.println(strVillageID+"Village Id: " + currentFetchBeneficiaryId+"  :  "+beneficiaryIds);
                addAuditTrailRecordsNew(strVillageID, jw, r.lastSyncDate, c1, appConfig, prevDateTime, stat1, beneficiaryIds, currentFetchBeneficiaryId);
            }
            jw.endArray();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<String> getReportersOrThrowReallocation(ExportRequest r, Connection c) throws Exception {
        String sb = "";
        ResultSet reportersRS = null;
        /*if (r.userType.equals("BC")) {
            sb = "Select distinct AutoId, userid from tblfacilitydetails where CC_Id = ? and UserId IN (Select UserId from tblinstusers where UserRole='ACHF'  and isDeactivated!=1 ) order by userid asc"; //06Jan2025 - Bindu - Add userid in select
            PreparedStatement ps1 = c.prepareStatement(sb);
            ps1.setString(1, r.mcId);
            reportersRS = ps1.executeQuery();
        } else if (r.userType.equals("PC")) {
            sb = "SELECT DISTINCT fd.AutoId FROM tblfacilitydetails fd" +
                    " JOIN tblinstusers iu" +
                    " ON fd.UserId = iu.UserId" +
                    " WHERE fd.MC_Id = ?" +
                    " AND iu.isValidated = 1 " +
                    "union " +
                    "select distinct CC_SysUID from tblfacilitydetails where MC_Id = ?";
            PreparedStatement ps1 = c.prepareStatement(sb);
            ps1.setString(1, r.mcId);
            ps1.setString(2, r.mcId);
            reportersRS = ps1.executeQuery();
        }*/ // 09Feb205 Bindu

//        09Feb2025 Bindu - Remove tblinstusers cond
        String strcolUserType = "";
        if (r.userType.equals("BC")) {
            strcolUserType = "CC_Id";
        } else if (r.userType.equals("PC")) {
            strcolUserType = "MC_Id";
        }
        sb = "Select distinct AutoId from tblfacilitydetails where " + strcolUserType + " = ?  order by AutoId asc";
        PreparedStatement ps1 = c.prepareStatement(sb);
        ps1.setString(1, r.mcId);
        reportersRS = ps1.executeQuery();

        List<String> reporters = new ArrayList<>();
        if (reportersRS != null && !reportersRS.isBeforeFirst()) {
            ExportResponse e = new ExportResponse();
            e.type = "/errors/empty-reporters";
            e.title = "You do not have any reporters";
            e.status = 500;
            e.detail = "Could not find reporters for given " + r.userType;
            e.instance = String.format("/export/%s/%s/%s", r.schemaName, r.lastSyncDate, r.mcId);
            throw new Exception(gson.toJson(e));
        }
        while (reportersRS.next()) {
            reporters.add(reportersRS.getString(1));
        }
        return reporters;
    }


    List<GetListOfTables> getTableNames(Statement statement) throws SQLException {

        List<GetListOfTables> listOfTablesPojo;


        //Loop to retrive all Primary tables
        ResultSet resultSetForPrimaryTables = statement.executeQuery("SELECT * FROM tbllistoftables ");


        listOfTablesPojo = new ArrayList<>();

        while (resultSetForPrimaryTables.next()) {

            String tblType = resultSetForPrimaryTables.getString("tblType");
            if (tblType.equalsIgnoreCase("Primary")) {
                GetListOfTables tablePojo = new GetListOfTables();
                String tblName = resultSetForPrimaryTables.getString("tblName");
                String villageColumnName = resultSetForPrimaryTables.getString("villageIdColumnName");
                String beneIdColumnName = resultSetForPrimaryTables.getString("BeneIdColumnName");

                tablePojo.setTableName(tblName);
                tablePojo.setVillageIdColumnName(villageColumnName);
                tablePojo.setBeneIdColumnName(beneIdColumnName);
                listOfTablesPojo.add(tablePojo);

            }
        }
        return listOfTablesPojo;
    }


    List<GetListOfTables> getTableNamesForFetchPrimary(Statement statement) throws SQLException {

        List<GetListOfTables> listOfTablesPojo;


        //Loop to retrive all Primary tables
        ResultSet resultSetForPrimaryTables = statement.executeQuery("SELECT * FROM tbllistoftables  where tbltype ='Primary' and  functionality like '%Fetch%' ");


        listOfTablesPojo = new ArrayList<>();

        while (resultSetForPrimaryTables.next()) {

            // String tblType = resultSetForPrimaryTables.getString("tblType");
//            if (tblType.equalsIgnoreCase("Primary")) {
            GetListOfTables tablePojo = new GetListOfTables();
            String tblName = resultSetForPrimaryTables.getString("tblName");
            String villageColumnName = resultSetForPrimaryTables.getString("villageIdColumnName");
            String beneIdColumnName = resultSetForPrimaryTables.getString("BeneIdColumnName");

            tablePojo.setTableName(tblName);
            tablePojo.setVillageIdColumnName(villageColumnName);
            tablePojo.setBeneIdColumnName(beneIdColumnName);
            tablePojo.setGroupby(resultSetForPrimaryTables.getString("groupby"));
            listOfTablesPojo.add(tablePojo);

//            }
        }
        return listOfTablesPojo;
    }

    List<GetListOfTables> getTableNamesForFetchSecondary(Statement statement) throws SQLException {

        List<GetListOfTables> listOfTablesPojo;


        //Loop to retrive all Primary tables
        ResultSet resultSetForPrimaryTables = statement.executeQuery("SELECT * FROM tbllistoftables  where tbltype ='Secondary' and  functionality like '%Fetch%' ");


        listOfTablesPojo = new ArrayList<>();

        while (resultSetForPrimaryTables.next()) {

            // String tblType = resultSetForPrimaryTables.getString("tblType");
//            if (tblType.equalsIgnoreCase("Primary")) {
            GetListOfTables tablePojo = new GetListOfTables();
            String tblName = resultSetForPrimaryTables.getString("tblName");
            String villageColumnName = resultSetForPrimaryTables.getString("villageIdColumnName");
            String beneIdColumnName = resultSetForPrimaryTables.getString("BeneIdColumnName");

            tablePojo.setTableName(tblName);
            tablePojo.setVillageIdColumnName(villageColumnName);
            tablePojo.setBeneIdColumnName(beneIdColumnName);
            tablePojo.setGroupby(resultSetForPrimaryTables.getString("groupby"));
            listOfTablesPojo.add(tablePojo);

//            }
        }
        return listOfTablesPojo;
    }


    private void exportFullReallocationNew(JsonWriter jw, List<String> listVillages, Connection c1, ExportRequest r, AppConfig appConfig) {
        try {
            Statement stat1 = c1.createStatement();
            jw.beginArray();
            String prevDateTime = getPreviousDate();

            List<GetListOfTables> listOfTablesPrimary = getTableNamesForFetchPrimary(stat1);
            List<GetListOfTables> listOfTablesSecondary = getTableNamesForFetchSecondary(stat1);

            for (String strVillageID : listVillages) {
                String beneficiaryIds = fetchPrimaryTableData(stat1, jw, listOfTablesPrimary, strVillageID, prevDateTime, appConfig);

                if (beneficiaryIds != null && !beneficiaryIds.trim().isEmpty()) {
                    fetchSecondaryTableData(stat1, jw, listOfTablesSecondary, strVillageID, beneficiaryIds, prevDateTime, appConfig, r);
                }
            }
            jw.endArray();
        } catch (Exception e) {
            LOGGER.error("exportFullReallocation", e);
            e.printStackTrace();
        }
    }

    // Fetch data from primary tables
    private String fetchPrimaryTableData(Statement stat1, JsonWriter jw, List<GetListOfTables> listOfTablesPrimary, String villageID, String prevDateTime, AppConfig appConfig) throws SQLException {
        String beneficiaryIds = "";

        for (GetListOfTables table : listOfTablesPrimary) {
            String tn = table.getTableName();
            String villageIdColumn = table.getVillageIdColumnName();
            String groupBy = table.getGroupby();
            String sql = buildPrimaryQuery(tn, villageIdColumn, villageID, prevDateTime, appConfig, groupBy);

            beneficiaryIds = executeQueryAndConvertToJson(stat1, sql, jw, tn, table.getBeneIdColumnName(), beneficiaryIds);
        }

        return beneficiaryIds;
    }

    // Fetch data from primary tables (Only retrieve Beneficiary IDs)
    private String fetchPrimaryTableDataBenIds(Statement stat1, List<GetListOfTables> listOfTablesPrimary, String villageID, String prevDateTime, AppConfig appConfig) throws SQLException {
        StringJoiner beneficiaryIdsJoiner = new StringJoiner("','", "'", "'");

        for (GetListOfTables table : listOfTablesPrimary) {
            String tn = table.getTableName();
            String villageIdColumn = table.getVillageIdColumnName();
            String groupBy = table.getGroupby();
            String sql = buildPrimaryQueryforBeneficiaryId(tn, villageIdColumn, villageID, appConfig, groupBy, table.getBeneIdColumnName());
//            System.out.println("SQL Query: " + sql);

            try (ResultSet tableRS = stat1.executeQuery(sql)) {
                while (tableRS.next()) {
                    String beneId = tableRS.getString(table.getBeneIdColumnName());
                    if (beneId != null && !beneId.trim().isEmpty()) {
                        beneficiaryIdsJoiner.add(beneId);
                    }
                }
            }
        }

        // Return collected Beneficiary IDs or null if empty
        return beneficiaryIdsJoiner.length() > 2 ? beneficiaryIdsJoiner.toString() : null;
    }

    // Fetch data from secondary tables
    private void fetchSecondaryTableData(Statement stat1, JsonWriter jw, List<GetListOfTables> listOfTablesSecondary, String villageID, String beneficiaryIds, String prevDateTime, AppConfig appConfig, ExportRequest r) throws SQLException {
        for (GetListOfTables table : listOfTablesSecondary) {
            String tn = table.getTableName();
            String groupBy = table.getGroupby();
            String sql = buildSecondaryQuery(tn, table.getBeneIdColumnName(), beneficiaryIds, villageID, prevDateTime, appConfig, r, groupBy);


            executeQueryAndConvertToJson(stat1, sql, jw, tn, null, null);
        }
    }

    // Build query for primary tables
    private String buildPrimaryQuery(String tn, String villageIdColumn, String villageID, String prevDateTime, AppConfig appConfig, String groupByColumn) {
        StringBuilder sqlBuilder = new StringBuilder();

        if (!appConfig.isTestingInternal) {
            sqlBuilder.append(String.format("SELECT * FROM %s WHERE %s = '%s' AND servercreateddate <= str_to_date('%s','%%Y-%%m-%%d %%H:%%i:%%s')", tn, villageIdColumn, villageID, prevDateTime));
        } else {
            sqlBuilder.append(String.format("SELECT * FROM %s WHERE %s = '%s'", tn, villageIdColumn, villageID));
        }

        // Add GROUP BY clause if a valid groupByColumn is provided
        if (groupByColumn != null && !groupByColumn.trim().isEmpty()) {
            sqlBuilder.append(" GROUP BY ").append(groupByColumn);
        }

        return sqlBuilder.toString();
    }

    // Build query for secondary tables
    private String buildSecondaryQuery(String tn, String beneIdColumnName, String beneficiaryIds, String villageID, String prevDateTime, AppConfig appConfig, ExportRequest r, String groupByColumn) {
        StringBuilder sqlBuilder = new StringBuilder();

        if (tn.equalsIgnoreCase("tblcasemgmt")) {
            sqlBuilder.append("SELECT * FROM ").append(tn)
                    .append(" WHERE (BeneficiaryID IN (Select womanId from tblregisteredwomen where regVillage = ").append(villageID).append(" ) OR ")
                    .append(" BeneficiaryID IN  (Select chlId from tblchildinfo where chlTribalHamlet = ").append(villageID).append(" ) OR ")
                    .append(" BeneficiaryID IN  (Select adolId from tbladolreg where regAdolFacilities = ").append(villageID).append(" ) ) ")
                    .append("AND hcmCreatedByUserType != '").append(r.userType).append("'");

            if (!appConfig.isTestingInternal) {
                sqlBuilder.append(" AND servercreateddate <= str_to_date('").append(prevDateTime).append("','%Y-%m-%d %H:%i:%s')");
            }
        } else if (tn.equalsIgnoreCase("tblchlparentdetails")) {
            if (!appConfig.isTestingInternal) {
                sqlBuilder.append("Select * from tblchlparentdetails where chlParentId IN (Select chlParentId from tblchildinfo where chlID IN (" + beneficiaryIds + "))").append(" AND servercreateddate <= str_to_date('").append(prevDateTime).append("','%Y-%m-%d %H:%i:%s')");
            } else {
                sqlBuilder.append("Select * from tblchlparentdetails where chlParentId IN (Select chlParentId from tblchildinfo where chlID IN (" + beneficiaryIds + "))");
            }
//            System.out.println(sqlBuilder);
        } else if (tn.equalsIgnoreCase("tblactivityjsonstore")) {
            if (!appConfig.isTestingInternal) {

                sqlBuilder.append("SELECT * FROM tblactivityjsonstore where UserId IN (Select userId from " + r.schemaName + ".tblfacilitydetails where (CC_Id ='" + r.mcId + "' or MC_Id = '" + r.mcId + "')) AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s') ");

            } else {

                sqlBuilder.append("SELECT * FROM tblactivityjsonstore where UserId IN (Select userId from " + r.schemaName + ".tblfacilitydetails where (CC_Id ='" + r.mcId + "' or MC_Id = '" + r.mcId + "')) ");

            }
//            System.out.println(sqlBuilder);
        } else {
            sqlBuilder.append("SELECT * FROM ").append(tn)
                    .append(" WHERE ").append(beneIdColumnName)
                    .append(" IN (").append(beneficiaryIds).append(")");

            if (!appConfig.isTestingInternal) {
                sqlBuilder.append(" AND servercreateddate <= str_to_date('").append(prevDateTime).append("','%Y-%m-%d %H:%i:%s')");
            }
        }

        // Add GROUP BY clause if a valid groupByColumn is provided
        if (groupByColumn != null && !groupByColumn.trim().isEmpty()) {
            sqlBuilder.append(" GROUP BY ").append(groupByColumn);
        }

        return sqlBuilder.toString();
    }

    // Execute SQL query and convert results to JSON
    private String executeQueryAndConvertToJson(Statement stat1, String sql, JsonWriter jw, String tn, String beneIdColumn, String beneficiaryIds) throws SQLException {
//        System.out.println("SQL Query: " + sql);

        ResultSet tableRS = stat1.executeQuery(sql);
        ResultSetMetaData rsm = tableRS.getMetaData();
        int columnCount = rsm.getColumnCount();

        while (tableRS.next()) {
            JsonObject row = convertResultSetToJson(tableRS, rsm, columnCount, tn);

            if (beneIdColumn != null) {
                String beneIdValue = tableRS.getString(beneIdColumn);
                if (beneficiaryIds != null && !beneficiaryIds.isEmpty()) {
                    beneficiaryIds += ",'" + beneIdValue + "'";
                } else {
                    beneficiaryIds = "'" + beneIdValue + "'";
                }
            }

            gson.toJson(row, jw);
        }

        return beneficiaryIds;
    }

    // Convert ResultSet row to JSON
    private JsonObject convertResultSetToJson(ResultSet tableRS, ResultSetMetaData rsm, int columnCount, String tn) throws SQLException {
        JsonObject row = new JsonObject();
        row.addProperty("tn", tn);

        for (int i = 1; i <= columnCount; i++) {
            String cn = rsm.getColumnName(i);
            if (schemaExclusions.columns.contains(cn.toLowerCase())) {
                continue;
            }

            int ct = rsm.getColumnType(i);
            boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
            if (numericVal) {
                row.addProperty(cn, tableRS.getInt(i));
            } else {
                row.addProperty(cn, tableRS.getString(i));
            }
        }

        return row;
    }

    //    27Feb2025 Arpitha - buildPrimary table data for second time and further fetch
    private String buildPrimaryQuerySubSet(String tn, String villageIdColumn, String villageID, String prevDateTime, AppConfig appConfig, String groupByColumn, String lastFetchDate) {
        StringBuilder sqlBuilder = new StringBuilder();

        if (!appConfig.isTestingInternal) {
            sqlBuilder.append(String.format("SELECT * FROM %s WHERE %s = '%s' AND servercreateddate > str_to_date('%s','%%Y-%%m-%%d %%H:%%i:%%s') AND servercreateddate <= str_to_date('%s','%%Y-%%m-%%d %%H:%%i:%%s')", tn, villageIdColumn, villageID, lastFetchDate, prevDateTime));
        } else {
            sqlBuilder.append(String.format("SELECT * FROM %s WHERE %s = '%s' AND servercreateddate > str_to_date('%s','%%Y-%%m-%%d %%H:%%i:%%s')", tn, villageIdColumn, villageID, lastFetchDate));

//        sqlBuilder.append(String.format("SELECT * FROM %s WHERE %s = '%s'", tn, villageIdColumn, villageID));
        }

        // Add GROUP BY clause if a valid groupByColumn is provided
        if (groupByColumn != null && !groupByColumn.trim().isEmpty()) {
            sqlBuilder.append(" GROUP BY ").append(groupByColumn);
        }

        return sqlBuilder.toString();
    }

    //  27Feb2025 Arpitha - secondary table data fetch for second and further fetch
    private String buildSecondaryQuerySubSet(String tn, String beneIdColumnName, String beneficiaryIds, String villageID, String prevDateTime, AppConfig appConfig, ExportRequest r, String groupByColumn) {
        StringBuilder sqlBuilder = new StringBuilder();

        if (tn.equalsIgnoreCase("tblcasemgmt")) {
            sqlBuilder.append("SELECT * FROM ").append(tn)
                    .append(" WHERE (BeneficiaryID IN (Select womanId from tblregisteredwomen where regVillage = ").append(villageID).append(" ) OR ")
                    .append(" BeneficiaryID IN  (Select chlId from tblchildinfo where chlTribalHamlet = ").append(villageID).append(" ) OR ")
                    .append(" BeneficiaryID IN  (Select adolId from tbladolreg where regAdolFacilities = ").append(villageID).append(" ) ) ")
                    .append("AND hcmCreatedByUserType != '").append(r.userType).append("'");

            if (!appConfig.isTestingInternal) {
                sqlBuilder.append(" AND servercreateddate > str_to_date('").append(r.lastSyncDate).append("','%Y-%m-%d %H:%i:%s')").append(" AND servercreateddate <= str_to_date('").append(prevDateTime).append("','%Y-%m-%d %H:%i:%s')");
            } else {
                sqlBuilder.append(" AND servercreateddate > str_to_date('").append(r.lastSyncDate).append("','%Y-%m-%d %H:%i:%s')");
            }
        } else if (tn.equalsIgnoreCase("tblchlparentdetails")) {
            if (!appConfig.isTestingInternal) {
                sqlBuilder.append("Select * from tblchlparentdetails where chlParentId IN (Select chlParentId from tblchildinfo where chlID IN (" + beneficiaryIds + "))").append(" AND servercreateddate <= str_to_date('").append(prevDateTime).append("','%Y-%m-%d %H:%i:%s')").append(" AND servercreateddate > str_to_date('").append(r.lastSyncDate).append("','%Y-%m-%d %H:%i:%s')");
            } else {
                sqlBuilder.append("Select * from tblchlparentdetails where chlParentId IN (Select chlParentId from tblchildinfo where chlID IN (" + beneficiaryIds + "))").append(" AND servercreateddate > str_to_date('").append(r.lastSyncDate).append("','%Y-%m-%d %H:%i:%s')");
            }
//            System.out.println(sqlBuilder);
        } else if (tn.equalsIgnoreCase("tblactivityjsonstore")) {
            if (!appConfig.isTestingInternal) {

                sqlBuilder.append("SELECT * FROM tblactivityjsonstore where UserId IN (Select userId from " + r.schemaName + ".tblfacilitydetails where (CC_Id ='" + r.mcId + "' or MC_Id = '" + r.mcId + "')) AND servercreateddate <= str_to_date('" + prevDateTime + "','%Y-%m-%d %H:%i:%s') AND servercreateddate > str_to_date('" + r.lastSyncDate + "','%Y-%m-%d %H:%i:%s')");

            } else {

                sqlBuilder.append("SELECT * FROM tblactivityjsonstore where UserId IN (Select userId from " + r.schemaName + ".tblfacilitydetails where (CC_Id ='" + r.mcId + "' or MC_Id = '" + r.mcId + "'))  AND servercreateddate > str_to_date('" + r.lastSyncDate + "','%Y-%m-%d %H:%i:%s')");

            }
//            System.out.println(sqlBuilder);
        } else {
            sqlBuilder.append("SELECT * FROM ").append(tn)
                    .append(" WHERE ").append(beneIdColumnName)
                    .append(" IN (").append(beneficiaryIds).append(")");

            if (!appConfig.isTestingInternal) {
                sqlBuilder.append(" AND servercreateddate > str_to_date('").append(r.lastSyncDate).append("','%Y-%m-%d %H:%i:%s')").append(" AND servercreateddate <= str_to_date('").append(prevDateTime).append("','%Y-%m-%d %H:%i:%s')");
            } else {
                sqlBuilder.append(" AND servercreateddate > str_to_date('").append(r.lastSyncDate).append("','%Y-%m-%d %H:%i:%s')");

            }
        }

        // Add GROUP BY clause if a valid groupByColumn is provided
        if (groupByColumn != null && !groupByColumn.trim().isEmpty()) {
            sqlBuilder.append(" GROUP BY ").append(groupByColumn);
        }

        return sqlBuilder.toString();
    }


    private String fetchPrimaryTableDataSubSet(Statement stat1, JsonWriter jw, List<GetListOfTables> listOfTablesPrimary, String villageID, String prevDateTime, AppConfig appConfig, String lastFetchDate) throws SQLException {
        String beneficiaryIds = "";

        for (GetListOfTables table : listOfTablesPrimary) {
            String tn = table.getTableName();
            String villageIdColumn = table.getVillageIdColumnName();
            String groupBy = table.getGroupby();
            String sql = buildPrimaryQuerySubSet(tn, villageIdColumn, villageID, prevDateTime, appConfig, groupBy, lastFetchDate);

            beneficiaryIds = executeQueryAndConvertToJson(stat1, sql, jw, tn, table.getBeneIdColumnName(), beneficiaryIds);
        }

        return beneficiaryIds;
    }

    // Fetch data from primary tables (Only retrieve Beneficiary IDs)
/*
    private String fetchPrimaryTableDataBenIds(Statement stat1, List<GetListOfTables> listOfTablesPrimary, String villageID, String prevDateTime, AppConfig appConfig) throws SQLException {
        StringJoiner beneficiaryIdsJoiner = new StringJoiner("','", "'", "'");

        for (GetListOfTables table : listOfTablesPrimary) {
            String tn = table.getTableName();
            String villageIdColumn = table.getVillageIdColumnName();
            String groupBy = table.getGroupby();
            String sql = buildPrimaryQuery(tn, villageIdColumn, villageID, prevDateTime, appConfig, groupBy);
            System.out.println("SQL Query: " + sql);

            try (ResultSet tableRS = stat1.executeQuery(sql)) {
                while (tableRS.next()) {
                    String beneId = tableRS.getString(table.getBeneIdColumnName());
                    if (beneId != null && !beneId.trim().isEmpty()) {
                        beneficiaryIdsJoiner.add(beneId);
                    }
                }
            }
        }

        // Return collected Beneficiary IDs or null if empty
        return beneficiaryIdsJoiner.length() > 2 ? beneficiaryIdsJoiner.toString() : null;
    }
*/

    // Fetch data from secondary tables
    private void fetchSecondaryTableDataSubSet(Statement stat1, JsonWriter jw, List<GetListOfTables> listOfTablesSecondary, String villageID, String beneficiaryIds, String prevDateTime, AppConfig appConfig, ExportRequest r) throws SQLException {
        for (GetListOfTables table : listOfTablesSecondary) {
            String tn = table.getTableName();
            String groupBy = table.getGroupby();
            String sql = buildSecondaryQuerySubSet(tn, table.getBeneIdColumnName(), beneficiaryIds, villageID, prevDateTime, appConfig, r, groupBy);


            executeQueryAndConvertToJson(stat1, sql, jw, tn, null, null);
        }
    }

    //    27Feb2025 Arpitha - new method to fetch edited data
    private String addAuditTrailRecordsNew(String reporterId, JsonWriter jw, String lastSyncDate, Connection c1, AppConfig appConfig, String prevDateTime, Statement stat1, String beneficiaryId, String currentFetchBeneficiaryId) throws SQLException {

        StringBuilder sqlBuilder = new StringBuilder();
        String strBeneficiaryId = "", strCurrentBeneficiaryId = "";

        if (currentFetchBeneficiaryId != null && currentFetchBeneficiaryId.trim().length() > 0)
            strCurrentBeneficiaryId = " and WomanId NOT IN (" + currentFetchBeneficiaryId + ")";

        if (beneficiaryId != null && beneficiaryId.trim().length() > 0) {
            strBeneficiaryId = " and WomanId IN (" + beneficiaryId + ")";

            if (!appConfig.isTestingInternal) { //21Jun2022 Bindu Add servercreateddate < prev date

                sqlBuilder = sqlBuilder.append("Select * from tblaudittrail where  ServerReceivedDate >  str_to_date('" + lastSyncDate + "' ,'%Y-%m-%d %H:%i:%s') and ServerReceivedDate <= str_to_date('" + prevDateTime + "' ,'%Y-%m-%d %H:%i:%s') and TblName IN ('tblchildinfo','tbldeliveryinfo','tblregisteredwomen','tbladolreg','tblEligibleCoupleReg','tblActivityJsonStore')  " + strBeneficiaryId + strCurrentBeneficiaryId);
            } else {
                sqlBuilder = sqlBuilder.append("Select * from tblaudittrail where  ServerReceivedDate >  str_to_date('" + lastSyncDate + "' ,'%Y-%m-%d %H:%i:%s')   and TblName IN ('tblchildinfo','tbldeliveryinfo','tblregisteredwomen','tbladolreg','tblEligibleCoupleReg','tblActivityJsonStore')   " + strBeneficiaryId + strCurrentBeneficiaryId);

            }


            executeQueryAndConvertToJson(stat1, sqlBuilder.toString(), jw, "tblaudittrail", "WomanId", null);


        }

        return sqlBuilder.toString();
    }

    private String buildPrimaryQueryforBeneficiaryId(String tn, String villageIdColumn, String villageID, AppConfig appConfig, String groupByColumn, String beneficiaryIdColumnName) {
        StringBuilder sqlBuilder = new StringBuilder();

        if (!appConfig.isTestingInternal) {
            sqlBuilder.append(String.format("SELECT " + beneficiaryIdColumnName + " FROM %s WHERE %s = '%s'", tn, villageIdColumn, villageID));
        } else {
            sqlBuilder.append(String.format("SELECT " + beneficiaryIdColumnName + " FROM %s WHERE %s = '%s'", tn, villageIdColumn, villageID));
        }

        // Add GROUP BY clause if a valid groupByColumn is provided
        if (groupByColumn != null && !groupByColumn.trim().isEmpty()) {
            sqlBuilder.append(" GROUP BY ").append(groupByColumn);
        }

        return sqlBuilder.toString();
    }
}
