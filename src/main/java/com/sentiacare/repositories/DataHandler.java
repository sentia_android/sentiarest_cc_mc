package com.sentiacare.repositories;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sentiacare.dtos.ExportRequest;
import com.sentiacare.dtos.FetchRequest;
import com.sentiacare.entities.*;
import com.sentiacare.services.DBOpsStatus;
import com.sentiacare.services.FetchStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.StringWriter;
import java.sql.*;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataHandler {
    private static final Logger LOGGER = LogManager.getLogger(DataHandler.class);

    private final SentiaPooledDataSource ds;

    private DocumentBuilder documentBuilder;
    private Transformer transformer;

    /*
     * Used by marshallToXMLString
     * */
    DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:s");

    public DataHandler(SentiaPooledDataSource ds) {
        this.ds = ds;
    }

    public void initDOMToStringTransformer() throws TransformerConfigurationException {
        TransformerFactory tf = TransformerFactory.newInstance();
        transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "no");
    }


    public String getData(String query, String dbName) {
        String xmlString = "";

        Connection connection = null;
        try {
            connection = ds.getConnection(dbName);
            Statement stat = connection.createStatement();
            ResultSet resultSet = stat.executeQuery(query);

            xmlString = marshallToXMLString(resultSet);
        } catch (SQLException e) {
            LOGGER.error("getData failed", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error closing database connection", e);
                }
            }
        }
        return xmlString;
    }


    private String marshallToXMLString(ResultSet resultSet) {
        String xml = "";
        try {
            if (documentBuilder == null) {
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                documentBuilder = builderFactory.newDocumentBuilder();
            }
            Document document = documentBuilder.newDocument();
            Element rootElement = document.createElement("NewDataSet");
            document.appendChild(rootElement);

            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            while (resultSet.next()) {
                Element table = document.createElement("Table");
                rootElement.appendChild(table);

                for (int i = 1; i <= columnCount; i++) {
                    String columnName = metaData.getColumnName(i);
                    String columnTypName = metaData.getColumnTypeName(i);
                    Object value = resultSet.getObject(i);

                    if (columnTypName.equalsIgnoreCase("datetime") ||
                            columnTypName.equalsIgnoreCase("timestamp")) {
                        Timestamp t = (Timestamp) value;
                        value = sdf.format(t.toLocalDateTime());
                    }


                    Element node = document.createElement(columnName.replaceAll("\\s", ""));
                    node.appendChild(document.createTextNode(value == null ? "" : value.toString()));
                    table.appendChild(node);
                }
            }
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(writer));
            xml = writer.toString();
        } catch (SQLException | TransformerException | ParserConfigurationException e) {
            LOGGER.error("Error converting ResultSet to XML String", e);
        } finally {
            assert documentBuilder != null;
            documentBuilder.reset();
        }
        return xml;
    }

    public void putData(String insert, String dbName) {
        Connection connection = null;
        try {
            connection = ds.getConnection(dbName);
            Statement stat = connection.createStatement();
            stat.execute(insert);
        } catch (SQLException e) {
            LOGGER.error(" putData failed", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error closing database connection", e);
                }
            }
        }
    }


    public Nurse getNurse(String AshaId, String password, String dbName) {
        Nurse n = null;
        Connection connection = null;
        try {
            connection = ds.getConnection(dbName);
            Statement stat = connection.createStatement();
            String query = "Select UserId, isValidated,UserMobileNumber from tblinstusers where AshaId = '" + AshaId + "' and Password = '" + password + "'";
            ResultSet resultSet = stat.executeQuery(query);
            while (resultSet.next()) {
                n = new Nurse();
                n.userId = resultSet.getString(1);
                n.validated = resultSet.getInt(2);
                n.UserMobileNumber = resultSet.getInt(3);
                n.AshaId = AshaId;
                n.password = password;
            }
        } catch (SQLException e) {
            LOGGER.error("getNurse failed", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error closing database connection", e);
                }
            }
        }
        return n;
    }


    public boolean institutionsExist(String dbName) {
        boolean exist = false;
        Connection connection = null;
        try {
            connection = ds.getConnection(dbName);
            Statement stat = connection.createStatement();
            ResultSet resultSet = stat.executeQuery("SELECT COUNT(InstituteId) FROM tblinstitutiondetails where isDeactivated = 0");
            if (resultSet.isBeforeFirst()) {
                exist = true;
            }
        } catch (Exception e) {
            LOGGER.error("institutionsExist failed", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error closing database connection", e);
                }
            }
        }
        return exist;
    }

    public AshaWorker get(String mobileNumber, String password, String dbName) {
        AshaWorker asha = null;
        Connection connection = null;
        try {
            connection = ds.getConnection(dbName);
            String queryBuilder = "SELECT UserId, istablost, isValidated, isDeactivated,Password  FROM tblinstusers WHERE" +
                    " UserMobileNumber = ?" +
                    " AND " +
                    " Password = ?";
            PreparedStatement stat = connection.prepareStatement(queryBuilder);
            stat.setString(1, mobileNumber);
            stat.setString(2, password);
            ResultSet resultSet = stat.executeQuery();
            if (resultSet.next()) {
                asha = new AshaWorker();
                asha.UserId = resultSet.getString(1);
                asha.isTabLost = resultSet.getInt(2) == 1;
                asha.isValidated = resultSet.getInt(3) == 1;
                asha.isDeactivated = resultSet.getInt(4) == 1;
                asha.password = resultSet.getString(5).equals(password);
            }
        } catch (SQLException e) {
            LOGGER.error("Retrieving record for given ashaId failed", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error closing database connection", e);
                }
            }
        }
        return asha;
    }

    public List<String> getDuplicateRegistrations(String thayiID, String dbName) {
        List<String> duplicates = new ArrayList<>(5);
        Connection connection = null;
        try {
            connection = ds.getConnection(dbName);
            String queryBuilder = "SELECT WomanId FROM tblregistration WHERE"
                    + " regPhone = ? OR regThayiCard = ? OR regAadhar = ?";
            PreparedStatement stat = connection.prepareStatement(queryBuilder, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            stat.setString(1, thayiID);
            stat.setString(2, thayiID);
            stat.setString(3, thayiID);

            ResultSet resultSet = stat.executeQuery();
            resultSet.first();
            do {
                duplicates.add(resultSet.getString(1));
            } while (resultSet.next());
        } catch (SQLException e) {
            LOGGER.error("getDuplicateRegistrations failed", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error closing database connection", e);
                }
            }
        }
        return duplicates;
    }

   /* *//*
     * @author Gururaja 04 Jun 2021 12:36
     * Remember, this method obtains two connections - connection with autoCommit = false
     * and connection1 with autoCommit = true. Must close both in finally to release back to Hikari pool
     *
     * TODO do something about woman profile picture?
     * *//*
    public DBOpsStatus executeSQLQueries(List<String> queries, String requestId, String dbName) throws Exception {
        DBOpsStatus dbOpsStatus;

        Connection connection, connection1;
        try {
            connection = ds.getConnection(dbName);
            connection1 = ds.getConnection(dbName);
        } catch (SQLException ex) {
            throw new Exception(String.format("Could not get connection to schema %s", dbName), ex);
        }

        updateLogToTransactionTable(connection1, requestId, DBStatus.ConnectionOpen, MsgStatus.Success);
        updateLogToTransactionTable(connection1, requestId, DBStatus.TransactionBegin, MsgStatus.Success);

        connection.setAutoCommit(false);

        boolean blnQueryPassed = true;
        for (String query : queries) {
            int retVal = putData(query, connection, requestId);
            if (retVal == 0) {
                if (query.toLowerCase().contains("insert")) {
                    updateLogToTransactionTable(connection1, requestId, DBStatus.Insert, MsgStatus.Failure);
                } else if (query.toLowerCase().contains("update")) {
                    updateLogToTransactionTable(connection1, requestId, DBStatus.Update, MsgStatus.Failure);
                } else if (query.toLowerCase().contains("delete")) {
                    updateLogToTransactionTable(connection1, requestId, DBStatus.Delete, MsgStatus.Failure);
                }
                blnQueryPassed = false;
                break;
            } else {
                if (query.toLowerCase().contains("insert")) {
                    updateLogToTransactionTable(connection1, requestId, DBStatus.Insert, MsgStatus.Success);
                } else if (query.toLowerCase().contains("update")) {
                    updateLogToTransactionTable(connection1, requestId, DBStatus.Update, MsgStatus.Success);
                } else if (query.toLowerCase().contains("delete")) {
                    updateLogToTransactionTable(connection1, requestId, DBStatus.Delete, MsgStatus.Success);
                }
            }
        }
        if (blnQueryPassed) {
            connection.commit();
            updateLogToTransactionTable(connection1, requestId, DBStatus.TransactionCommit, MsgStatus.Success);
            LOGGER.info("Committed DB Transaction Successfully");
            dbOpsStatus = new DBOpsStatus(DBStatus.TransactionCommit, true);
        } else {
            connection.rollback();
            updateLogToTransactionTable(connection1, requestId, DBStatus.TransactionRollback, MsgStatus.Success);
            LOGGER.info("Rolled Back DB Transaction Successfully");
            dbOpsStatus = new DBOpsStatus(DBStatus.TransactionRollback, true);
        }
        updateLogToTransactionTable(connection1, requestId, DBStatus.ConnectionClose, MsgStatus.Success);
        connection.setAutoCommit(true);
        connection.close();
        connection1.close();
        return dbOpsStatus;
    }*/

    public DBOpsStatus doBatch(List<BatchItem> batchItems, String requestId, String dbName) throws Exception {
        if (batchItems.isEmpty()) {
            throw new Exception(String.format("No insert/update statements found in xml. Request Id %s - Schema name %s", requestId, dbName));
        }
        Connection connection;
        try {
            connection = ds.getConnection(dbName);
        } catch (SQLException ex) {
            throw new Exception(String.format("Could not get connection to schema %s", dbName), ex);
        }

        updateLogToTransactionTable(connection, requestId, DBStatus.ConnectionOpen, MsgStatus.Success);
        updateLogToTransactionTable(connection, requestId, DBStatus.TransactionBegin, MsgStatus.Success);

        connection.setAutoCommit(false);

        Statement s = connection.createStatement();
        for (BatchItem batchItem : batchItems) {
            s.addBatch(batchItem.statement);
        }
        boolean canCommit = true;
        int[] updateCounts;
        try {
            updateCounts = s.executeBatch();
        } catch (BatchUpdateException e) {
            canCommit = false;
            updateCounts = e.getUpdateCounts();
        }
        if (canCommit) {
            connection.commit();
        } else {
            connection.rollback();
        }
        new Thread(new DoTheRest(this, s, batchItems, requestId, canCommit, updateCounts)).start();
        return new DBOpsStatus(canCommit ? DBStatus.TransactionCommit : DBStatus.TransactionRollback, canCommit);
    }

   /* public String getSQL(String requestId, StatementType statementType, MsgStatus status) {
        Date date = new Date();
        String logDateTime = DateFormat.getDateInstance(DateFormat.SHORT).format(date) + "_"
                + DateFormat.getTimeInstance(DateFormat.SHORT).format(date);
        return "Insert into trantable(requestid, transid, action, comment, logdatetime, filestatus, dbstatus, msgstatus) values ('"
                + requestId + "','" + "" + "','" + "" + "','" + "" + "','" + logDateTime + "','"
                + "" + "','" + statementType.name() + "','" + status.name() + "')";
    }*/

    /*public String getSQL(String requestId, DBStatus dbStatus, MsgStatus msgStatus) {
        Date date = new Date();
        String logDateTime = DateFormat.getDateInstance(DateFormat.SHORT).format(date) + "_"
                + DateFormat.getTimeInstance(DateFormat.SHORT).format(date);
        return "Insert into trantable(requestid, transid, action, comment, logdatetime, filestatus, dbstatus, msgstatus) values ('"
                + requestId + "','" + "" + "','" + "" + "','" + "" + "','" + logDateTime + "','"
                + "" + "','" + dbStatus.name() + "','" + msgStatus.name() + "')";
    }*/

    private void updateLogToTransactionTable(Connection connection, String requestId, DBStatus dbStatus, MsgStatus msgStatus) {
        Date date = new Date();
        String logDateTime = DateFormat.getDateInstance(DateFormat.SHORT).format(date) + "_"
                + DateFormat.getTimeInstance(DateFormat.SHORT).format(date);
        String sqlstmt = "Insert into trantable(requestid, transid, action, comment, logdatetime, filestatus, dbstatus, msgstatus) values ('"
                + requestId + "','" + "" + "','" + "" + "','" + "" + "','" + logDateTime + "','"
                + "" + "','" + dbStatus.name() + "','" + msgStatus.name() + "')";
        putData(sqlstmt, connection, requestId);
    }

    public int putData(String sQuery, Connection connection, String requestId) {
        try (PreparedStatement ps = connection.prepareStatement(sQuery)) {
            ps.executeUpdate();
            return 1;
        } catch (SQLException e) {
            LOGGER.error("Could not execute statement `{}` for Request Id {}", sQuery, requestId, e);
            return 0;
        }
    }

    public void markFileErrorWithRollback(String path, String schemaName) {
        Connection connection = null;
        String requestId = path.substring(0, path.lastIndexOf('.'));
        try {
            Date date = new Date();
            String logDateTime = DateFormat.getDateInstance(DateFormat.SHORT).format(date) + "_"
                    + DateFormat.getTimeInstance(DateFormat.SHORT).format(date);
            String sqlstmt = "Insert into trantable(requestid, transid, action, comment, logdatetime, filestatus, dbstatus, msgstatus) values ('"
                    + requestId + "','"
                    + "" + "','"
                    + "" + "','"
                    + String.format("File Error from sync-cron for file %s", path) + "','"
                    + logDateTime + "','"
                    + "" + "','"
                    + DBStatus.TransactionRollback.name() + "','"
                    + MsgStatus.Success.name()
                    + "')";
            connection = ds.getConnection(schemaName);
            putData(sqlstmt, connection, requestId);

            sqlstmt = "Insert into trantable(requestid, transid, action, comment, logdatetime, filestatus, dbstatus, msgstatus) values ('"
                    + requestId + "','"
                    + "" + "','"
                    + "" + "','"
                    + String.format("File Error from sync-cron for file %s", path) + "','"
                    + logDateTime + "','"
                    + "" + "','"
                    + DBStatus.ConnectionClose.name() + "','"
                    + MsgStatus.Success.name()
                    + "')";
            putData(sqlstmt, connection, requestId);
        } catch (SQLException e) {
            LOGGER.error("Could not mark `file error` for file {}", path, e);
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException ex) {
                LOGGER.error("Could not either set autoCommit to true or close connection after marking file error for file {}", path, ex);
            }
        }
    }

    public SentiaPooledDataSource getDataSource() {
        return ds;
    }

    public void createFetchInitRecord(SentiaPooledDataSource ds, File f, FetchRequest r, ExportRequest exportRequest, String mtdName) throws Exception{
        FullSchemaRepository fullSchemaRepository = new FullSchemaRepository(ds);
        Connection conn = ds.getConnection(r.schemaName);
        String clientDbName = fullSchemaRepository.getOrThrow(exportRequest, conn);
        conn.close();
        conn = ds.getConnection(clientDbName);

        LocalDateTime localTime = LocalDateTime.now();
        String currentTime = sdf.format(localTime);
        String fileName = f.getName();
        String sql = "";
        sql = "Insert into tblserverdatafetchlog (userId,userType,requestId,requestSentAt,requestReceivedAt,restMethodName,fileName,fileGeneratedAt,status,lastFetchDateRequest) values ("+
                "'"+r.userId+"', "+
                "'"+r.userType+"', "+
                "'"+r.requestId+"', "+
                "'"+r.sendAt+"', "+
                "'"+currentTime+"', "+
                /*"'fetchV2', "+*/
                "'"+mtdName+"', "+  //09Feb2025 Bindu - Pass from mainmtd
                "'"+fileName+"', "+
                "'"+currentTime+"', "+
                "'"+ FetchStatus.Initiated.name() +"',"+
                "'"+r.lastSyncDate+"'); ";

        PreparedStatement statement = conn.prepareStatement(sql);
        statement.execute();
        conn.close();
    }
    public tblServerDataFetchLog getDataofFile(SentiaPooledDataSource ds, String requestId, String userId, String dbName, ExportRequest exportRequest){
        tblServerDataFetchLog log = new tblServerDataFetchLog();
        try{
            FullSchemaRepository fullSchemaRepository = new FullSchemaRepository(ds);
            Connection conn = ds.getConnection(dbName);
            String clientDbName = fullSchemaRepository.getOrThrow(exportRequest, conn);
            conn.close();
            conn = ds.getConnection(clientDbName);

            String sql = "Select * from tblserverdatafetchlog where userId = ? and requestId = ? ;";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,userId);
            statement.setString(2,requestId);
            ResultSet res = statement.executeQuery();
            if(res.next()){
                log.setFileSize(res.getString("fileSize"));
                log.setFileName(res.getString("fileName"));
            }
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return  log;
    }

    //ramesh 26-jun-2022
    public String getOrThrow(Connection c,String userId) throws Exception {
        PreparedStatement ps0 = c.prepareStatement("SELECT Inst_Database FROM tblinstusers WHERE UserMobileNumber = ?");
        ps0.setString(1, userId);
        ResultSet rs0 = ps0.executeQuery();
        if (!rs0.isBeforeFirst()) {
            throw new Exception("You are not registered with an institution");
        }
        rs0.next();
        return rs0.getString(1);
    }

    public void createTabLostrecord(TablostRequest r, File tablostFile) {
        Connection conn ;
        try{
            conn = ds.getConnection(r.clientDB);
            LocalDateTime localTime = LocalDateTime.now();
            String currentTime = sdf.format(localTime);
            String fileName = tablostFile.getName();
            String sql;
            sql = "Insert into tblserverdatafetchlog (userId,userType,requestId,requestSentAt,requestReceivedAt,restMethodName,fileName,fileGeneratedAt,status,lastFetchDateRequest) values ("+
                    "'"+r.userId+"', "+
                    "'"+r.userType+"', "+
                    "'"+r.requestId+"', "+
                    "'"+r.sendAt+"', "+
                    "'"+currentTime+"', "+
                    "'Tablost', "+
                    "'"+fileName+"', "+
                    "'"+currentTime+"', "+
                    "'"+ FetchStatus.Initiated.name() +"',"+
                    "'null'); ";

            PreparedStatement statement = conn.prepareStatement(sql);
            statement.execute();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getDataAsJson(String query, String dbName,String tn) {
        String xmlString = "";

        Connection connection = null;
        try {
            connection = ds.getConnection(dbName);
            Statement stat = connection.createStatement();
            ResultSet resultSet = stat.executeQuery(query);

            xmlString = marshallToJsonString(resultSet,tn);
        } catch (Exception e) {
            LOGGER.error("getData failed", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error closing database connection", e);
                }
            }
        }
        return xmlString;
    }

    private String marshallToJsonString(ResultSet resultSet,String tn) throws Exception{
        ResultSetMetaData rsm = resultSet.getMetaData();
        int columnCount = rsm.getColumnCount();
        JsonArray jsonArray = new JsonArray();
        while (resultSet.next()) {
            JsonObject row = new JsonObject();
            row.addProperty("tn", tn);
            for (int i = 1; i <= columnCount; i++) {
                String cn = rsm.getColumnName(i);
                int ct = rsm.getColumnType(i);
                boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                if (numericVal) {
                    row.addProperty(cn, resultSet.getInt(i));
                } else {
                    row.addProperty(cn, resultSet.getString(i));
                }
            }
            jsonArray.add(row);
//            s.append(row);
        }
        return jsonArray.toString();
    }


}
