package com.sentiacare.repositories;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MyDataSourceWrapper {

    private static MyDataSourceWrapper ds;

    private static final Map<String, HikariDataSource> dataSourceForName = new HashMap<>();

    public static MyDataSourceWrapper getInstance() {
        if(ds == null) {
            ds = new MyDataSourceWrapper();
        }
        return ds;
    }

    public synchronized Connection getConnection(String schemaName) throws SQLException {
        HikariDataSource hds = dataSourceForName.get(schemaName);
        if(hds == null) {
            HikariConfig hikariConfig = new HikariConfig();
            hikariConfig.setUsername("root");
            hikariConfig.setPassword("bc@AS!21O");
            hikariConfig.setJdbcUrl("jdbc:mysql://localhost:3306/" + schemaName);
            hikariConfig.setMaximumPoolSize(10);
            hikariConfig.setMaxLifetime(28500000); // 7.9 hours < 8 hours (default) set for MySQL idle_timeout

            hds = new HikariDataSource(hikariConfig);
            dataSourceForName.put(schemaName, hds);
        }
        return hds.getConnection();
    }
}
