package com.sentiacare.repositories;

import com.sentiacare.entities.BatchItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class DoTheRest2 implements Runnable {

    private final Logger LOGGER = LogManager.getLogger(DoTheRest2.class);

    public final List<BatchItem> batchItems;
    public final String requestId;

    public DoTheRest2(List<BatchItem> batchItems, String requestId) {
        this.batchItems = batchItems;
        this.requestId = requestId;
    }

    @Override
    public void run() {
        List<String> statementsNotExecuted = new ArrayList<>();
        for (int i = 0; i < batchItems.size(); i++) {
            statementsNotExecuted.add(batchItems.get(i).statement);
        }
        if (statementsNotExecuted.isEmpty()) {
            return;
        }
        String result = String.join("|", statementsNotExecuted);
        LOGGER.error("TransactionRollback, statements for requestId {} could not be executed. Statement list: {}", requestId, result);
    }
}
