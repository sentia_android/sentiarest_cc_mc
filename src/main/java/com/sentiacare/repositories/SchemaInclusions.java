package com.sentiacare.repositories;

import java.util.*;

public class SchemaInclusions {

    public List<String> tables = new ArrayList<>(Arrays.asList(
            "tblchildinfo",
            "tblchildvisitdetails",
            "tblchlparentdetails",
           // "tblcomplicationmgmt",
            "tbldeliveryinfo",
           // "tblhrpcomplicationmgmt",
            "tblregisteredwomen",
            "tblvisitchildheader",
            "tblvisitdetails",
            "tblvisitheader",
            "tbladolreg",
            "tbladolvisitheader",
            "tbladolpregnant",
            "tblcovidtestdetails",
            "tblcovidvaccinedetails",
            "tblchildgrowth",
            "tblcasemgmt",
            "tbluseractivity",
            "tblimagestore",
            "tblstocksupplytracker",
//            "tblinstusers", ramesh now we are getting in after login
            "tblbirthpreparedness",
            "tbleligiblecouplereg",
            "tblechomevisit",
            "tblotheractivity",
            "tblcomplicationmanagement",
            "tblrecoverystatus",
            "tblmortalityreg",
            "tblactivityjsonstore",
            "tbladolcounselling"
    ));

    public Map<String, String> groupByForTable = new HashMap<>();

    {
        groupByForTable.put("tblchildinfo", "GROUP BY chlId, transId ");
        groupByForTable.put("tblchildvisitdetails", "GROUP BY ChildId, VisitNum, visDSymptoms ");
        groupByForTable.put("tblchlparentdetails", "GROUP BY chlParentId, transId ");
      //  groupByForTable.put("tblcomplicationmgmt", "GROUP BY WomanId, VisitNum ");
        groupByForTable.put("tbldeliveryinfo", "GROUP BY WomanId, transId ");
       // groupByForTable.put("tblhrpcomplicationmgmt", "GROUP BY WomanId,VisitNum ");
        groupByForTable.put("tblregisteredwomen", "GROUP BY WomanId ");
        groupByForTable.put("tblvisitchildheader", "GROUP BY WomanId,ChildId, VisitNum ");
        groupByForTable.put("tblvisitdetails", "GROUP BY WomanId, VisitNum,visDSymptoms ");
        groupByForTable.put("tblvisitheader", "GROUP BY WomanId,VisitNum ");
        groupByForTable.put("tbladolreg", "GROUP BY adolId ");
        groupByForTable.put("tbladolvisitheader", "GROUP BY AdolId, VisitId ");
        groupByForTable.put("tbladolpregnant", "GROUP BY AdolId, pregnantCount ");
        groupByForTable.put("tbladolvisitdetails", "GROUP BY AdolId, VisitId ");
        groupByForTable.put("tblcovidtestdetails", "GROUP BY WomenId, VisitNum");
        groupByForTable.put("tblcovidvaccinedetails", "GROUP BY BeneficiaryId, CovidVaccinatedNo");
        groupByForTable.put("tblchildgrowth", "GROUP BY chlId, chlGMVisitId");
        groupByForTable.put("tblmansimitratracker", "");
        groupByForTable.put("tblimagestore", "GROUP BY ImageName,transId");//07Feb2025 Arpitha - to retrieve distinct data
        groupByForTable.put("tblbirthpreparedness", " GROUP BY WomanId, bpVisitDate "); //Ramesh 8-6-2022
        groupByForTable.put("tbleligiblecouplereg", " GROUP BY CoupleId ");
        groupByForTable.put("tblechomevisit", " GROUP BY CoupleId, cplVisId ");
        groupByForTable.put("tblotheractivity", "");
        groupByForTable.put("tblComplicationManagement", " GROUP BY compVisitNum, beneficiaryId ");
        groupByForTable.put("tblmortalityreg", " GROUP BY mortBeneficiaryId ");
        groupByForTable.put("tblactivityjsonstore", " GROUP BY activityId "); //18July2023 - Rakesh
        groupByForTable.put("tbladolcounselling", " GROUP BY UserId, adolId , counId ");

    }

    public List<String> editableTables = new ArrayList<>(Arrays.asList(
            "tblchildinfo",
            "tbldeliveryinfo",
            "tblregisteredwomen",
            "tbladolreg",
            "tblEligibleCoupleReg",
            "tblActivityJsonStore"
    ));
}
