package com.sentiacare.repositories;

import java.util.ArrayList;
import java.util.List;

public class SchemaExclusions {

    public final List<String> tables = new ArrayList<>();
    public final List<String> columns = new ArrayList<>();

    {
        tables.add("tbldisplaymaster");
        tables.add("tblm_usermaster");
        tables.add("tbldisplaynames");
        tables.add("tblm_loginaudit");
        tables.add("tblashachildimz");
        tables.add("trantable");
        tables.add("tblreportusers");
        tables.add("tblashatracker");
        tables.add("tblservicestypemaster");
        tables.add("tblm_userfacility");
        tables.add("tblfetchdetails");
        tables.add("tblmansimitratracker"); //Bindu add tblmansimitratracker
        columns.add("regjsy");
        columns.add("vishstatus");
//        columns.add("regwomenimage");
        columns.add("serviceorderid");
        columns.add("imagepath");
        columns.add("servercreateddate");
        columns.add("serverupdateddate");
        columns.add("serverreceiveddate");
        columns.add("modulestobeenabled");
        columns.add("phcname");
        columns.add("scname");
        columns.add("panchayathname");
//      exclude columns for fetch from tblregisteredwomen - 04Mar2025
        columns.add("regprematurebirth");
        columns.add("regprematurebirth");
        columns.add("regnotinregionreason");
        columns.add("reggpsphoto");
        columns.add("reggpslocation");
        columns.add("regfacilitytype");
        columns.add("reglmpnotknown");
        columns.add("regcategory");
        columns.add("regnickname");
    }
}
