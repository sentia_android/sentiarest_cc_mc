package com.sentiacare.repositories;

import com.sentiacare.entities.BatchItem;
import com.sentiacare.entities.DBStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.EXECUTE_FAILED;
import static java.sql.Statement.SUCCESS_NO_INFO;

public class DoTheRest implements Runnable {

    private final Logger LOGGER = LogManager.getLogger(DoTheRest.class);

   /* private final DataHandler dataHandler;
    private final Statement s;*/
    private final List<BatchItem> batchItems;
    private final String requestId;
    private final boolean canCommit;
    private final int[] updateCounts;

    public DoTheRest(DataHandler dataHandler, Statement s, List<BatchItem> batchItems, String requestId, boolean canCommit, int[] updateCounts) {
       /* this.dataHandler = dataHandler;
        this.s = s;*/
        this.batchItems = batchItems;
        this.requestId = requestId;
        this.canCommit = canCommit;
        this.updateCounts = updateCounts;
    }

//    23Jan2023 Arpitha
    public DoTheRest(List<BatchItem> batchItems, String requestId, boolean canCommit, int[] updateCounts) {
        this.batchItems = batchItems;
        this.requestId = requestId;
        this.canCommit = canCommit;
        this.updateCounts = updateCounts;
    }

    @Override
    public void run() {
        List<String> statementsNotExecuted = new ArrayList<>();
        for (int i = 0; i < batchItems.size(); i++) {
            int updateCount = updateCounts[i];
            if (updateCount == EXECUTE_FAILED) {
                statementsNotExecuted.add(batchItems.get(i).statement);
            }
        }
        if (statementsNotExecuted.isEmpty()) {
            return;
        }
        String result = String.join("|", statementsNotExecuted);
        LOGGER.error("TransactionRollback, statements for requestId {} could not be executed. Statement list: {}", requestId, result);
    }/*{
        try {
            doIt();
        } catch (SQLException e) {
            if (e instanceof BatchUpdateException) {
                LOGGER.error("Could not write statements to trantable. Request Id {}", requestId, e);
            } else {
                LOGGER.error(e);
            }
        } finally {
            try {
                Connection c = s.getConnection();
                c.setAutoCommit(true);
                c.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
    }*/

    /*private void doIt() throws SQLException {
        List<String> statementsNotExecuted = new ArrayList<>();
        s.clearBatch();
        for (int i = 0; i < batchItems.size(); i++) {
            int updateCount = updateCounts[i];
            MsgStatus status = MsgStatus.Success;
            if (updateCount == SUCCESS_NO_INFO || updateCount >= 0) { // Not required because of above assignment
                status = MsgStatus.Success;
            } else if (updateCount == EXECUTE_FAILED) {
                status = MsgStatus.Failure;
                statementsNotExecuted.add(batchItems.get(i).statement);
            }
            s.addBatch(dataHandler.getSQL(requestId, batchItems.get(i).statementType, status));
        }
        if (canCommit) {
            s.addBatch(dataHandler.getSQL(requestId, DBStatus.TransactionCommit, MsgStatus.Success));
        } else {
            s.addBatch(dataHandler.getSQL(requestId, DBStatus.TransactionRollback, MsgStatus.Success));
        }
        s.addBatch(dataHandler.getSQL(requestId, DBStatus.ConnectionClose, MsgStatus.Success));
        s.executeBatch();

        if(statementsNotExecuted.isEmpty()) {
            return;
        }
        String result = String.join("|", statementsNotExecuted);
        LOGGER.warn("TransactionRollback, statements for requestId {} could not be executed. Statement list: {}", requestId, result);
    }*/
}
