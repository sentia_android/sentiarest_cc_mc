package com.sentiacare.dtos;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TableResult")
public class TableResult {

    public String _tablename = "";
    public String Status = "False";
    public String FileStatus = "";
    public String DBStatus = "";
    public String Comment = "";
    public String TranId = "";
    public String TranAction = "";
    public String _logdatetime = "";
    public boolean DbExists = false;

}

