package com.sentiacare.dtos;

public class ExportRequest {

    public String mcId;
    public String lastSyncDate;
    public String schemaName;
    public String userType;

}
