package com.sentiacare.dtos;

public class ExportResponse {

    public String type;
    public String title;
    public int status;
    public String detail;
    public String instance;
    
}
