package com.sentiacare.dtos;

import com.sentiacare.dtos.synclogDots.DateMonYearDots;

public class ReportResponse {
    DateMonYearDots dots;

    public ReportResponse(DateMonYearDots dots) {
        this.dots = dots;
    }
}
