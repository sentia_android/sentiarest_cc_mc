package com.sentiacare.dtos.synclogDots;

public class ReqIDStatus {
    String requestId;
    String status;
    String transStatus;
    String comments;

    public ReqIDStatus(String requestId, String status, String transStatus, String comments) {
        this.requestId = requestId;
        this.status = status;
        this.transStatus = transStatus;
        this.comments = comments;
    }
}
