package com.sentiacare.dtos.synclogDots;

public class DateMonYearDots {
    public int date;
    public int month;
    public int year;

    public DateMonYearDots(int date, int month, int year) {
        this.date = date;
        this.month = month;
        this.year = year;
    }
}
