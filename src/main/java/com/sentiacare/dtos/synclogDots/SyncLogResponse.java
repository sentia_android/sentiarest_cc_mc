package com.sentiacare.dtos.synclogDots;

import java.util.List;

public class SyncLogResponse {
    public String userId;
    public List<ReqIDStatus> requestIDStatus;
}

