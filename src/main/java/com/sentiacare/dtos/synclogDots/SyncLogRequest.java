package com.sentiacare.dtos.synclogDots;

import java.util.ArrayList;

public class SyncLogRequest {
    public String userId;
    public ArrayList<String> requestId;
    public String schemaName;
}
