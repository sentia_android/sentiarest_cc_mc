package com.sentiacare.dtos;

public class CustomException {
    String expMsg;
    int statusCode;
    String msg;

    public CustomException(String expMsg, int statusCode, String msg) {
        this.expMsg = expMsg;
        this.statusCode = statusCode;
        this.msg = msg;
    }
}
