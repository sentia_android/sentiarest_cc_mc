package com.sentiacare.dtos;

public class UploadResponse {

    public String message;
    public boolean error;

    public UploadResponse(String message, boolean error) {
        this.message = message;
        this.error = error;
    }
}
