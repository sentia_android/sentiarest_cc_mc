package com.sentiacare.dtos;

public class ReportRequest {
    String userId;
    String masterDbName;

    public String getUserId() {
        return userId;
    }

    public String getMasterDbName() {
        return masterDbName;
    }
}