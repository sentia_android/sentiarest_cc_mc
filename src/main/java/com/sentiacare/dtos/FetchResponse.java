package com.sentiacare.dtos;

public class FetchResponse {
    private String mcId;
    private String requestId;
    private String status;
    private String comments;
    private String fileSize;
    private String fileName;

    public FetchResponse(String mcId, String requestId, String status, String comments,String fileSize,String fileName) {
        this.mcId = mcId;
        this.requestId = requestId;
        this.status = status;
        this.comments = comments;
        this.fileSize = fileSize;
        this.fileName = fileName;
    }
    public String getStatus() {
        return status;
    }
}
