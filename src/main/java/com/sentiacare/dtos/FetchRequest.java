package com.sentiacare.dtos;

public class FetchRequest {
    public String bc_pcId;
    public String userType;
    public String userId;
    public String schemaName;
    public String lastSyncDate;
    public String requestId;
    public String sendAt;
}
