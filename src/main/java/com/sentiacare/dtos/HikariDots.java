package com.sentiacare.dtos;

public class HikariDots {
    int totalConnection;
    int activeConnection;
    int idleConnection;
    int awaitConnection;
    boolean canProcessRequest;
    String dbName;

    public HikariDots(int totalConnection, int activeConnection, int idleConnection, int awaitConnection, boolean canProcessRequest, String dbName) {
        this.totalConnection = totalConnection;
        this.activeConnection = activeConnection;
        this.idleConnection = idleConnection;
        this.awaitConnection = awaitConnection;
        this.canProcessRequest = canProcessRequest;
        this.dbName = dbName;
    }

    public boolean isCanProcessRequest() {
        return canProcessRequest;
    }

    public void setCanProcessRequest(boolean canProcessRequest) {
        this.canProcessRequest = canProcessRequest;
    }
}
