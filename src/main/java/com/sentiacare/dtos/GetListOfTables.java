//Rakesh

package com.sentiacare.dtos;

public class GetListOfTables {
    public String tableName;
    public String tblType;
    public String villageIdColumnName;

    public String groupby;

    public String getBeneIdColumnName() {
        return beneIdColumnName;
    }

    public void setBeneIdColumnName(String beneIdColumnName) {
        this.beneIdColumnName = beneIdColumnName;
    }

    public String beneIdColumnName;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTblType() {
        return tblType;
    }

    public void setTblType(String tblType) {
        this.tblType = tblType;
    }

    public String getVillageIdColumnName() {
        return villageIdColumnName;
    }

    public void setVillageIdColumnName(String villageIdColumnName) {
        this.villageIdColumnName = villageIdColumnName;
    }

    public String getGroupby() {
        return groupby;
    }

    public void setGroupby(String groupby) {
        this.groupby = groupby;
    }
}
