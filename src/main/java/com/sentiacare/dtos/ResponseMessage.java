package com.sentiacare.dtos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "ResponseMessage")
public class ResponseMessage {

    private int Messagecnt;

    @XmlElement(name = "Message")
    public String Message = "";

    @XmlElement(name = "RequestId")
    public String RequestId = "";

    @XmlElement
    public boolean DBExists = true;

    @XmlElement
    public List<TableResult> TableResults = new ArrayList<>();

    @XmlElement(name = "Messagecnt")
    public int getMessagecnt() {
        return TableResults == null || TableResults.isEmpty() ? 0 : TableResults.size();
    }

}
