package com.sentiacare.dtos;

public class SyncParams {

    public String requestId;
    public String schemaName;

    public SyncParams(String requestId, String schemaName) {
        this.requestId = requestId;
        this.schemaName = schemaName;
    }
}
