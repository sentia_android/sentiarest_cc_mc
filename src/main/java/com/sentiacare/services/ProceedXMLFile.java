package com.sentiacare.services;

import com.sentiacare.domain.SentiatendException;
import com.sentiacare.dtos.HikariDots;
import com.sentiacare.entities.BatchItem;
import com.sentiacare.entities.DBStatus;
import com.sentiacare.repositories.DoTheRest;
import com.sentiacare.repositories.DoTheRest2;
import com.sentiacare.repositories.SentiaPooledDataSource;
import com.sentiacare.utils.AppConfig;
import com.sentiacare.utils.FileUtils;
import com.sentiacare.utils.XMLUtils2;
import com.zaxxer.hikari.HikariConfigMXBean;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.io.EofException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import java.io.File;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ProceedXMLFile {
    private final Logger LOGGER = LogManager.getLogger(ProceedXMLFile.class);

    private final Logger LOGGER2 = LogManager.getLogger("EOFLogger");


    private final File file;
    private final AppConfig appConfig;
    private final SentiaPooledDataSource ds;

    private final DocumentBuilder documentBuilder;

    public ProceedXMLFile(File file, AppConfig appConfig, SentiaPooledDataSource ds, DocumentBuilder documentBuilder) {
        this.file = file;
        this.appConfig = appConfig;
        this.ds = ds;
        this.documentBuilder = documentBuilder;

    }

    public boolean processXMLFile() throws Exception, SentiatendException {
        String RequestId = "";
        String dbname = "";
        String userId = "";
        if(!file.exists()){
            return false;
        }
        Document doc = null;

            doc = documentBuilder.parse(file);

        NodeList nodes = doc.getElementsByTagName("Request");
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(0);
            if (element.getElementsByTagName("RequestId").getLength() > 0)
                RequestId = element.getElementsByTagName("RequestId").item(0).getTextContent();
            if (element.getElementsByTagName("DatabaseId").getLength() > 0) {
                dbname = element.getElementsByTagName("DatabaseId").item(0).getTextContent();
                if (dbname == null || dbname.length() == 0) {
                    LOGGER.error("File {} does not contain DatabaseId", file.getPath());
                }
            }
        }
        if(RequestId != null) {
            userId = RequestId.substring(16, 21);
        }
        boolean unprocessed = true;

        Connection c = null;
        DBOpsStatus dbOpsStatus;
        String strDbStatus, strComment;
        String startTime = "";
        try {
//            23Jan2023 Arpitha
            boolean canGetConnection = isPoolAvailable();
            if (!canGetConnection) {
                System.out.printf("Connection Not Available right now, Will process on Next Iteration for %s%n", RequestId);
                return false;
            }//23Jan2023 Arpitha

            c = ds.getConnection(dbname);
            /**
             *  If IsAnyInitiatedTransaction return True, Initiated Records exists. So, That User Upcoming Request will not Process.
             */
            if (isAnyInitiatedTransaction(c, userId)) {//23Jan2023 Arpitha
                System.out.printf("Skipped for %s-%s%n", userId, RequestId);
                return false;
            }//23Jan2023 Arpitha
            updateXMLProcessSynclog_MysqlasInitiated(c,RequestId,userId);
            startTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
            List<BatchItem> statements = XMLUtils2.extractStatementsOrThrow(file);
            if(appConfig.batchProcess){//23Jan2023 Arpitha
            dbOpsStatus = doBatchforXML(statements, RequestId, dbname, c,userId,startTime,"");
            }else{//23Jan2023 Arpitha
                dbOpsStatus = doStatementForXML(statements, RequestId, dbname, c, userId, startTime, "");
            }//23Jan2023 Arpitha
            if(dbOpsStatus.dbStatus != null)
                strDbStatus  = dbOpsStatus.dbStatus.toString(); // Bindu set db status

            if (dbOpsStatus.dbStatus == DBStatus.TransactionRollback) {
                throw new SentiatendException("Moving file to unprocessed directory because of TransactionRollback");
            } else {
                unprocessed = false;
            }
            FileUtils.move(file.toPath(), true, Paths.get(appConfig.WebServerProcessedFolderName)); // must async this
            return true;
        } catch (SentiatendException e) {
            if (e.getCause() instanceof EofException) {
                LOGGER2.error("Could not unzip xml for requestId : {}", e.getCause());
            } else {
                LOGGER.error(e.getMessage(), e);
            }
            if (unprocessed) {
                FileUtils.move(file.toPath(), false, Paths.get(appConfig.WebServerUnprocessedFolderName));
            }
            strDbStatus = DBStatus.TransactionRollback.toString();
            strComment = e.messageForLogs;
            updateXMLProcessSynclog_Mysql(c,strDbStatus,RequestId,userId,startTime,strComment,!unprocessed);
            return false;
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
            strDbStatus = DBStatus.TransactionRollback.toString();
            strComment = e.getMessage();
            throw  new Exception(strComment);
        }
        finally {
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean updateXMLProcessSynclog_Mysql( Connection c, String TransStatus, String reqID, String userId,String startAt,String comments,boolean proceedOrNot) { // 26Jul2022 Bindu Add connection parameter
        PreparedStatement ps = null;
        try {
            c.setAutoCommit(false);
            String currTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
            if(comments==null||comments.length()==0){
            ps = c.prepareStatement("UPDATE tblsyncservicelog_mysql Set  Status =? , transStatus = ?, transAt = ? where requestId = ? and userId = ? ");//fixme need to add user id in where condition
                ps.setString(4, reqID);
                ps.setString(5, userId);
            }else{
                ps = c.prepareStatement("UPDATE tblsyncservicelog_mysql Set  Status =? , transStatus = ?, transAt = ?,comments = ?  where requestId = ? and userId = ? ");
                ps.setString(4, comments);
                ps.setString(5, reqID);
                ps.setString(6, userId);
            }
            ps.setString(1, proceedOrNot?"Processed":"Unprocessed");
            ps.setString(2, TransStatus);
            ps.setString(3, currTime);

            ps.execute();
            c.commit();
            return true;
        } catch (SQLException e) {

            LOGGER.error("saveServiceRequestLog New - Catch " + e.getMessage(), e); // Bindu 26Jul2022 Add Tag
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.clearParameters();
                    ps.clearBatch();
                }
            } catch (SQLException e) {
                LOGGER.error("SaveServiceRequestLog New - Exception in finally", e); // Bindu 26Jul2022 Add Tag
            }
        }
    }

    public DBOpsStatus doBatchforXML(List<BatchItem> batchItems, String requestId, String dbName, Connection connection,String userId,String startTime,String strComment) throws SentiatendException {
        if (batchItems.isEmpty()) {
            throw new SentiatendException(String.format("No insert/update statements found in xml. Request Id %s - Schema name %s", requestId, dbName));
        }
        Statement statement = null;
        boolean canCommit = true;
        int[] updateCounts = new int[0];
        try {
            connection.setAutoCommit(false);

            statement = connection.createStatement();
            for (BatchItem batchItem : batchItems) {
                statement.addBatch(batchItem.statement);
            }
            try {
                updateCounts = statement.executeBatch();
            } catch (BatchUpdateException e) {
                LOGGER.error(e.getMessage(), e);
                canCommit = false;
                updateCounts = e.getUpdateCounts();
            }
            boolean logAdded = updateXMLProcessSynclog_Mysql(connection, canCommit ? DBStatus.TransactionCommit.name() : DBStatus.TransactionRollback.name(), requestId, userId, startTime, strComment,canCommit);
            if (canCommit && logAdded) {
                connection.commit();
            } else {
                connection.rollback();
            }
            return new DBOpsStatus(canCommit ? DBStatus.TransactionCommit : DBStatus.TransactionRollback, canCommit);
        } catch (SQLException ex) {
            throw new SentiatendException(String.format("Could not get connection to schema %s", dbName), ex);
        } finally {
            try {
                if (statement != null) {
                    statement.clearBatch();
                }
                new Thread(new DoTheRest(batchItems, requestId, canCommit, updateCounts)).start();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
    }

    //8-Sep-2022 Ramesh
    public boolean updateXMLProcessSynclog_MysqlasInitiated( Connection c, String reqID, String userId) { // 26Jul2022 Bindu Add connection parameter
        PreparedStatement ps = null;
        try {
            c.setAutoCommit(false);
            String currTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
            ps = c.prepareStatement("UPDATE tblsyncservicelog_mysql Set  fileProcessedAt = ?, transStatus = ?  where requestId = ? and userId = ? ");
            ps.setString(1, currTime);
            ps.setString(2, "Initiated");
            ps.setString(3, reqID);
            ps.setString(4, userId);
            ps.execute();
            c.commit();
            return true;
        } catch (SQLException e) {
            LOGGER.error("saveServiceRequestLog New - Catch " + e.getMessage(), e); // Bindu 26Jul2022 Add Tag
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.clearParameters();
                    ps.clearBatch();
                }
            } catch (SQLException e) {
                LOGGER.error("SaveServiceRequestLog New - Exception in finally", e); // Bindu 26Jul2022 Add Tag
            }
        }
    }

    private boolean isAnyInitiatedTransaction(Connection c, String userId) {
        try {
            String statement = String.format("SELECT * FROM tblsyncservicelog_mysql where userId = '%s' and transStatus = 'Initiated';", userId);
            PreparedStatement ps = c.prepareStatement(statement);
            ResultSet resultSet = ps.executeQuery();
            return resultSet.isBeforeFirst();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

//    23Jan2023 Arpitha
public DBOpsStatus doStatementForXML(List<BatchItem> batchItems, String requestId, String dbName, Connection connection, String userId, String startTime, String strComment) throws SentiatendException {
    if (batchItems.isEmpty()) {
        throw new SentiatendException(String.format("No insert/update statements found in xml. Request Id %s - Schema name %s", requestId, dbName));
    }
    Statement statement = null;
    boolean canCommit = true;
    List<BatchItem> exceptionBatches = new ArrayList<>();
    try {
        connection.setAutoCommit(false);

        statement = connection.createStatement();

        /** Execute each statement one by one based on Statement Type
         *
         */
        for (BatchItem batchItem : batchItems) {
            switch (batchItem.statementType) {
                case Insert:
                    try {
                        statement.execute(batchItem.statement);
                    } catch (SQLException e) {
                        strComment = e.getMessage();
                        exceptionBatches.add(batchItem);
                        canCommit = false;
                    }
                    break;
                case Update:
                    try {
                        int updatedRowsCount = statement.executeUpdate(batchItem.statement);
                        if (updatedRowsCount == 0) {
                            strComment = "Update Statement didn't affect any records";
//                            exceptionBatches.add(batchItem);
                           // canCommit = false;
                        }
                    } catch (SQLException e) {
                        strComment = e.getMessage();
                        exceptionBatches.add(batchItem);
                        canCommit = false;
                    }
                    break;
            }
            /**
             * If CanCommit is set to false means, there is a Exception so, further transaction are unnecessary.
             * So the loop is broke immediately when the exception is caught
             */
            if (!canCommit) {
                break;
            }
        }
        if (canCommit) {
            connection.commit();
        } else {
            connection.rollback();
        }
        boolean logAdded = updateXMLProcessSynclog_Mysql(connection, canCommit ? DBStatus.TransactionCommit.name() : DBStatus.TransactionRollback.name(), requestId, userId, startTime, strComment, canCommit);
        return new DBOpsStatus(canCommit ? DBStatus.TransactionCommit : DBStatus.TransactionRollback, canCommit);
    } catch (SQLException ex) {
        throw new SentiatendException(String.format("Could not get connection to schema %s", dbName), ex);
    } finally {
        try {
            if (statement != null) {
                statement.clearBatch();
            }
//            new Thread(new DoTheRest(batchItems, requestId, canCommit, updateCounts)).start();
            new Thread(new DoTheRest2(exceptionBatches, requestId)).start();
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }
}

    public boolean isPoolAvailable() {
        try {
            HikariDataSource hikari = ds.getHikariDataSource();
            HikariConfigMXBean hikariConfig = ds.getHikariDataSource().getHikariConfigMXBean();
            HikariPoolMXBean bean = hikari.getHikariPoolMXBean();
            String[] url = hikari.getJdbcUrl().split("/");
            String schema  = url[url.length-1];
            HikariDots hikariDots = new HikariDots(bean.getTotalConnections(), bean.getActiveConnections(), bean.getIdleConnections(), bean.getThreadsAwaitingConnection(), true,schema);
            if (bean.getActiveConnections() == hikariConfig.getMaximumPoolSize()) {
                hikariDots.setCanProcessRequest(false);
            }

            return hikariDots.isCanProcessRequest();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
