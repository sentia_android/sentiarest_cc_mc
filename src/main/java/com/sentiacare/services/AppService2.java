/*
package com.sentiacare.services;

import com.sentiacare.domain.SentiatendException;
import com.sentiacare.dtos.AppConfigForSync;
import com.sentiacare.dtos.SyncParams;
import com.sentiacare.entities.*;
import com.sentiacare.repositories.ArchivalMarker;
import com.sentiacare.repositories.SentiaPooledDataSource;
import com.sentiacare.repositories.TranTableRepository;
import com.sentiacare.utils.FileUtils;
import com.sentiacare.utils.XMLUtils2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.io.EofException;

import javax.xml.validation.Schema;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.zip.GZIPInputStream;

public class AppService2 {

    private static final Logger LOGGER = LogManager.getLogger(AppService2.class);

    private static final Logger LOGGER2 = LogManager.getLogger("EOFLogger");

    public static String sync(InputStream stream, String requestId, AppConfigForSync appConfig, SentiaPooledDataSource ds, String date, Schema schema) {
        ServiceLog log = new ServiceLog();
        log.requestSentAt = Timestamp.valueOf(LocalDateTime.parse(date, DateTimeFormatter.RFC_1123_DATE_TIME));
        log.requestReceivedAt = Timestamp.valueOf(LocalDateTime.now());
        log.restMethod = "MessageForwarder";

        String message;
        Path f = null;
        List<TransactionAudit> audits = new ArrayList<>();
        TranTableRepository repository = null;
        SyncParams syncParams = null;
        boolean unprocessed = true;
        Path unzippedFilPath = null;
        try {
            unzippedFilPath = targetPath(appConfig);
            String xml = unzipOrThrow(stream, unzippedFilPath);



            */
/*String [] g = xml.split("<Request>");
            xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request>"+g[1];
            String[] jjs = xml.split("</Request>");
            xml = jjs[0]+"</Request>";//Testing from xml*//*

            syncParams = XMLUtils2.extract(xml);
            SentiaValidator.throwIfNotValid(syncParams);
            addEvent(audits, syncParams.requestId, FileStatus.FoundRequestId, MessageStatus.Success, null, null);

            repository = new TranTableRepository(ds);

            log.userId = syncParams.requestId.substring(16, 21);
            log.requestId = syncParams.requestId;
            repository.saveServiceRequestLog(log, syncParams.schemaName);

            XMLUtils2.throwIfNotValid(xml, schema);
            addEvent(audits, syncParams.requestId, FileStatus.Validation, MessageStatus.Success, null, null);

            repository.throwIfRequestIdExists(syncParams.requestId, syncParams.schemaName);

            f = FileUtils.saveToFolder2(xml, syncParams.requestId, appConfig.getWebServerInputFolderName());
            addEvent(audits, syncParams.requestId, FileStatus.SaveToFolder, MessageStatus.Success, null, null);

            List<BatchItem> statements = XMLUtils2.extractStatementsOrThrow(f.toFile());
            DBOpsStatus dbOpsStatus = repository.doBatch(statements, syncParams.requestId, syncParams.schemaName);
            addEvent(audits, syncParams.requestId, null, MessageStatus.Success, null, dbOpsStatus.dbStatus);

            log.transStatus = dbOpsStatus.dbStatus.toString();
            log.transAt = Timestamp.valueOf(LocalDateTime.now());
            repository.updateServiceRequestLog(log, syncParams.schemaName);

            if (dbOpsStatus.dbStatus == DBStatus.TransactionRollback) {
                throw new SentiatendException("Moving file to unprocessed directory because of TransactionRollback");
            } else {
                unprocessed = false;
            }

            message = XMLUtils2.responseXML2(syncParams.requestId, true, dbOpsStatus.dbStatus,
                    "Completed sync, will move file to Processed directory.", MessageStatus.Success);

            FileUtils.move(f, true, Paths.get(appConfig.getWebServerProcessedFolderName())); // must async this
        } catch (SentiatendException e) {
            if (e.getCause() instanceof EofException) {
                LOGGER2.error("Could not unzip xml for requestId : {}", requestId, e.getCause());
            } else {
                LOGGER.error(e.getMessage(), e);
            }
            if (unprocessed) {
                moveFileToUnprocessed(f, appConfig);
            }
            addEvent(audits, syncParams == null ? null : syncParams.requestId, e.fileStatus, MessageStatus.Failure, e.getMessage(), null);
            message = exceptionMessageForClient(syncParams, e.getMessage());
        } finally {
            saveEvents(audits, repository, syncParams);
            if (unzippedFilPath != null) {
                try {
                    Files.delete(unzippedFilPath);
                } catch (IOException e) {
                    LOGGER.error("Could not handle deletion of unzipped file", e);
                }
            }
            if (syncParams != null) {
                ArchivalMarker archivalMarker = new ArchivalMarker(syncParams.requestId, syncParams.schemaName, ds);
                new Thread(archivalMarker).start();
            }
        }
        return message;
    }

    private static String unzipOrThrow(InputStream stream, Path target) throws SentiatendException {
        try (GZIPInputStream i = new GZIPInputStream(stream)) {
//        try{//Testing from xml
            Files.copy(i, target);
//            CopyOption[] options = { StandardCopyOption.REPLACE_EXISTING };//Testing from xml

//            Files.copy(stream,target,options);//Testing from xml
            return new String(Files.readAllBytes(target), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new SentiatendException("Could not unzip request", e);
        }
    }

    private static Path targetPath(AppConfigForSync appConfig) {
        return Paths.get(appConfig.getUnzipDirectory() + "\\MessageForwarder\\" + UUID.randomUUID() + ".xml");
    }

    private static void moveFileToUnprocessed(Path f, AppConfigForSync appConfig) {
        try {
            if (f != null) {
                FileUtils.move(f, false, Paths.get(appConfig.getWebServerUnprocessedFolderName()));
            }
        } catch (SentiatendException ex) {
            LOGGER.error("Could not move file to unprocessed directory", ex);
        }
    }

    */
/*private static void addFileStatusEvent(SentiatendException e, SyncParams syncParams, List<TransactionAudit> audits, boolean failureEvent) {
        if (e.fileStatus != null) {
            if (syncParams != null) {
                addEvent(audits, syncParams.requestId, e.fileStatus, failureEvent ? DBStatus.TransactionRollback : null,
                        e.messageStatus, e.messageForLogs);
            }
        }
    }*//*


    private static String exceptionMessageForClient(SyncParams syncParams, String exceptionMessage) {
        try {
            return XMLUtils2.responseXML2(syncParams == null ? "" : syncParams.requestId, true,
                    DBStatus.TransactionRollback, exceptionMessage, MessageStatus.Failure);
        } catch (SentiatendException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return String.format("<ResponseMessage><TableResults><Comment>%s</Comment><DBStatus>%s</DBStatus></TableResults>" +
                            "<Status>Failure</Status></ResponseMessage>",
                    exceptionMessage, DBStatus.TransactionRollback);
        }
    }

    private static void saveEvents(List<TransactionAudit> audits, TranTableRepository repository, SyncParams syncParams) {
        try {
            if (repository != null) {
                repository.saveEvents(audits, syncParams.schemaName, syncParams.requestId);
            }
        } catch (SentiatendException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private static void addEvent(List<TransactionAudit> audits, String requestId, FileStatus fs, MessageStatus ms, String comment, DBStatus ds) {
        String logDateTime = logDateTime();
        audits.add(new TransactionAudit(requestId, fs, ds, ms.toString(), comment, logDateTime));
    }

    private static String logDateTime() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:s"));
    }
}
*/
