package com.sentiacare.services;

import com.sentiacare.domain.SentiatendException;
import com.sentiacare.entities.DBStatus;
import com.sentiacare.repositories.SentiaPooledDataSource;
import com.sentiacare.utils.AppConfig;
import com.sentiacare.utils.FileUtils;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class ScheduleFileProcessing {
    private final SentiaPooledDataSource ds;
    private final AppConfig appConfig;
    private final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

    public ScheduleFileProcessing(SentiaPooledDataSource ds, AppConfig appConfig) {
        this.ds = ds;
        this.appConfig = appConfig;
    }

    //To pick the xml from the table which status is Null and do the SQL transactions.
    //If the no of counts from table and processed file count is same return true, else false
    public boolean doFileProcessing(){
        Connection con = null;
        int recordCount;
        int processedCount = 0;
        try{
            String currTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
            System.out.println("Process started at "+currTime);
            con = ds.getConnection(appConfig.currDbName);
            PreparedStatement statment = con.prepareStatement("Select * from tblsyncservicelog_mysql where clientChecksum = serverChecksum and transStatus is null  and UserId IN ('20058','20059','20060','20061','20062','20063','20064','20065','20066','20067','20068','20069','20073','20074','20075') order by id asc");
            ResultSet result = statment.executeQuery();
            recordCount =result.getRow();
            while (result.next()){
                processedCount = processedCount +1;
                String reqId = result.getString("requestId");
             
                String userId = result.getString("userId");
                String filename = result.getString("filename");
                File currFile = new File(appConfig.syncDirectory + "/" + filename);
                ProceedXMLFile pXMLFile = new ProceedXMLFile(currFile, appConfig, ds, documentBuilderFactory.newDocumentBuilder());
                try{
                    pXMLFile.processXMLFile();
                }catch (Exception e){
                    String comment = e.getMessage();
                    pXMLFile.updateXMLProcessSynclog_Mysql(con, DBStatus.TransactionRollback.name(),reqId,userId,currTime,comment,false);
                    FileUtils.move(currFile.toPath(), false, Paths.get(appConfig.WebServerUnprocessedFolderName));
                    e.printStackTrace();
                }
            }
            if(recordCount==processedCount){
                return true;
            }
            return false;
        }catch (Exception | SentiatendException e){
            e.printStackTrace();
            return  false;
        }finally {
            if(con!=null){
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
