package com.sentiacare.services;

import com.sentiacare.entities.DBStatus;

public class DBOpsStatus {

    public DBStatus dbStatus;
    public boolean success;

    public DBOpsStatus(DBStatus dbStatus, boolean success) {
        this.dbStatus = dbStatus;
        this.success = success;
    }
}
