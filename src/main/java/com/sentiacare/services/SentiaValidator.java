package com.sentiacare.services;

import com.sentiacare.domain.SentiatendException;
import com.sentiacare.dtos.SyncParams;
import com.sentiacare.entities.FileStatus;
import com.sentiacare.entities.MessageStatus;
import com.sentiacare.utils.XMLUtils2;

public class SentiaValidator {

    public static void throwIfNotValid(SyncParams syncParams) throws SentiatendException {
        String message;
        if (syncParams.requestId == null || syncParams.requestId.isEmpty()) {
            message = XMLUtils2.responseXML(syncParams.requestId, true, "", FileStatus.FoundRequestId, MessageStatus.Failure, "Request Id Element missing");
            throw new SentiatendException(message, FileStatus.FoundRequestId, MessageStatus.Failure, "Request Id Element missing");
        }
        if (syncParams.schemaName == null || syncParams.schemaName.isEmpty()) {
            message = XMLUtils2.responseXML(syncParams.requestId, false, "", FileStatus.NoDatabase, MessageStatus.Failure, "No Database in sent xml");
            throw new SentiatendException(message, FileStatus.NoDatabase, MessageStatus.Failure, "No Database in sent xml");
        }
    }
}
