package com.sentiacare.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;
import com.sentiacare.domain.BusinessHandler;
import com.sentiacare.dtos.*;
import com.sentiacare.dtos.synclogDots.ReqIDStatus;
import com.sentiacare.dtos.synclogDots.SyncLogRequest;
import com.sentiacare.dtos.synclogDots.SyncLogResponse;
import com.sentiacare.entities.TablostRequest;
import com.sentiacare.entities.TablostResponse;
import com.sentiacare.entities.tblServerDataFetchLog;
import com.sentiacare.repositories.DataHandler;
import com.sentiacare.repositories.SentiaPooledDataSource;
import com.sentiacare.repositories.TabLostTnandCol;
import com.sentiacare.utils.AppConfig;
import com.zaxxer.hikari.HikariConfigMXBean;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import io.javalin.core.util.FileUtil;
import io.javalin.http.Context;
import io.javalin.http.UploadedFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;

public class AppService {
    private static final Logger LOGGER = LogManager.getLogger(AppService.class);

    private final AppConfig appConfig;
    private final BusinessHandler businessHandler;
    //    private final XMLInputFactory xmlInputFactory;
    private static Connection conn = null;

    //  private final Map<String, Integer> numberOfRequestsToRequestId = new HashMap<>();

   /* public static ArrayList<String> skipTables = new ArrayList<>(Arrays.asList("tblashaincentives",
            "tbldisplaymaster",
            "tblm_usermaster",
            "tbldisplaynames",
            "tblm_loginaudit",
            "tblashachildimz",
            "trantable",
            "tblreportusers",
            "tblashatracker",
            "tblservicestypemaster",
            "tblm_userfacility",
            "tblfacilitydetails",
            "tblreferralfacility",
            "tblaudittrail",
            "tblbmiforage",
            "tblcomplicationmgmt",
            "tblhrpcomplicationmgmt",
            "tblloginaudit",
            "tblm_usermaster",
            "tblmansimitratracker",
            "tblmansimitratrackertemp",
            "tblmuacforage",
            "tblreportusers",
            "tblservicestypemaster",
            "tblusertracker",
            "tblusertrackertemp",
            "tbluserimztracker",
            "tbluserimztrackertemp",
            "tblinstusers",
            "tblfetchdetails",
            "tbldashboardfetchdetails",
            "tbl_fetchusertracker_toclient", "servicelog", "tranusers", "tblstat_adolstatus", "tblstat_cgmcompl"
    ));*/

    public static ArrayList<String> skipColumns = new ArrayList<>(Arrays.asList("regjsy",
            "vishstatus",
            "recordvieweddate",
            "serviceorderid",
            "imagepath"));
    public static ArrayList<String> instskipColumns = new ArrayList<>(Arrays.asList(
            "phcname",
            "scname",
            "panchayathname"
    ));

    //    23Jun2021 Arpitha
    public static ArrayList<String> fetchMMTablesData = new ArrayList<>(Arrays.asList(
            "tblchildinfo",
            "tblchildvisitdetails",
            "tblchlparentdetails",
            "tblchildgrowth",
            //   "tblcomplicationmgmt",
            "tbldeliveryinfo",
            //  "tblhrpcomplicationmgmt",
            "tblregisteredwomen",
            "tblvisitchildheader",
            "tblvisitdetails",
            "tblvisitheader",
            "tbladolreg",
            "tbladolvisitheader",
            "tbladolpregnant",
            "tblinstusers"
    ));
    // private final ExecutorService executorService;
    private final SentiaPooledDataSource ds;

    public AppService(AppConfig appConfig, BusinessHandler businessHandler, SentiaPooledDataSource instance) {
        this.appConfig = appConfig;
        this.businessHandler = businessHandler;
       /* this.xmlInputFactory = xmlInputFactory;
        this.executorService = executorService;*/
        this.ds = instance;
    }

    /*public CompletableFuture<String> messageForwarderInFuture(String xml, DBOps dbOps) {
        CompletableFuture<String> future = new CompletableFuture<>();

        executorService.submit(() -> {
            future.complete(messageForwarder(xml, dbOps));
        });
        return future;
    }*/

   /* public String messageForwarder(String xml, DBOps dbOps) {
        String decryptedXml = "";
        try {
            decryptedXml = CryptoUtils.decrypt(xml, appConfig.secretKey);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException e) {
            LOGGER.error("Error decrypting xml", e);
            return String.format("Sorry! there was an error encountered while decrypting ... Cause: %s ##", e.getMessage());
        }

        String requestId = "", dbname = "";
        try {
            XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(new StringReader(decryptedXml));
            while (xmlStreamReader.hasNext()) {
                int xmlEvent = xmlStreamReader.next();
                if (xmlEvent == XMLEvent.START_ELEMENT) {
                    String elementName = xmlStreamReader.getLocalName();
                    if (elementName.equalsIgnoreCase("requestid")) {
                        requestId = xmlStreamReader.getElementText();
                    }
                    if (elementName.equalsIgnoreCase("databaseid")) {
                        dbname = xmlStreamReader.getElementText();
                        if (dbname == null || dbname.isEmpty()) {
                            return responseXML(requestId, false, "", FileStatus.NoDatabase, MessageStatus.Failure, "No Database in sent xml");
                        }
                    }
                }
                if ((requestId != null && !requestId.isEmpty()) && (!dbname.isEmpty())) {
                    break;
                }
            }
        } catch (XMLStreamException e) {
            LOGGER.error("Error creating XMLStreamReader", e);
            return String.format("Sorry! there was an error encountered while reading xml ... Cause: %s ##", e.getMessage());
        }

        if (requestId == null || requestId.isEmpty()) {
            return responseXML(requestId, true, "", FileStatus.FoundRequestId, MessageStatus.Failure, "Request Id Element missing ");
        }

        String processedResult = businessHandler.checkForRequestIdExists(requestId, dbname);
        int requestCount = 0;
        String requestStatus = "";
        XMLStreamReader xmlStreamReader;
        try {
            xmlStreamReader = xmlInputFactory.createXMLStreamReader(new StringReader(processedResult));
            while (xmlStreamReader.hasNext()) {
                int xmlEvent = xmlStreamReader.next();
                if (xmlEvent == XMLEvent.START_ELEMENT) {
                    String elementName = xmlStreamReader.getLocalName();
                    if (elementName.equalsIgnoreCase("requestcount")) {
                        requestCount = Integer.parseInt(xmlStreamReader.getElementText());
                    }
                    if (elementName.equalsIgnoreCase("requeststatus")) {
                        requestStatus = xmlStreamReader.getElementText();
                    }
                }
            }
            if (requestCount > 0) {
                return responseXML(requestId, true, requestStatus, FileStatus.FoundRequestId, MessageStatus.Failure, "Request Id Already Found and Msg status is ");
            }
        } catch (XMLStreamException e) {
            LOGGER.error("Error creating XMLStreamReader", e);
            return String.format("Sorry! there was an error encountered while creating XMLStreamReader ... Cause: %s ##", e.getMessage());
        }

        //Log that Request Id has been found in the new xml msg
        businessHandler.updateLogToTranTable(requestId, dbname, "", "", "", LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")), FileStatus.FoundRequestId.name(), "", MessageStatus.Success.name());

        //Log that Msg has been decrypted successfully from the new xml msg
        businessHandler.updateLogToTranTable(requestId, dbname, "", "", "", LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")), FileStatus.Decryption.name(), "", MessageStatus.Success.name());

        decryptedXml = decryptedXml.replace("\n", "").replace("\r", "");
        try {
            Validator validator = XMLUtils.getValidator(appConfig.WebServerXSDFilePath);
            XMLUtils.throwIfNotValid(decryptedXml, validator);
            businessHandler.updateLogToTranTable(requestId, dbname, "", "", "", LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")), FileStatus.Validation.name(), "", MessageStatus.Success.name());
        } catch (SAXException | IOException e) {
            LOGGER.error("MsgFwd :: Error Encountered while Validation ", e);
            businessHandler.updateLogToTranTable(requestId, dbname, "", "", "", LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")), FileStatus.Validation.name(), "", MessageStatus.Failure.name());
            businessHandler.updateLogToTranTable(requestId, dbname, "", "", "", LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")), "", DBStatus.TransactionRollback.name(), MessageStatus.Success.name());
            if (e instanceof SAXException) {
                return responseXML(requestId, true, requestStatus, FileStatus.Validation, MessageStatus.Failure,
                        String.format("Invalid XML found while validating file. Cause: %s ##", e.getMessage()));
            }
            return responseXML(requestId, true, requestStatus, FileStatus.Validation, MessageStatus.Failure,
                    String.format("Error encountered while validating file. Cause: %s ##", e.getMessage()));
        }
        try {
            File f = FileUtils.saveToFolder(decryptedXml, requestId, appConfig.WebServerInputFolderName);
            businessHandler.updateLogToTranTable(requestId, dbname, "", "", "", LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) +
                            "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")),
                    FileStatus.SaveToFolder.name(), "", MessageStatus.Success.name());
            DBOpsStatus dbOpsStatus = dbOps.insertOrUpdate(f, requestId, dbname);
            ResponseMessage responseMessage = new ResponseMessage();
            responseMessage.RequestId = requestId;
            TableResult r = XMLUtils.GetResponseXMLForATran("", "", "",
                    FileStatus.NA, dbOpsStatus.dbStatus, MessageStatus.Success, true);
            responseMessage.TableResults.add(r);
            try {
                Marshaller jaxbMarshaller = XMLUtils.getMarshaller(ResponseMessage.class);
                return XMLUtils.serializeToString(responseMessage, jaxbMarshaller);
            } catch (JAXBException e) {
                LOGGER.error("Error serializing ResponseMessage to xml", e);
                return String.format("Sorry! there was an error encountered while serializing ResponseMessage ... Cause: %s ##", e.getMessage());
            }
        } catch (IOException e) {
            LOGGER.error("MsgFwd :: Error encountered while saving xml file ", e);
            businessHandler.updateLogToTranTable(requestId, dbname, "", "", e.getMessage(), LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) +
                            "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")),
                    FileStatus.SaveToFolder.name(), "", MessageStatus.Failure.name());
            return responseXML(requestId, true, requestStatus, FileStatus.SaveToFolder, MessageStatus.Failure,
                    String.format("Error encountered while saving xml file. Cause: %s ##", e.getMessage()));
        }
        }
        */


   /* public String responseXML(String requestId, boolean dbExists, String requestStatus, FileStatus fileStatus, MessageStatus messageStatus, String errorMessage) {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.RequestId = requestId;
        responseMessage.DBExists = dbExists;
        responseMessage.TableResults = new ArrayList<>();

        TableResult r = XMLUtils.GetResponseXMLForATran("", "", errorMessage + requestStatus,
                fileStatus, DBStatus.NA, messageStatus, dbExists);
        responseMessage.TableResults.add(r);
        try {
            Marshaller jaxbMarshaller = XMLUtils.getMarshaller(ResponseMessage.class);
            return XMLUtils.serializeToString(responseMessage, jaxbMarshaller);
        } catch (JAXBException e) {
            LOGGER.error("Error serializing ResponseMessage to xml", e);
            return "Sorry! there was an error encountered while serializing ResponseMessage ...";
        }
    }*/

   /* public String getRegistrationData(String json) {
        String defaultData = "";
        Gson gson = new Gson();
        GetRegistrationRequest request = gson.fromJson(json, GetRegistrationRequest.class);
        String result = businessHandler.getRegistrationData(request.thayiId, request.aadhaarId, request.dbname);
        defaultData = defaultData + result;
        if (defaultData.isEmpty()) {
            return "<mainTag></mainTag>";
        } else {
            return "<mainTag>" + defaultData + "</mainTag>";
        }
    }*/
/*
    public String getDataFromServer(String xml) {
        String decryptedXml;
        try {
            decryptedXml = CryptoUtils.decrypt(xml, appConfig.secretKey);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException e) {
            LOGGER.error("Error decrypting xml", e);
            return String.format("Error decrypting xml. Cause: %s ##", e.getMessage());
        }
        decryptedXml = decryptedXml.replace("\\", "");
        try {
            Validator validator = XMLUtils.getValidator(appConfig.WebServerXSDFilePath);
            XMLUtils.throwIfNotValid(decryptedXml, validator);
        } catch (SAXException | IOException e) {
            LOGGER.error("MsgFwd :: Error Encountered while Validation ", e);
            if (e instanceof SAXException) {
                return String.format("Invalid XML found while validating file. Cause: %s ##", e.getMessage());
            }
            return String.format("Error encountered while validating file. Cause: %s ##", e.getMessage());
        }

        StringBuilder requestIds = new StringBuilder();
        String dbname = "";
        String requestIdsAsString = "";
        XMLStreamReader xmlStreamReader;
        try {
            xmlStreamReader = xmlInputFactory.createXMLStreamReader(new StringReader(decryptedXml));
            while (xmlStreamReader.hasNext()) {
                int xmlEvent = xmlStreamReader.next();
                if (xmlEvent == XMLEvent.START_ELEMENT) {
                    String elementName = xmlStreamReader.getLocalName();
                    if (elementName.equalsIgnoreCase("requestid")) {
                        String requestId = xmlStreamReader.getElementText();
                        if (requestIds.toString().contains(requestId)) {
                        } else {
                            requestIds.append("'").append(requestId).append("',");
                        }
                    }
                    if (elementName.equalsIgnoreCase("databaseid")) {
                        dbname = xmlStreamReader.getElementText();
                        if (dbname == null || dbname.isEmpty()) {
                            LOGGER.warn("GetDataFromServer :: No Database tag in XML Data :: ");
                            return responseXML(requestIds.toString(), false, "",
                                    FileStatus.NoDatabase, MessageStatus.Failure, "No Database in sent xml");
                        }
                    }
                }
            }
        } catch (XMLStreamException e) {
            LOGGER.error("Error creating XMLStreamReader", e);
            return String.format("Sorry! there was an error encountered while creating XMLStreamReader ... Cause: %s ##", e.getMessage());
        }
        requestIdsAsString = requestIds.toString();
        requestIdsAsString = requestIdsAsString.substring(0, requestIdsAsString.length() - 1);

        String xmlResultSet = businessHandler.getDataFromServer(requestIdsAsString, dbname);

        boolean blnDBStatusSuccess = false;
        boolean blnFileStatusSuccess = false;
        boolean blnFileIsInProgress = false;
        boolean blnDBRolledBackSuccessfully = false;
        boolean blnDBCommittedSuccessfully = false;

        FileStatus fileStatus = null;
        MessageStatus messageStatus = null;
        DBStatus dbStatus = null;

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(xmlResultSet.getBytes()));
            NodeList tables = doc.getElementsByTagName("Table");

            if (tables != null && tables.getLength() > 0) {
                for (int i = 0; i < tables.getLength(); i++) {
                    Node table = tables.item(i);
                    if (table.getNodeType() == Node.ELEMENT_NODE) {
                        Element tableElement = (Element) table;

                        NodeList fileStatusNodes = tableElement.getElementsByTagName("filestatus");
                        NodeList msgStatusNodes = tableElement.getElementsByTagName("msgstatus");
                        NodeList dbStatusNodes = tableElement.getElementsByTagName("dbstatus");

                        String fs = "", ms = "", ds = "";

                        if (fileStatusNodes != null && fileStatusNodes.getLength() > 0) {
                            fs = fileStatusNodes.item(0).getTextContent();
                        }
                        if (msgStatusNodes != null && msgStatusNodes.getLength() > 0) {
                            ms = msgStatusNodes.item(0).getTextContent();
                        }
                        if (dbStatusNodes != null && dbStatusNodes.getLength() > 0) {
                            ds = dbStatusNodes.item(0).getTextContent();
                        }

                        if (fs.equalsIgnoreCase(FileStatus.SaveToFolder.name())) {
                            if (ms.equalsIgnoreCase(MessageStatus.Success.name())) {
                                fileStatus = FileStatus.SaveToFolder;
                                messageStatus = MessageStatus.Success;
                                blnFileStatusSuccess = true;
                            } else if (ms.isEmpty()) {
                                blnFileIsInProgress = true;
                                break;
                            }
                        }
                        if (ms.isEmpty()) {
                            if (ds.equalsIgnoreCase(DBStatus.ConnectionOpen.name())) {
                                blnFileIsInProgress = true;
                                dbStatus = DBStatus.ConnectionOpen;
                                break;
                            }
                        } else if (ms.equalsIgnoreCase(MessageStatus.Success.name())) {
                            if (ds.equalsIgnoreCase(DBStatus.TransactionRollback.name())) {
                                blnDBStatusSuccess = true;
                                blnDBRolledBackSuccessfully = true;
                                dbStatus = DBStatus.TransactionRollback;
                                break;
                            } else if (ds.equalsIgnoreCase(DBStatus.TransactionCommit.name())) {
                                blnDBStatusSuccess = true;
                                blnDBCommittedSuccessfully = true;
                                dbStatus = DBStatus.TransactionCommit;
                                break;
                            } else if (ds.equalsIgnoreCase(DBStatus.ConnectionClose.name())) {
                                blnDBStatusSuccess = true;
                                dbStatus = DBStatus.ConnectionClose;
                                break;
                            }
                        }
                    }
                }
            } else {
                ResponseMessage responseMessage = new ResponseMessage();
                responseMessage.TableResults = new ArrayList<>();
                responseMessage.RequestId = requestIdsAsString.replaceAll("'", "");
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "RequestId(s) not found",
                        FileStatus.NA, DBStatus.NA, MessageStatus.Failure, true);
                responseMessage.TableResults.add(r);
                try {
                    Marshaller jaxbMarshaller = XMLUtils.getMarshaller(ResponseMessage.class);
                    return XMLUtils.serializeToString(responseMessage, jaxbMarshaller);
                } catch (JAXBException e) {
                    LOGGER.error("Error serializing ResponseMessage to xml", e);
                    return String.format("Sorry! there was an error encountered while serializing ResponseMessage ... Cause: %s ##", e.getMessage());
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOGGER.error("Error reading XML", e);
            return String.format("Sorry! there was an error encountered while reading XML ... Cause: %s ##", e.getMessage());
        }


        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.TableResults = new ArrayList<>();
        responseMessage.RequestId = requestIdsAsString.replaceAll("'", "");
        if (blnFileIsInProgress) {
            TableResult r = XMLUtils.GetResponseXMLForATran("", "", "File processing is in progress",
                    fileStatus, DBStatus.NA, messageStatus, true);
            responseMessage.TableResults.add(r);
            try {
                Marshaller jaxbMarshaller = XMLUtils.getMarshaller(ResponseMessage.class);
                return XMLUtils.serializeToString(responseMessage, jaxbMarshaller);
            } catch (JAXBException e) {
                LOGGER.error("Error serializing ResponseMessage to xml", e);
                return String.format("Sorry! there was an error encountered while serializing ResponseMessage ... Cause: %s ##", e.getMessage());
            }
        }
        if (blnDBStatusSuccess) {
            if (blnDBCommittedSuccessfully) {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "DB Transaction Completed",
                        fileStatus, dbStatus, messageStatus, true);
                responseMessage.TableResults.add(r);
            }
            if (blnDBRolledBackSuccessfully) {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "DB Transaction Rolled Back",
                        fileStatus, dbStatus, messageStatus, true);
                responseMessage.TableResults.add(r);
            }
            if (!blnDBCommittedSuccessfully && !blnDBRolledBackSuccessfully) {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "DB Transaction is in progress",
                        fileStatus, dbStatus, messageStatus, true);
                responseMessage.TableResults.add(r);
            }
        } else {
            if (blnFileStatusSuccess) {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "File Save to Folder Successfully",
                        fileStatus, DBStatus.NA, messageStatus, true);
                responseMessage.TableResults.add(r);
            } else {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "Either RequestId(s) not found or File Save to Folder Failed",
                        fileStatus, DBStatus.NA, messageStatus, true);
                responseMessage.TableResults.add(r);
            }
        }
        try {
            Marshaller jaxbMarshaller = XMLUtils.getMarshaller(ResponseMessage.class);
            return XMLUtils.serializeToString(responseMessage, jaxbMarshaller);
        } catch (JAXBException e) {
            LOGGER.error("Error serializing ResponseMessage to xml", e);
            return String.format("Sorry! there was an error encountered while serializing ResponseMessage ... Cause: %s ##", e.getMessage());
        }
    }*/


    /**
     * Returns records from tblSettings, tblvillages, tbldangersigns and tbldangerdetails for the userid
     */
    /*public String getAllCurrentData(String xml) {
        String value = "<?xml version=\"1.0\"?>";

        String mainDbname = "";
        String dbname = "";
        String emailid = "";
        String password = "";
        String groupid = "";
        String userid = "";
        String userLastUpdatedDate = "";
        String settingsLastUpdatedDate = "";
        boolean isLoginCheck = false;

        String deviceId = "";
        XMLStreamReader xmlStreamReader;
        try {
            xmlStreamReader = xmlInputFactory.createXMLStreamReader(new StringReader(xml));
            while (xmlStreamReader.hasNext()) {
                int xmlEvent = xmlStreamReader.next();
                if (xmlEvent == XMLEvent.START_ELEMENT) {
                    String elementName = xmlStreamReader.getLocalName();
                    if (elementName.equalsIgnoreCase("masterdb")) {
                        mainDbname = xmlStreamReader.getElementText();
                    }
                    if (elementName.equalsIgnoreCase("emailid")) {
                        emailid = xmlStreamReader.getElementText();
                    }
                    if (elementName.equalsIgnoreCase("password")) {
                        password = xmlStreamReader.getElementText();
                    }
                    if (elementName.equalsIgnoreCase("deviceid")) {
                        deviceId = xmlStreamReader.getElementText();
                    }
                    if (elementName.equalsIgnoreCase("islogincheck")) {
                        isLoginCheck = Boolean.parseBoolean(xmlStreamReader.getElementText());
                    }
                    if (elementName.equalsIgnoreCase("userupdateddate")) {
                        userLastUpdatedDate = xmlStreamReader.getElementText();
                    }
                    if (elementName.equalsIgnoreCase("settingsupdateddate")) {
                        settingsLastUpdatedDate = xmlStreamReader.getElementText();
                    }
                }
            }
        } catch (XMLStreamException e) {
            LOGGER.error("Error creating XMLStreamReader", e);
            return String.format("Sorry! there was an error encountered while creating XMLStreamReader ... Cause: %s ##", e.getMessage());
        }
        String defaultData = "";
        if (!emailid.isEmpty() && !password.isEmpty() && !mainDbname.isEmpty()) {
            String result = businessHandler.getUserAndSettingsData(emailid, password, userLastUpdatedDate, settingsLastUpdatedDate, isLoginCheck, mainDbname, deviceId);
            defaultData += result;
        }
        if (!defaultData.isEmpty()) {
            return "<mainTag>" + defaultData + "</mainTag>";
        }
        return "<mainTag></mainTag>";
    }

    public String GetColorCodedValues(String json) {
        Gson gson = new Gson();
        GetColorCodeValuesRequest request = gson.fromJson(json, GetColorCodeValuesRequest.class);
        String defaultData = "";
        if (!request.dbname.isEmpty()) {
            String result = businessHandler.getColorCodedValuesData(request.lastadddate, request.dbname);
            defaultData = defaultData + result;
        }
        if (defaultData.isEmpty()) {
            return "<mainTag></mainTag>";
        } else {
            return "<mainTag>" + defaultData + "</mainTag>";
        }
    }



    public String getDeliveryInfo(String json) {
        Gson gson = new Gson();
        GetDeliveryInfo request = gson.fromJson(json, GetDeliveryInfo.class);
        String defaultData = "";
        if (!request.dbname.isEmpty()) {
            String result = businessHandler.getDeliveryInfo(request.ThayiID, request.dbname, request.isWomanId);
            defaultData = defaultData + result;
        }
        if (defaultData.isEmpty()) {
            return "<mainTag></mainTag>";
        } else {
            return defaultData;
        }

    }*/
    public String getHRPComplicationActionData(String json) {
        Gson gson = new Gson();
        GetHRPComplicationActionRequest request = gson.fromJson(json, GetHRPComplicationActionRequest.class);
        String defaultData = "";
        if (!request.dbname.isEmpty()) {
            String result = businessHandler.getHRPComplicationActionData(request.dbname, request.dateLastUpdated, request.UserId);
            defaultData = defaultData + result;
        }

        if (defaultData.isEmpty()) {
            return "<mainTag></mainTag>";
        } else {
            return "<mainTag>" + defaultData + "</mainTag>";
        }

    }

    public String getSettingsForUser(String json) {
        Gson gson = new Gson();
        GetSettingsForUserRequest request = gson.fromJson(json, GetSettingsForUserRequest.class);
        String defaultData = "";
        if (!request.dbname.isEmpty()) {
            String result = businessHandler.getSettingsForUser(request.UserId, request.lastUpdatedTime, request.dbname, request.ashaId);

            defaultData = defaultData + result;
        }
        if (defaultData.isEmpty()) {
            return "<mainTag></mainTag>";
        } else {
            return defaultData;
        }
    }

    public String getVillagesForUser(String json) {
        Gson gson = new Gson();
        GetVillagesForUser request = gson.fromJson(json, GetVillagesForUser.class);
        String defaultData = "";
        if (request.dbname == null || request.dbname.length() == 0) {
            return "";
        }
        //    if (!request.dbname.isEmpty()) {
        String result = businessHandler.getVillagesForUser(request.UserId, request.lastUpdatedTime, request.dbname);
        defaultData = defaultData + result;
        //   }


        return defaultData;
    }

   /* public String getDatabase(Context context) {
        try {
            String datas = context.body();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson json = gsonBuilder.create();
            Type empMapType = new TypeToken<Map<String, String>>() {
            }.getType();
            Map<String, String> jsonData = new HashMap<>();
            jsonData = json.fromJson(datas, empMapType);

            String mobileNumber = jsonData.get("mobileNumber");
            String lastSyncDate = jsonData.get("lastSyncDate");
            String databaseName = jsonData.get("masterDB");
            String userType = jsonData.get("userType");
            String dbName = "";
            Connection con = ds.getConnection(databaseName);
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery("Select Inst_Database,UserId,isValidated,InstituteId,isTabLost,UserRole from tblinstusers where UserMobileNumber = '" + mobileNumber + "'");
            if (resultSet.next()) {
                ArrayList<ArrayList<String>> temp = getInistutionDetailsRecords(databaseName, resultSet.getString(4));
                ArrayList<ArrayList<String>> insttemp = getInstUsersRecords(databaseName, mobileNumber);
                if (resultSet.getString(6).equals(userType)) {
                    if (resultSet.getString(3).equals("1")) {
                        if (resultSet.getString(5).equals("1")) {
                            dbName = resultSet.getString(1);
                            if (lastSyncDate.length() == 0) {
                                return getAllMySQLData(dbName, resultSet.getString(2), temp, databaseName, insttemp);
                            } else {
                                return getMySQLData(dbName, lastSyncDate, resultSet.getString(2));
                            }
                        } else {
                            return json.toJson("Please Contact Sentia Team");
                        }
                    } else {
                        return json.toJson("The user is not validated");
                    }
                } else {
                    return json.toJson(userType + " User not Found");
                }
            } else if (resultSet.getRow() == 0) {
                return json.toJson("No user found");
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }*/

    /*private ArrayList<ArrayList<String>> getInstUsersRecords(String databaseName, String mobileNumber) {
        try {
//            ArrayList<String> tempskipColumns = skipColumns;
//            tempskipColumns.addAll(instskipColumns);
            Connection con = ds.getConnection(databaseName);
            Statement statement1 = con.createStatement();
            ResultSet result = statement1.executeQuery("select * from tblinstusers");
            ResultSetMetaData metaData = result.getMetaData();
            int columnSize = metaData.getColumnCount();
            ArrayList<String> columnValues = new ArrayList<>();
            ArrayList<ArrayList<String>> rowValues = new ArrayList<>();
            for (int x = 1; x < metaData.getColumnCount(); x++) {
                if (metaData.getColumnLabel(x).toLowerCase().contains("usermobilenumber")) {
                    result = statement1.executeQuery("select * from tblinstusers where UserMobileNumber = '" + mobileNumber + "   '");
                    break;
                }
            }
            int j = 1;
            while (j <= columnSize) {
//                    if ((!metaData.getColumnLabel(j).toLowerCase().contains("server")) || (!skipColumns.contains(metaData.getColumnLabel(j).toLowerCase()))){
                if (!instskipColumns.contains(metaData.getColumnLabel(j).toLowerCase())) {
                    if ((!metaData.getColumnLabel(j).toLowerCase().contains("server"))) {
                        columnValues.add(metaData.getColumnLabel(j));
                    }
                }
                j++;
            }
            rowValues.add(columnValues);
            columnValues = new ArrayList<>();
            while (result.next()) {
                int init = 1;
                while (init <= columnSize) {
                    if (!instskipColumns.contains(metaData.getColumnLabel(init).toLowerCase())) {
                        if ((!metaData.getColumnLabel(init).toLowerCase().contains("server"))) {
                            columnValues.add(result.getString(init));
                        }
                    }
                    init++;
                }
                rowValues.add(columnValues);
                columnValues = new ArrayList<>();
            }
            return rowValues;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/

    private ArrayList<ArrayList<String>> getInistutionDetailsRecords(String databaseName, String string) {
        try {
            Connection con = ds.getConnection(databaseName);
            Statement statement1 = con.createStatement();
            ResultSet result = statement1.executeQuery("select * from tblinstitutiondetails");
            ResultSetMetaData metaData = result.getMetaData();
            int columnSize = metaData.getColumnCount();
            ArrayList<String> columnValues = new ArrayList<>();
            ArrayList<ArrayList<String>> rowValues = new ArrayList<>();
            for (int x = 1; x < metaData.getColumnCount(); x++) {
                if (metaData.getColumnLabel(x).toLowerCase().contains("instituteid")) {
                    result = statement1.executeQuery("select * from tblinstitutiondetails where InstituteId = '" + string + "   '");
                    break;
                }
            }
            int j = 1;
            while (j <= columnSize) {
//                    if ((!metaData.getColumnLabel(j).toLowerCase().contains("server")) || (!skipColumns.contains(metaData.getColumnLabel(j).toLowerCase()))){
                if (!skipColumns.contains(metaData.getColumnLabel(j).toLowerCase())) {
                    if ((!metaData.getColumnLabel(j).toLowerCase().contains("server"))) {
                        columnValues.add(metaData.getColumnLabel(j));
                    }
                }
                j++;
            }
            rowValues.add(columnValues);
            columnValues = new ArrayList<>();
            while (result.next()) {
                int init = 1;
                while (init <= columnSize) {
                    if (!skipColumns.contains(metaData.getColumnLabel(init).toLowerCase())) {
                        if ((!metaData.getColumnLabel(init).toLowerCase().contains("server"))) {
                            columnValues.add(result.getString(init));
                        }
                    }
                    init++;
                }
                rowValues.add(columnValues);
                columnValues = new ArrayList<>();
            }
            return rowValues;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

   /* private String getAllMySQLData(String dbName, String ASHAID, ArrayList<ArrayList<String>> temp, String masterdb, ArrayList<ArrayList<String>> insttemp) {
        try {
            conn = ds.getConnection(dbName);
            Statement statement = conn.createStatement();
//            ResultSet resultSet = statement.executeQuery("show full tables where table_type ='BASE TABLE'");
            ResultSet resultSet = statement.executeQuery("show tables from " + dbName + " where tables_in_" + dbName + " NOT like 'view%'");
            ArrayList<String> tableNames = new ArrayList<>();
            while (resultSet.next()) {
                if (!skipTables.contains(resultSet.getString(1).toLowerCase())) {
                    tableNames.add(resultSet.getString(1));
                }
            }// to get list of tables
            resultSet.close();
            ArrayList<String> columnValues = new ArrayList<>();
            ArrayList<ArrayList<String>> rowValues = new ArrayList<>();
            Map<String, ArrayList<ArrayList<String>>> map = new HashMap<>();
            map.put("tblinstitutiondetails", temp);
            map.put("tblinstusers", insttemp);
            for (String currentTablename : tableNames) {
//                ArrayList<String> skipColumnsTemp = skipColumns; //Ramesh 29-7-2021 Added skip column if the table is instusers , to skip phcname,scname and panchayath
//                if (currentTablename.equalsIgnoreCase("tblinstusers")) {
//                    skipColumnsTemp.addAll(instskipColumns);
//                }

                Statement statement1 = conn.createStatement();
                ResultSet result = statement1.executeQuery("select * from " + currentTablename);
                ResultSetMetaData metaData = result.getMetaData();
                int columnSize = metaData.getColumnCount();
                for (int x = 1; x < metaData.getColumnCount(); x++) {
                    if (metaData.getColumnLabel(x).toLowerCase().contains("userid")) {
                        result = statement1.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "'");
                        break;
                    }
                }
                int j = 1;
                while (j <= columnSize) {
//                    if ((!metaData.getColumnLabel(j).toLowerCase().contains("server")) || (!skipColumns.contains(metaData.getColumnLabel(j).toLowerCase()))){
                    if (!skipColumns.contains(metaData.getColumnLabel(j).toLowerCase())) {
                        if ((!metaData.getColumnLabel(j).toLowerCase().contains("server"))) {
                            columnValues.add(metaData.getColumnLabel(j));
                        }
                    }
                    j++;
                }
                rowValues.add(columnValues);
                columnValues = new ArrayList<>();
                while (result.next()) {
                    int init = 1;
                    while (init <= columnSize) {
                        if (!skipColumns.contains(metaData.getColumnLabel(init).toLowerCase())) {
                            if ((!metaData.getColumnLabel(init).toLowerCase().contains("server"))) {
                                columnValues.add(result.getString(init));
                            }
                        }
                        init++;
                    }
                    rowValues.add(columnValues);
                    columnValues = new ArrayList<>();
                }
                map.put(currentTablename, rowValues);
                rowValues = new ArrayList<>();
            }
            map = updateInstUserDatafromClientDB(map, dbName, ASHAID);
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson json = gsonBuilder.create();
            return json.toJson(map);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }*/

   /* private Map<String, ArrayList<ArrayList<String>>> updateInstUserDatafromClientDB(Map<String, ArrayList<ArrayList<String>>> map, String dbName, String ashaid) {
        try {
            ArrayList<ArrayList<String>> tblinstusers = map.get("tblinstusers");
            Connection con = ds.getConnection(dbName);
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery("Select LastWomennumber,LastActivityNumber,LastTransNumber,LastRequestNumber,LastDangerNumber,LastParentNumber,LastAdolNumber from tblinstusers where UserId = '" + ashaid + "'");
            if (resultSet.next())
                for (int i = 1; i < tblinstusers.get(0).size(); i++) {
                    if (tblinstusers.get(0).get(i).equals("LastWomennumber")) {
                        tblinstusers.get(1).set(i, String.valueOf(resultSet.getInt("LastWomennumber")));
                    }
                    if (tblinstusers.get(0).get(i).equals("LastActivityNumber")) {
                        tblinstusers.get(1).set(i, String.valueOf(resultSet.getInt("LastActivityNumber")));
                    }
                    if (tblinstusers.get(0).get(i).equals("LastTransNumber")) {
                        tblinstusers.get(1).set(i, String.valueOf(resultSet.getInt("LastTransNumber")));
                    }
                    if (tblinstusers.get(0).get(i).equals("LastRequestNumber")) {
                        tblinstusers.get(1).set(i, String.valueOf(resultSet.getInt("LastRequestNumber")));
                    }
                    if (tblinstusers.get(0).get(i).equals("LastDangerNumber")) {
                        tblinstusers.get(1).set(i, String.valueOf(resultSet.getInt("LastDangerNumber")));
                    }
                    if (tblinstusers.get(0).get(i).equals("LastParentNumber")) {
                        tblinstusers.get(1).set(i, String.valueOf(resultSet.getInt("LastParentNumber")));
                    }
                    if (tblinstusers.get(0).get(i).equals("LastAdolNumber")) {
                        tblinstusers.get(1).set(i, String.valueOf(resultSet.getInt("LastAdolNumber")));
                    }
                }
            map.remove("tblinstusers");
            map.put("tblinstusers", tblinstusers);

            return map;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/

    /*public String getMySQLData(String dbName, String lastsyncDate, String ASHAID) {
        try {
            conn = ds.getConnection(dbName);
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("show full tables where table_type ='BASE TABLE'");
            ArrayList<String> tableNames = new ArrayList<>();
            while (resultSet.next()) {
                if (!skipTables.contains(resultSet.getString(1).toLowerCase())) {
                    tableNames.add(resultSet.getString(1));
                }
            }
            resultSet.close();
            ArrayList<String> columnValues = new ArrayList<>();
            ArrayList<ArrayList<String>> rowValues = new ArrayList<>();
            Map<String, ArrayList<ArrayList<String>>> map = new HashMap<>();
            for (String currentTablename : tableNames) {
                Statement statement1 = conn.createStatement();
                ResultSet result = statement1.executeQuery("select * from " + currentTablename);
                ResultSetMetaData metaData = result.getMetaData();
                int columnSize = metaData.getColumnCount();
                for (int x = 1; x < metaData.getColumnCount(); x++) {
                    if (metaData.getColumnLabel(x).toLowerCase().contains("userid")) {
                        if (checkTablehasRecordDate(conn, currentTablename)) {
                            try {
                                result = statement1.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "'"
                                        + " and RecordUpdatedDate > '" + lastsyncDate + "' ;");
                            } catch (Exception e) {
                                Statement temp = conn.createStatement();
                                result = temp.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "'"
                                        + " and RecordUpdatedDate > str_to_date('" + lastsyncDate + "','%d-%m-%Y %H:%i');");
                            }

                        } else {
                            result = statement1.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "' ;");
                        }
                        break;
                    }
                }
                int j = 1;
                while (j <= columnSize) {
                    if (!skipColumns.contains(metaData.getColumnLabel(j).toLowerCase())) {
                        if ((!metaData.getColumnLabel(j).toLowerCase().contains("server"))) {
                            columnValues.add(metaData.getColumnLabel(j));
                        }
                    }
                    j++;
                }
                rowValues.add(columnValues);
                columnValues = new ArrayList<>();
                if (result != null)
                    while (result.next()) {
                        int init = 1;
                        while (init <= columnSize) {
                            if (!skipColumns.contains(metaData.getColumnLabel(init).toLowerCase())) {
                                if ((!metaData.getColumnLabel(init).toLowerCase().contains("server"))) {
                                    columnValues.add(result.getString(init));
                                }
                            }
                            init++;
                        }
                        rowValues.add(columnValues);
                        columnValues = new ArrayList<>();
                    }
                map.put(currentTablename, rowValues);
                rowValues = new ArrayList<>();
            }
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson json = gsonBuilder.create();
            return json.toJson(map);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }*/

    public String getReferralcenters(String json) {
        Gson gson = new Gson();
        GetReferralCenters request = gson.fromJson(json, GetReferralCenters.class);
        String defaultData = "";
        if (request.dbname == null || request.dbname.length() == 0) {
            return "";
        }
        // if (!request.dbname.isEmpty()) {
        String result = businessHandler.getReferralcenters(request.UserId, request.lastUpdatedTime, request.dbname);
        defaultData = defaultData + result;
        // }

        return defaultData;
    }

    public String saveLogFile(Context ctx) {
        /*ctx.uploadedFiles("files").forEach(f -> {
            FileUtil.streamToFile(f.getContent(), appConfig.logUploadsDirectory + "\\" + f.getFilename());
        });*/
        ctx.uploadedFiles("files").forEach(f -> FileUtil.streamToFile(f.getContent(), appConfig.logUploadsDirectory + "\\" + f.getFilename()));
        Gson gson = new Gson();
        return gson.toJson(new UploadResponse("Success", false));
    }

    /*  public String uploadImage(Context ctx) {
          try {
              String filename = ctx.formParam("filename");
              UploadedFile file = ctx.uploadedFile(filename);

              FileUtil.streamToFile(file.getContent(), appConfig.WebServerUploaded + "/" + file.getFilename() + ".jpg");
              File uploadedfile = new File(appConfig.WebServerUploaded + "/" + file.getFilename() + ".jpg");
              if (uploadedfile.exists()) {
                  return "Uploaded";
              } else {
                  return "Upload Failed";
              }
          } catch (Exception e) {
              e.printStackTrace();
              return e.getMessage();
          }
      }
  */
    public String getfetchCaseMgmt(Context ctx) {
        try {
            String datas = ctx.body();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson json = gsonBuilder.create();
            Type empMapType = new TypeToken<Map<String, String>>() {
            }.getType();
            Map<String, String> jsonData = new HashMap<>();
            jsonData = json.fromJson(datas, empMapType);

            String MMId = jsonData.get("MMId");
            String lastServerDate = jsonData.get("lastServerDate");
            String databaseName = jsonData.get("clientDB");
            return businessHandler.getCaseMgmt(MMId, lastServerDate, databaseName);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public String getFetchfromServer(Context context) {
        try {
            boolean isUserNotFound = false, isUserNotValidated = false, isCCUserNotFound = false;
            String datas = context.body();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson json = gsonBuilder.create();
            Type empMapType = new TypeToken<Map<String, String>>() {
            }.getType();
            Map<String, String> jsonData = new HashMap<>();
            jsonData = json.fromJson(datas, empMapType);

            Map<String, Map<String, ArrayList<ArrayList<String>>>> dataofUserMap = new HashMap<>();

            String CCID = jsonData.get("CCID");
            String lastSyncDate = jsonData.get("lastSyncDate");
            String databaseName = jsonData.get("masterDB");
            //  String PanchanyathName = jsonData.get("PanchayathName");
            String dbName = "";
            Connection con = ds.getConnection(databaseName);
            Statement smt = con.createStatement();
            ResultSet userResult = smt.executeQuery("Select distinct UserId from tblfacilitydetails where CC_Id = '" + CCID + "' and UserId IN (Select UserId from tblinstusers where UserRole='ACHF') order by userid asc");

/*
            ResultSet userResult = smt.executeQuery("Select UserId from tblinstusers where PanchayathName = '" + PanchanyathName + "'");
*/

            if (userResult.next()) {
                do {
                    Statement statement = con.createStatement();
                    String userId = userResult.getString(1);
                    ResultSet resultSet = statement.executeQuery("Select Inst_Database,UserId,isValidated,InstituteId,isTabLost from tblinstusers where UserId = '" + userId + "'");
                    if (resultSet.next()) {
                        isUserNotFound = false;
                        ArrayList<ArrayList<String>> temp = getInistutionDetailsRecords(databaseName, resultSet.getString(4));
                        if (resultSet.getString(3).equals("1")) {
                            isUserNotValidated = false;
                            dbName = resultSet.getString(1);
                            if (lastSyncDate.length() == 0) {
                                dataofUserMap.put(userId, fetchAllData(dbName, resultSet.getString(2), temp, databaseName));
                            } else {
                                dataofUserMap.put(userId, fetchData(dbName, lastSyncDate, resultSet.getString(2)));
                            }

                        } else {
//                        return json.toJson("The user is not validated");
                            isUserNotValidated = true;
                        }
                    } else if (resultSet.getRow() == 0) {
//                    return json.toJson("No user found");
                        isUserNotFound = true;
                    }
                    resultSet.close();
                } while (userResult.next());
                userResult.close();
            } else
                isCCUserNotFound = true;

//            23Jun2021 Arpitha
            if (dataofUserMap.size() > 0)
                return json.toJson(dataofUserMap);
            else if (isUserNotValidated)
                return json.toJson("User Not Validated");
            else if (isUserNotFound)
                return json.toJson("MM User Not Found");
            else if (isCCUserNotFound)
                return json.toJson("CC User Not Found");
            else
                return json.toJson("No Data Found");
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    private Map<String, ArrayList<ArrayList<String>>> fetchAllData(String dbName, String
            ASHAID, ArrayList<ArrayList<String>> temp, String masterdb) {
        String sql = "";
        try {
            conn = ds.getConnection(dbName);
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("show full tables where table_type ='BASE TABLE'");
            ArrayList<String> tableNames = new ArrayList<>();
            while (resultSet.next()) {
                if (fetchMMTablesData.contains(resultSet.getString(1).toLowerCase())) {
                    tableNames.add(resultSet.getString(1));
                }
            }// to get list of tables
            resultSet.close();
            ArrayList<String> columnValues = new ArrayList<>();
            ArrayList<ArrayList<String>> rowValues = new ArrayList<>();
            Map<String, ArrayList<ArrayList<String>>> map = new HashMap<>();
//            map.put("tblinstitutiondetails",temp);
            for (String currentTablename : tableNames) {
                Statement statement1 = conn.createStatement();
//                22Jun2021 Arpitha
                String distinctColumns = " group by ";

                if (currentTablename.equalsIgnoreCase("tbladolreg"))
                    distinctColumns = distinctColumns + " adolId ";
                else if (currentTablename.equalsIgnoreCase("tbladolpregnant"))
                    distinctColumns = distinctColumns + " AdolId and pregnantCount ";
                else if (currentTablename.equalsIgnoreCase("tbladolvisitheader"))
                    distinctColumns = distinctColumns + "  AdolId, VisitId ";
                else if (currentTablename.equalsIgnoreCase("tbladolvisitdetails"))
                    distinctColumns = distinctColumns + "  AdolId, VisitId ";
                else if (currentTablename.equalsIgnoreCase("tblregisteredwomen"))
                    distinctColumns = distinctColumns + " WomanId ";
//                else if (currentTablename.equalsIgnoreCase("tblcomplicationmgmt"))
//                    distinctColumns = distinctColumns + "  WomanId, VisitNum  ";
                else if (currentTablename.equalsIgnoreCase("tbldeliveryinfo"))
                    distinctColumns = distinctColumns + " WomanId ";
//                else if (currentTablename.equalsIgnoreCase("tblhrpcomplicationmgmt"))
//                    distinctColumns = distinctColumns + "  WomanId,VisitNum ";
                else if (currentTablename.equalsIgnoreCase("tblvisitchildheader"))
                    distinctColumns = distinctColumns + "  WomanId,ChildId,ChildNo  ";
                else if (currentTablename.equalsIgnoreCase("tblvisitdetails"))
                    distinctColumns = distinctColumns + " WomanId, VisitNum,visDSymptoms ";
                else if (currentTablename.equalsIgnoreCase("tblvisitheader"))
                    distinctColumns = distinctColumns + "  WomanId,VisitNum ";
                else if (currentTablename.equalsIgnoreCase("tblchildinfo"))
                    distinctColumns = distinctColumns + " chlId ";
                else if (currentTablename.equalsIgnoreCase("tblchlparentdetails"))
                    distinctColumns = distinctColumns + " chlParentId ";
                else if (currentTablename.equalsIgnoreCase("tblchildvisitdetails"))
                    distinctColumns = distinctColumns + "  ChildId, VisitNum ";
                else if (currentTablename.equalsIgnoreCase("tblchildgrowth"))
                    distinctColumns = distinctColumns + "  chlId, chlGMVisitId ";
                else if (currentTablename.equalsIgnoreCase("tblinstusers"))
                    distinctColumns = "";

                ResultSet result = statement1.executeQuery("select * from " + currentTablename);
                ResultSetMetaData metaData = result.getMetaData();
                int columnSize = metaData.getColumnCount();
                for (int x = 1; x < metaData.getColumnCount(); x++) {
                    if (metaData.getColumnLabel(x).toLowerCase().contains("userid")) {
//                        result = statement1.executeQuery("select * from " + currentTablename);
                        /*result = statement1.executeQuery("select * from " + currentTablename + " where UserId = '"
                                + ASHAID + "' "+distinctColumns);*/
                        sql = "select * from " + currentTablename + " where UserId = '"
                                + ASHAID + "' " + distinctColumns;
                        result = statement1.executeQuery(sql);
                        break;
                    }
                }
                int j = 1;
                while (j <= columnSize) {
//                    if ((!metaData.getColumnLabel(j).toLowerCase().contains("server")) || (!skipColumns.contains(metaData.getColumnLabel(j).toLowerCase()))){
                    if (!skipColumns.contains(metaData.getColumnLabel(j).toLowerCase())) {
                        if ((!metaData.getColumnLabel(j).toLowerCase().contains("server"))) {
                            columnValues.add(metaData.getColumnLabel(j));
                        }
                    }
                    j++;
                }
                rowValues.add(columnValues);
                columnValues = new ArrayList<>();
                while (result.next()) {
                    int init = 1;
                    while (init <= columnSize) {
                        if (!skipColumns.contains(metaData.getColumnLabel(init).toLowerCase())) {
                            if ((!metaData.getColumnLabel(init).toLowerCase().contains("server"))) {
                                columnValues.add(result.getString(init));
                            }
                        }
                        init++;
                    }
                    rowValues.add(columnValues);
                    columnValues = new ArrayList<>();
                }
                map.put(currentTablename, rowValues);
                rowValues = new ArrayList<>();
            }
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            String f = sql;
            return null;
        }
    }


    //23Jun2021 Arpitha
    public Map<String, ArrayList<ArrayList<String>>> fetchData(String dbName, String lastsyncDate, String
            ASHAID) {
        try {
            conn = ds.getConnection(dbName);
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("show full tables where table_type ='BASE TABLE'");
            ArrayList<String> tableNames = new ArrayList<>();
            while (resultSet.next()) {
                if (fetchMMTablesData.contains(resultSet.getString(1).toLowerCase())) {
                    tableNames.add(resultSet.getString(1));
                }
            }
            resultSet.close();
            ArrayList<String> columnValues = new ArrayList<>();
            ArrayList<ArrayList<String>> rowValues = new ArrayList<>();
            Map<String, ArrayList<ArrayList<String>>> map = new HashMap<>();
            for (String currentTablename : tableNames) {
                Statement statement1 = conn.createStatement();
                ResultSet result = statement1.executeQuery("select * from " + currentTablename);
                ResultSetMetaData metaData = result.getMetaData();
                int columnSize = metaData.getColumnCount();
                for (int x = 1; x < metaData.getColumnCount(); x++) {
                    if (metaData.getColumnLabel(x).toLowerCase().contains("userid")) {


                        String distinctColumns = " group by ";

                        if (currentTablename.equalsIgnoreCase("tbladolreg"))
                            distinctColumns = distinctColumns + " adolId ";
                        else if (currentTablename.equalsIgnoreCase("tbladolpregnant"))
                            distinctColumns = distinctColumns + " AdolId and pregnantCount ";
                        else if (currentTablename.equalsIgnoreCase("tbladolvisitheader"))
                            distinctColumns = distinctColumns + "  AdolId, VisitId ";
                        else if (currentTablename.equalsIgnoreCase("tbladolvisitdetails"))
                            distinctColumns = distinctColumns + "  AdolId, VisitId ";
                        else if (currentTablename.equalsIgnoreCase("tblregisteredwomen"))
                            distinctColumns = distinctColumns + " WomanId ";
//                        else if (currentTablename.equalsIgnoreCase("tblcomplicationmgmt"))
//                            distinctColumns = distinctColumns + "  WomanId, VisitNum  ";
                        else if (currentTablename.equalsIgnoreCase("tbldeliveryinfo"))
                            distinctColumns = distinctColumns + " WomanId ";
//                        else if (currentTablename.equalsIgnoreCase("tblhrpcomplicationmgmt"))
//                            distinctColumns = distinctColumns + "  WomanId,VisitNum ";
                        else if (currentTablename.equalsIgnoreCase("tblvisitchildheader"))
                            distinctColumns = distinctColumns + "  WomanId,ChildId,ChildNo  ";
                        else if (currentTablename.equalsIgnoreCase("tblvisitdetails"))
                            distinctColumns = distinctColumns + " WomanId, VisitNum,visDSymptoms ";
                        else if (currentTablename.equalsIgnoreCase("tblvisitheader"))
                            distinctColumns = distinctColumns + "  WomanId,VisitNum ";
                        else if (currentTablename.equalsIgnoreCase("tblchildinfo"))
                            distinctColumns = distinctColumns + " chlId ";
                        else if (currentTablename.equalsIgnoreCase("tblchlparentdetails"))
                            distinctColumns = distinctColumns + " chlParentId ";
                        else if (currentTablename.equalsIgnoreCase("tblchildvisitdetails"))
                            distinctColumns = distinctColumns + "  ChildId, VisitNum ";


                        if (checkTablehasRecordCreatedorUpdatedDate(conn, currentTablename).equalsIgnoreCase("ServerUpdatedDate")) {
                            try {
//                            result = statement1.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "'"
//                                    + " and ServerUpdatedDate > '" + lastsyncDate + "' "+distinctColumns+";");
                                result = statement1.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "'"
                                        + " and ServerUpdatedDate > str_to_date('" + lastsyncDate + "','%d-%m-%Y %H:%i') " + distinctColumns + ";");
                            } catch (Exception e) {
                                Statement temp = conn.createStatement();
                                result = temp.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "'"
                                        + " and ServerUpdatedDate > str_to_date('" + lastsyncDate + "','%d-%m-%Y %H:%i') " + distinctColumns + ";");
                            }

                        } else if (checkTablehasRecordCreatedorUpdatedDate(conn, currentTablename).equalsIgnoreCase("ServerCreatedDate")) {
                            try {
//                            result = statement1.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "'"
//                                    + " and ServerCreatedDate > '" + lastsyncDate + "' "+distinctColumns+";");
                                result = statement1.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "'"
                                        + " and ServerCreatedDate > str_to_date('" + lastsyncDate + "','%d-%m-%Y %H:%i') " + distinctColumns + ";");
                            } catch (Exception e) {
                                Statement temp = conn.createStatement();
                                result = temp.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "'"
                                        + " and ServerUpdatedDate > str_to_date('" + lastsyncDate + "','%d-%m-%Y %H:%i') " + distinctColumns + ";");
                            }

                        } else {
                            result = statement1.executeQuery("select * from " + currentTablename + " where UserId = '" + ASHAID + "' " + distinctColumns + ";");
                        }
                        break;
                    }
                }
                int j = 1;
                while (j <= columnSize) {
                    if (!skipColumns.contains(metaData.getColumnLabel(j).toLowerCase())) {
                        if ((!metaData.getColumnLabel(j).toLowerCase().contains("server"))) {
                            columnValues.add(metaData.getColumnLabel(j));
                        }
                    }
                    j++;
                }
                rowValues.add(columnValues);
                columnValues = new ArrayList<>();
                if (result != null)
                    while (result.next()) {
                        int init = 1;
                        while (init <= columnSize) {
                            if (!skipColumns.contains(metaData.getColumnLabel(init).toLowerCase())) {
                                if ((!metaData.getColumnLabel(init).toLowerCase().contains("server"))) {
                                    columnValues.add(result.getString(init));
                                }
                            }
                            init++;
                        }
                        rowValues.add(columnValues);
                        columnValues = new ArrayList<>();
                    }
                map.put(currentTablename, rowValues);
                rowValues = new ArrayList<>();
            }
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*private static boolean checkTablehasRecordDate(Connection conn, String currentTablename) {
        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("select * from " + currentTablename);
            ResultSetMetaData metaData = result.getMetaData();
            for (int x = 1; x < metaData.getColumnCount(); x++) {
                if (metaData.getColumnLabel(x).toLowerCase().contains("serverupdateddate")) {
                    return true;
                } else if (metaData.getColumnLabel(x).toLowerCase().contains("serverupdateddate"))
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }*/

    private static String checkTablehasRecordCreatedorUpdatedDate(Connection conn, String currentTablename) {
        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("select * from " + currentTablename);
            ResultSetMetaData metaData = result.getMetaData();
            for (int x = 1; x < metaData.getColumnCount(); x++) {
                if (metaData.getColumnLabel(x).toLowerCase().contains("serverupdateddate")) {
                    return "serverupdateddate";
                } else if (metaData.getColumnLabel(x).toLowerCase().contains("servercreateddate"))
                    return "servercreateddate";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getUserDetailsforMCUser(String json) {
        try {
            Gson gson = new Gson();
            GetAllInstUser request = gson.fromJson(json, GetAllInstUser.class);
            String defaultData = "";
            if (request.dbname == null || request.dbname.length() == 0) {
                return "";
            }
            if (!request.dbname.isEmpty()) {
                String result = businessHandler.getUserDetailsForUser(request.ashaId, request.dbname, request.lastUpdatedTime);
                defaultData = defaultData + result;
            }

            return defaultData;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getACHFData(String body) {
        try {
            Gson gson = new Gson();
            GetSettingsForUserRequest request = gson.fromJson(body, GetSettingsForUserRequest.class);
            String defaultData = "";
            if (request.dbname == null || request.dbname.length() == 0) {
                return "";
            }
            if (request.dbname != null && request.dbname.length() > 0) {
               /* Connection con = ds.getConnection(request.dbname);
                Statement statement = con.createStatement();
                ResultSet resultSet = statement.executeQuery("Select Inst_Database from tblinstusers where userId = '" + request.UserId + "'");//6-6-2022 Added asha id to get the client db name in services
                if (resultSet.next()) {
                    String dbName = resultSet.getString(1);
                    if (dbName != null && dbName.length() > 0) {*/
                return businessHandler.getAchfData(request.dbname, request.UserId, request.userRole);
                   /* }
                }*/
                //con.close();
            }

            return defaultData;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public InputStream imageDownload(Context ctx) {
        try {
            String filename = ctx.formParam("filename");

//            filename = filename.replace("jpeg", "jpg");

            // String path = appConfig.WebServerUploaded;
            String achfImagePath = "C:\\Sentiacare\\SentiacareACHFServices\\uploaded";
//            path = path.replace("SentiacareMC_CCServices", "SentiacareACHFServices");
            // path = path.replace("SentiacareBCPCServices", "SentiacareACHFServices");

            System.out.println(achfImagePath);
            File file = new File(achfImagePath + "/" + filename + ".jpeg");
            if (file.exists()) {
                System.out.println("Image Found");
                return new FileInputStream(file);
            } else {
                System.out.println("Image NOT Found: " + file.getPath());
                return new StringBufferInputStream("Image not Found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new StringBufferInputStream(e.getMessage());
        }
    }

    //    26Apr2022 Arpitha
   /* public String getDataFromServer(InputStream stream, String date, AppConfig appConfig) {
        ServiceLog log = new ServiceLog();
        log.requestSentAt = Timestamp.valueOf(LocalDateTime.parse(date, DateTimeFormatter.RFC_1123_DATE_TIME));
        log.requestReceivedAt = Timestamp.valueOf(LocalDateTime.now());
        log.restMethod = "GetDataFromServer";

        String xml = null;
        Path target;
        try (GZIPInputStream i = new GZIPInputStream(stream)) {
            target = Paths.get(appConfig.unzipDirectory + "\\GetDataFromServer\\" + UUID.randomUUID() + ".xml");
            System.out.println(target);
            Files.copy(i, target);
            xml = new String(Files.readAllBytes(target));
        } catch (IOException e) {
            LOGGER.error("Could not unzip request body", e);
        }

        try {
            Validator validator = XMLUtils.getValidator(this.appConfig.WebServerXSDFilePath);
            XMLUtils.throwIfNotValid(xml, validator);
        } catch (SAXException | IOException e) {
            LOGGER.error("MsgFwd :: Error Encountered while Validation ", e);
            if (e instanceof SAXException) {
                return String.format("Invalid XML found while validating file. Cause: %s ##", e.getMessage());
            }
            return String.format("Error encountered while validating file. Cause: %s ##", e.getMessage());
        }

        String requestId = null;
        String dbname = null;

        XMLStreamReader xmlStreamReader;
        try {
            xmlStreamReader = xmlInputFactory.createXMLStreamReader(new StringReader(xml));
            while (xmlStreamReader.hasNext()) {
                int xmlEvent = xmlStreamReader.next();
                if (xmlEvent == XMLEvent.START_ELEMENT) {
                    String elementName = xmlStreamReader.getLocalName();
                    if (elementName.equalsIgnoreCase("requestid")) {
                        requestId = xmlStreamReader.getElementText();
                    } else if (elementName.equalsIgnoreCase("databaseid")) {
                        dbname = xmlStreamReader.getElementText();
                    }
                }
            }
        } catch (XMLStreamException e) {
            LOGGER.error("Error creating XMLStreamReader", e);
            return String.format("Sorry! there was an error encountered while creating XMLStreamReader ... Cause: %s ##", e.getMessage());
        }
        if (requestId == null || requestId.isEmpty()) {
            LOGGER.error("GetDataFromServer :: No requestid in XML Data :: ");
            return responseXML(requestId, false, "",
                    FileStatus.NoDatabase, MessageStatus.Failure, "No requestid in sent xml");
        }
        if (dbname == null || dbname.isEmpty()) {
            LOGGER.error("GetDataFromServer :: No Database in XML Data :: ");
            return responseXML(requestId, false, "",
                    FileStatus.NoDatabase, MessageStatus.Failure, "No Database in sent xml");
        }

        log.userId = requestId.substring(16, 21);
        log.requestId = requestId;
        TranTableRepository repository = new TranTableRepository(ds);
        repository.saveServiceRequestLog(log, dbname);

        String xmlResultSet = businessHandler.getDataFromServer(requestId, dbname);
        if (xmlResultSet.isEmpty()) {
            LOGGER.warn("Empty ResultSet for given requestIds {}", requestId);
            return String.format("Empty ResultSet for given requestIds %s", requestId);
        }
        boolean blnDBStatusSuccess = false;
        boolean blnFileStatusSuccess = false;
        boolean blnFileIsInProgress = false;
        boolean blnDBRolledBackSuccessfully = false;
        boolean blnDBCommittedSuccessfully = false;

        FileStatus fileStatus = null;
        MessageStatus messageStatus = null;
        DBStatus dbStatus = null;

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(xmlResultSet.getBytes()));
            NodeList tables = doc.getElementsByTagName("Table");

            if (tables != null && tables.getLength() > 0) {
                for (int i = 0; i < tables.getLength(); i++) {
                    Node table = tables.item(i);
                    if (table.getNodeType() == Node.ELEMENT_NODE) {
                        Element tableElement = (Element) table;

                        NodeList fileStatusNodes = tableElement.getElementsByTagName("filestatus");
                        NodeList msgStatusNodes = tableElement.getElementsByTagName("msgstatus");
                        NodeList dbStatusNodes = tableElement.getElementsByTagName("dbstatus");

                        String fs = "", ms = "", ds = "";

                        if (fileStatusNodes != null && fileStatusNodes.getLength() > 0) {
                            fs = fileStatusNodes.item(0).getTextContent();
                        }
                        if (msgStatusNodes != null && msgStatusNodes.getLength() > 0) {
                            ms = msgStatusNodes.item(0).getTextContent();
                        }
                        if (dbStatusNodes != null && dbStatusNodes.getLength() > 0) {
                            ds = dbStatusNodes.item(0).getTextContent();
                        }

                        if (fs.equalsIgnoreCase(FileStatus.SaveToFolder.name())) {
                            if (ms.equalsIgnoreCase(MessageStatus.Success.name())) {
                                fileStatus = FileStatus.SaveToFolder;
                                messageStatus = MessageStatus.Success;
                                blnFileStatusSuccess = true;
                            } else if (ms.isEmpty()) {
                                blnFileIsInProgress = true;
                                break;
                            }
                        }
                        if (ms.isEmpty()) {
                            if (ds.equalsIgnoreCase(DBStatus.ConnectionOpen.name())) {
                                blnFileIsInProgress = true;
                                dbStatus = DBStatus.ConnectionOpen;
                                break;
                            }
                        } else if (ms.equalsIgnoreCase(MessageStatus.Success.name())) {
                            if (ds.equalsIgnoreCase(DBStatus.TransactionRollback.name())) {
                                blnDBStatusSuccess = true;
                                blnDBRolledBackSuccessfully = true;
                                dbStatus = DBStatus.TransactionRollback;
                                break;
                            } else if (ds.equalsIgnoreCase(DBStatus.TransactionCommit.name())) {
                                blnDBStatusSuccess = true;
                                blnDBCommittedSuccessfully = true;
                                dbStatus = DBStatus.TransactionCommit;
                                break;
                            } else if (ds.equalsIgnoreCase(DBStatus.ConnectionClose.name())) {
                                blnDBStatusSuccess = true;
                                dbStatus = DBStatus.ConnectionClose;
                                break;
                            }
                        }
                    }
                }
            } else {
                ResponseMessage responseMessage = new ResponseMessage();
                responseMessage.TableResults = new ArrayList<>();
                responseMessage.RequestId = requestId;
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "RequestId(s) not found",
                        FileStatus.NA, DBStatus.NA, MessageStatus.Failure, true);
                responseMessage.TableResults.add(r);
                try {
                    Marshaller jaxbMarshaller = XMLUtils.getMarshaller(ResponseMessage.class);
                    return XMLUtils.serializeToString(responseMessage, jaxbMarshaller);
                } catch (JAXBException e) {
                    LOGGER.error("Error serializing ResponseMessage to xml", e);
                    return String.format("Sorry! there was an error encountered while serializing ResponseMessage ... Cause: %s ##", e.getMessage());
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOGGER.error("Error reading XML", e);
            return String.format("Sorry! there was an error encountered while reading XML ... Cause: %s ##", e.getMessage());
        }


        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.TableResults = new ArrayList<>();
        responseMessage.RequestId = requestId;
        if (blnFileIsInProgress) {
            TableResult r = XMLUtils.GetResponseXMLForATran("", "", "File processing is in progress",
                    fileStatus, DBStatus.NA, messageStatus, true);
            responseMessage.TableResults.add(r);
            try {
                Marshaller jaxbMarshaller = XMLUtils.getMarshaller(ResponseMessage.class);
                return XMLUtils.serializeToString(responseMessage, jaxbMarshaller);
            } catch (JAXBException e) {
                LOGGER.error("Error serializing ResponseMessage to xml", e);
                return String.format("Sorry! there was an error encountered while serializing ResponseMessage ... Cause: %s ##", e.getMessage());
            }
        }
        if (blnDBStatusSuccess) {
            if (blnDBCommittedSuccessfully) {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "DB Transaction Completed",
                        fileStatus, dbStatus, messageStatus, true);
                responseMessage.TableResults.add(r);
            }
            if (blnDBRolledBackSuccessfully) {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "DB Transaction Rolled Back",
                        fileStatus, dbStatus, messageStatus, true);
                responseMessage.TableResults.add(r);
            }
            if (!blnDBCommittedSuccessfully && !blnDBRolledBackSuccessfully) {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "DB Transaction is in progress",
                        fileStatus, dbStatus, messageStatus, true);
                responseMessage.TableResults.add(r);
            }
        } else {
            if (blnFileStatusSuccess) {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "File Save to Folder Successfully",
                        fileStatus, DBStatus.NA, messageStatus, true);
                responseMessage.TableResults.add(r);
            } else {
                TableResult r = XMLUtils.GetResponseXMLForATran("", "", "Either RequestId(s) not found or File Save to Folder Failed",
                        fileStatus, DBStatus.NA, messageStatus, true);
                responseMessage.TableResults.add(r);
            }
        }
        try {
            Marshaller jaxbMarshaller = XMLUtils.getMarshaller(ResponseMessage.class);
            return XMLUtils.serializeToString(responseMessage, jaxbMarshaller);
        } catch (JAXBException e) {
            LOGGER.error("Error serializing ResponseMessage to xml", e);
            return String.format("Sorry! there was an error encountered while serializing ResponseMessage ... Cause: %s ##", e.getMessage());
        }
    }
*/

    /*public synchronized InputStream tablost(String data) {
        Gson json = new Gson();
        try {
            Type empMapType = new TypeToken<Map<String, String>>() {
            }.getType();
            Map<String, String> jsonData = new HashMap<>();
            jsonData = json.fromJson(data, empMapType);

            String mobileNumber = jsonData.get("mobileNumber");
            String databaseName = jsonData.get("masterDB");
            String userType = jsonData.get("userType");
            Connection con = ds.getConnection(databaseName);
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery("Select Inst_Database,UserId,isValidated,InstituteId,isTabLost,UserRole from tblinstusers where UserMobileNumber = '" + mobileNumber + "'");
            if (resultSet.next()) {
                if (resultSet.getString(6).equals(userType)) {
                    if (resultSet.getString(3).equals("1")) {
                        if (resultSet.getString(5).equals("1")) {
                            String dbName = resultSet.getString(1);
                            File tablostFile = new File(appConfig.exportDirectory, userType + "_tablost_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmms")) + ".json");
                            try (JsonWriter jw = new JsonWriter(new FileWriter(tablostFile))) {
                                getTabLostData(dbName, resultSet.getString(2), jw, databaseName,userType);
                                jw.flush();
                                return new FileInputStream(tablostFile);
                            } catch (Exception e) {
                                String msg = json.toJson(new TablostResponse(e.getMessage(),true));
                                return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
                            }
                        } else {
                            String msg = json.toJson(new TablostResponse("Please Contact Sentia Team",true));
                            return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
                        }
                    } else {
                        String msg = json.toJson(new TablostResponse("The user is not validated",true));
                        return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
                    }
                } else {
                    String msg = json.toJson(new TablostResponse(" User not Found",true));
                    return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
                }
            } else if (resultSet.getRow() == 0) {
                String msg = json.toJson(new TablostResponse("No user found",true));
                return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
            }
            String msg = json.toJson(new TablostResponse("e.getMessage()",false));
            return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
            String msg = json.toJson(new TablostResponse(e.getMessage(),true));
            return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
        }
    }*/

    private void getTabLostData(String clientDB, String userId, JsonWriter jw, String masterDb, String userType) throws Exception {
        TabLostTnandCol tabLostTnandCol = new TabLostTnandCol();
        jw.beginArray();
        if (userType.equalsIgnoreCase("bc")) {
            createConnectionandGetData(jw, clientDB, userId, tabLostTnandCol, tabLostTnandCol.tablostTables_BC, clientDB);//to get data from client db
        } else if (userId.equalsIgnoreCase("pc")) {
            createConnectionandGetData(jw, clientDB, userId, tabLostTnandCol, tabLostTnandCol.tablostTables_PC, clientDB);//to get data from client db
        }
        createConnectionandGetData(jw, masterDb, userId, tabLostTnandCol, tabLostTnandCol.tablostTables_masterDb, clientDB); //to get data from master db
        jw.endArray();
    }

    private void createConnectionandGetData(JsonWriter jw, String db, String userId, TabLostTnandCol tabLostTnandCol, List<String> tables, String clientDB) {
        try (Connection con = ds.getConnection(db)) {
            Connection tempCon = null;
            Statement stat1 = con.createStatement();
            try {
                Gson gson = new Gson();
                for (String tn : tables) {
                    StringBuilder sqlBuilder = new StringBuilder();
                    if (tn.equalsIgnoreCase("tblinstitutiondetails")) {
                        sqlBuilder.append(String.format("SELECT * FROM %s", tn));
                    } else {
                        sqlBuilder.append(String.format("SELECT * FROM %s WHERE UserId = %s", tn, userId));
                    }

                    sqlBuilder.append(" ");
                    String groupBy = tabLostTnandCol.groupByForTable.get(tn);
                    sqlBuilder.append(groupBy == null ? "" : groupBy);

                    ResultSet tableRS = stat1.executeQuery(sqlBuilder.toString());
                    ResultSetMetaData rsm = tableRS.getMetaData();
                    int columnCount = rsm.getColumnCount();
                    ResultSet clientResultSet = null;
                    if (tn.equalsIgnoreCase("tblinstusers")) { //ramesh 18-Aug-2022 to add the client data for lastNumber to records
                        tempCon = ds.getConnection(clientDB);
                        Statement st = tempCon.createStatement();
                        clientResultSet = st.executeQuery(String.format("SELECT * FROM tblinstusers WHERE UserId = %s", userId));
                        clientResultSet.next();
                    }
                    while (tableRS.next()) {
                        JsonObject row = new JsonObject();
                        row.addProperty("tn", tn);
                        for (int i = 1; i <= columnCount; i++) {
                            String cn = rsm.getColumnName(i);
                            if (tabLostTnandCol.tablostSkipColumns.contains(cn.toLowerCase())) {
                                continue;
                            }
                            if (tn.equalsIgnoreCase("tblinstusers")) {
                                if (tabLostTnandCol.tablostSkipColumns_insttn.contains(cn.toLowerCase())) {
                                    continue;
                                }
                            }
                            int ct = rsm.getColumnType(i);
                            boolean numericVal = ct == Types.INTEGER || ct == Types.BIGINT || ct == Types.NUMERIC;
                            if (numericVal) {
                                if (tn.equalsIgnoreCase("tblinstusers") && (tabLostTnandCol.tablostInstClientColumns.contains(cn.toLowerCase()))) {
                                    row.addProperty(cn, clientResultSet != null ? clientResultSet.getInt(cn) : 0);
                                } else {
                                    row.addProperty(cn, tableRS.getInt(i));
                                }
                            } else {
                                row.addProperty(cn, tableRS.getString(i));
                            }
                        }
                        gson.toJson(row, jw);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            } finally {
                if (tempCon != null) {
                    tempCon.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public InputStream tablost2(String body, DataHandler dataHandler) {
        Gson json = new Gson();
        try {
            TablostRequest tablostRequest = json.fromJson(body, TablostRequest.class);
            String mobileNumber = tablostRequest.getMobileNo();
            String databaseName = tablostRequest.getMasterDB();
            String userType = tablostRequest.getUserType();
            //   String requestId = tablostRequest.getRequestId();
            try (Connection con = ds.getConnection(databaseName)) {
                Statement statement = con.createStatement();
                ResultSet resultSet = statement.executeQuery("Select Inst_Database,UserId,isValidated,InstituteId,isTabLost,UserRole from tblinstusers where UserMobileNumber = '" + mobileNumber + "'");
                if (resultSet.next()) {
                    if (resultSet.getString(6).equals(userType)) {
                        if (resultSet.getString(3).equals("1")) {
                            if (resultSet.getString(5).equals("1")) {
                                String dbName = resultSet.getString(1);
                                tablostRequest.setClientDB(dbName);
                                tablostRequest.setUserId(resultSet.getString(2));
                                FetchResponse fetchRes = checkandDoTabLost(tablostRequest, dataHandler);
                                String msg = json.toJson(new TablostResponse(fetchRes.getStatus(), false, true, fetchRes));
                                return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
                            } else {
                                String msg = json.toJson(new TablostResponse("Please Contact Sentia Team", true, false, null));
                                return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
                            }
                        } else {
                            String msg = json.toJson(new TablostResponse("The user is not validated", true, false, null));
                            return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
                        }
                    } else {
                        String msg = json.toJson(new TablostResponse("Userrole mismatch", true, false, null));
                        return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
                    }
                } else if (resultSet.getRow() == 0) {
                    String msg = json.toJson(new TablostResponse("No user found", true, false, null));
                    return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
                }
                String msg = json.toJson(new TablostResponse("", false, false, null));
                return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
            } catch (Exception e) {
                String msg = json.toJson(new TablostResponse(e.getMessage(), true, false, null));
                return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
            }
        } catch (Exception e) {
            e.printStackTrace();
            String msg = json.toJson(new TablostResponse(e.getMessage(), true, false, null));
            return new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
        }
    }

    private FetchResponse checkandDoTabLost(TablostRequest tablostRequest, DataHandler handler) {
        String status = getStatusofTabLost(tablostRequest);
        if (status.equals(FetchStatus.NotInitiated.name())) {
            File tablostFile = new File(appConfig.exportDirectory, tablostRequest.userType + "_tablost_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmms")) + ".json");
            handler.createTabLostrecord(tablostRequest, tablostFile);
            GenTabLost(tablostRequest, tablostFile);
            return new FetchResponse(tablostRequest.userId, tablostRequest.requestId, "Data Fetch Initiated",
                    "Data fetch is Initiated, Please wait while data is generated and Downloaded", "", "");
        } else if (status.equals(FetchStatus.Success.name())) {
            return new FetchResponse(tablostRequest.userId, tablostRequest.requestId, "Success",
                    "Data downloading, Please wait...", "", "");
        } else if (status.equals(FetchStatus.Failed.name())) {
            return new FetchResponse(tablostRequest.userId, tablostRequest.requestId, "Failed",
                    "Server Error", "", "");
        } else {
            return new FetchResponse(tablostRequest.userId, tablostRequest.requestId, "Data Fetch not yet Available",
                    "Data which was requested is not yet available to download, Please wait...", "", "");
        }
    }

    private String getStatusofTabLost(TablostRequest request) {
        Connection conn = null;
        try {
            conn = ds.getConnection(request.getClientDB());
            String sql = "Select * from tblServerDatafetchlog where requestId = ? and userId = ? ;";
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setString(1, request.getRequestId());
            stat.setString(2, request.getUserId());
            ResultSet result = stat.executeQuery();
            if (result.next()) {
                String status = result.getString("status");
                if (!status.isEmpty()) {
                    if (status.equalsIgnoreCase(FetchStatus.Initiated.name())) {
                        conn.close();
                        return FetchStatus.Initiated.name();
                    } else if (status.equalsIgnoreCase(FetchStatus.Success.name())) {
                        conn.close();
                        return FetchStatus.Success.name();
                    } else if (status.equalsIgnoreCase(FetchStatus.Failed.name())) {
                        conn.close();
                        return FetchStatus.Failed.name();
                    }
                }
            }
            conn.close();
            return FetchStatus.NotInitiated.name();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            return FetchStatus.NotInitiated.name();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized InputStream downloadDataFetch(String body, DataHandler dataHandler) {
        try {
            Gson gson = new Gson();
            TablostRequest tabreq = gson.fromJson(body, TablostRequest.class);
            Connection con = ds.getConnection(tabreq.masterDB);
            String clientDB = dataHandler.getOrThrow(con, tabreq.mobileNo);
            con = ds.getConnection(clientDB);
            String sql = "Select * from tblServerDatafetchlog where requestId = ?;";
            PreparedStatement stat = con.prepareStatement(sql);
            stat.setString(1, tabreq.requestId);
            ResultSet res = stat.executeQuery();
            String fileName = "";
            if (res.next()) {
                if (res.getString("status").equals(FetchStatus.Success.name())) {
                    fileName = res.getString("fileName");
                }
            }
            con.close();
            System.out.printf("Downloading file for %s%n", tabreq.requestId);
            File file = new File(appConfig.exportDirectory, fileName);
            fileName = file.getPath();
            if (fileName.length() > 0) {
                file = new File(fileName);
                if (file.exists()) {
                    return new FileInputStream(file);
                } else {
                    return new ByteArrayInputStream("File not Found".getBytes(StandardCharsets.UTF_8));
                }
            }
            return new ByteArrayInputStream("File not Found".getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
            return new ByteArrayInputStream(e.getMessage().getBytes(StandardCharsets.UTF_8));
        }
    }

    private void GenTabLost(TablostRequest request, File tablostFile) {
        CompletableFuture.supplyAsync(() -> {
            try (JsonWriter jw = new JsonWriter(new FileWriter(tablostFile))) {
                getTabLostData(request.clientDB, request.userId, jw, request.masterDB, request.userType);
                jw.flush();
                MessageDigest md = MessageDigest.getInstance("MD5");
                FileInputStream fis = new FileInputStream(tablostFile);

                byte[] byteArray = new byte[1024];
                int bytesCount;
                while ((bytesCount = fis.read(byteArray)) != -1) {
                    md.update(byteArray, 0, bytesCount);
                }
                fis.close();
                byte[] digest = md.digest();
                StringBuilder sb = new StringBuilder();
                /*for (int i = 0; i < digest.length; i++) {
                    sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                }*/
                for (byte b : digest) {
                    sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
                }
                String checkSum = sb.toString();
                long sizeLong = Files.size(tablostFile.toPath());
                sizeLong = sizeLong / 1024;
                String size = String.valueOf(sizeLong);
                updateTablostLog(request, size, checkSum, FetchStatus.Success.name(), "");
            } catch (Exception e) {
//                Gson gson = new Gson();
//                String msg = gson.toJson(new TablostResponse(e.getMessage(),true,false,null));
                try {
                    updateTablostLog(request, "0", "", FetchStatus.Failed.name(), e.getMessage());
                } catch (Exception ex) {
                    ex.printStackTrace();
                    LOGGER.error(String.format("Tablost request %s- %s", request.requestId, e));
                }
            }
            return false;
        });
    }

    private void updateTablostLog(TablostRequest request, String size, String checkSum, String status, String comments) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection(request.clientDB);
            String sql = "Select * from tblServerDatafetchlog where requestId =? and userId = ?;";
            PreparedStatement statment = conn.prepareStatement(sql);
            statment.setString(1, request.requestId);
            statment.setString(2, request.userId);
            ResultSet res = statment.executeQuery();
            if (res.next()) {
                tblServerDataFetchLog log = new tblServerDataFetchLog();
                log.setId(res.getString(1));
                log.setUserId(res.getString(2));
                log.setUserType(res.getString(3));
                log.setRequestId(res.getString(4));
                log.setRequestSentAt(res.getString(5));
                log.setRequestReceivedAt(res.getString(6));
                log.setRestMethodName(res.getString(7));
                log.setFileName(res.getString(8));
                log.setFileSize(res.getString(9));
                log.setCheckSum(res.getString(10));
                log.setFileGeneratedAt(res.getString(11));
                log.setFileCompletedAt(res.getString(12));
                log.setStatus(res.getString(13));
                log.setComments(res.getString(14));

                DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:s");
                LocalDateTime time = LocalDateTime.now();
                String currTime = sdf.format(time);
                log.setFileCompletedAt(currTime);
                log.setStatus(status);
                log.setComments(comments);
                log.setFileSize(size);
                log.setCheckSum(checkSum);

                String updateSql = "Update tblServerDatafetchlog set fileCompletedAt = ?, status = ?, comments =?, fileSize = ?,checkSum = ?  where requestId = ?;";
                PreparedStatement updateStat = conn.prepareStatement(updateSql);
                updateStat.setString(1, log.getFileCompletedAt());
                updateStat.setString(2, log.getStatus());
                updateStat.setString(3, log.getComments());
                updateStat.setString(4, log.getFileSize());
                updateStat.setString(5, log.getCheckSum());
                updateStat.setString(6, log.getRequestId());
                updateStat.executeUpdate();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), "Connection closure throw error");
            e.printStackTrace();
            throw e;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException exx) {
                    LOGGER.error(exx.getMessage(), "Connection closure throw error");
                    exx.printStackTrace();
                    throw exx;
                }
            }
        }
    }

    public String getSyncLogStatus(Context ctx) {
        Connection con = null;
        try {
            String data = ctx.body();
            Gson gson = new Gson();
            SyncLogRequest request = gson.fromJson(data, SyncLogRequest.class);
            con = ds.getConnection(request.schemaName);
            SyncLogResponse response = new SyncLogResponse();
            ArrayList<ReqIDStatus> reqStatus = new ArrayList<>();
            for (String reqId : request.requestId) {
                ReqIDStatus res = businessHandler.getSyncLog_MySQL(reqId, request.userId, con);
                if (res != null) {
                    reqStatus.add(res);
                }
            }
            response.requestIDStatus = reqStatus;
            response.userId = request.userId;
            return gson.toJson(response);
        } catch (Exception e) {
            e.printStackTrace();
            return "Error";
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public String hikariData() {
        try {
            HikariDataSource hikari = ds.getHikariDataSource();
            HikariConfigMXBean hikariConfig = ds.getHikariDataSource().getHikariConfigMXBean();
            HikariPoolMXBean bean = hikari.getHikariPoolMXBean();
            String[] url = hikari.getJdbcUrl().split("/");
            String schema = url[url.length - 1];
            System.out.println(schema);
            HikariDots hikariDots = new HikariDots(bean.getTotalConnections(), bean.getActiveConnections(), bean.getIdleConnections(), bean.getThreadsAwaitingConnection(), true, schema);
            if (bean.getActiveConnections() == hikariConfig.getMaximumPoolSize()) {

                hikariDots.setCanProcessRequest(false);

            }
            Gson g = new Gson();
            return g.toJson(hikariDots);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public String dbReport(Context ctx) {
        try {
            String body = ctx.body();
            Gson gson = new Gson();
            ReportRequest request = gson.fromJson(body, ReportRequest.class);
            if (request == null) {
                return "";
            }
            if (request.getMasterDbName().isEmpty()) {
                return "";
            }
            String sql = "Select * from tblDbReportAutoLogs where userId = '" + request.getUserId() + "' and isGenerated = 0";
            return businessHandler.getdataHandle().getDataAsJson(sql, request.getMasterDbName(), "tblDbReportAutoLogs");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String ValidUserForDownLoad(String json) {

        Gson gson = new Gson();
        GetValidateUserForLoginRequest request = gson.fromJson(json, GetValidateUserForLoginRequest.class);
        String defaultData = "";
        if (!request.DBname.isEmpty()) {
            String result = businessHandler.getValidateUserForLoginData(request.mobileNumber, request.Pswd, request.DBname);
            defaultData = defaultData + result;
        }
        if (defaultData.isEmpty()) {
            return "<mainTag></mainTag>";
        } else {
            return "<mainTag>" + defaultData + "</mainTag>";
        }

    }

    /*public InputStream activityCalenderJsonDownload(Context ctx) {
        try {
            String filename = ctx.formParam("filename");
            
            String path = appConfig.WebServerUploaded;
            path = path.replace("SentiacareMC_CCServices", "SentiacareACHFServices");
            File file = new File(path + "/" + filename + ".json");
            if (file.exists()) {
                return new FileInputStream(file);
            } else {
                return new StringBufferInputStream("Activity Calender JSON File not Found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new StringBufferInputStream(e.getMessage());
        }
    }
*/

    public List<InputStream> activityCalenderJsonFileFetch(Context ctx) {
        try {
            // String filenames = ctx.formParam("filename");
            String filenames = "29-06-2023_20047.json, 30-06-2023_20047.json";
            String path = appConfig.WebServerUploaded;
            List<String> jsonFileNameInDir = getStoredActivityCalenderFilesNames();

            filenames = filenames.trim();
            String[] filenameArray = filenames.split(",");
            List<String> filenameList = new ArrayList<>();
            for (String filename : filenameArray) {
                filenameList.add(filename.trim());
            }

            List<InputStream> filesToReturn = new ArrayList<>();

            for (String jsonFileName : jsonFileNameInDir) {
                if (!filenameList.contains(jsonFileName)) {
                    try {
                        File file = new File(path, jsonFileName);
                        InputStream inputStream = new FileInputStream(file);
                        filesToReturn.add(inputStream);
                    } catch (FileNotFoundException e) {

                        e.printStackTrace();
                        return (List<InputStream>) new StringBufferInputStream("Error Occured");
                    }
                }
            }

            return filesToReturn;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

/*

    public InputStream activityCalenderJsonDownload(Context ctx) throws FileNotFoundException {
        String filenames = ctx.formParam("filename");
        String path = appConfig.WebServerUploaded;

        assert filenames != null;
        if (filename.equalsIgnoreCase("noJsonFile")) {

        } else {

        }

        File file = new File(path + "/" + filename + ".json");
        if (file.exists()) {
            return new FileInputStream(file);
        } else {
            return new StringBufferInputStream("Activity Calender JSON File not Found");
        }
    }
*/

    public List<String> getStoredActivityCalenderFilesNames() {
        String directoryPath = "C:\\SentiacareUAT\\SentiacareACHFServices\\uploaded";
        try {
            List<String> fileNames = new ArrayList<>();
            File directory = new File(directoryPath);

            if (directory.exists() && directory.isDirectory()) {
                File[] files = directory.listFiles();

                for (File file : files) {
                    if (file.isFile()) {
                        fileNames.add(file.getName());
                    }
                }
            }

            return fileNames;
        } catch (Exception e) {
            throw new RuntimeException(e);

        }
    }


    public InputStream activityCalenderJsonFileDownload(Context ctx) { //July2023 Rakesh
        try {
            String filename = ctx.formParam("filename");

            String path = appConfig.achfActivityJsonFileDirectory;
            File file = new File(path + "/" + filename + ".json");
            if (file.exists()) {
                ctx.status(404).result("JSON file not found");
                return null;
            } else {
                return new StringBufferInputStream("Json file not Found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            ctx.status(500).result("Internal server error");
            return null;
        }
    }

    public String uploadJsonFile(Context ctx) {
        try {
            String filename = ctx.formParam("filename");
            UploadedFile file;
            if (filename != null && filename.trim().length() > 0) {
                file = ctx.uploadedFile(filename);

                FileUtil.streamToFile(Objects.requireNonNull(file).getContent(), appConfig.WebServerActivityCalendarFilePath + "/" + file.getFilename() + ".json");
                File uploadedFile = new File(appConfig.WebServerActivityCalendarFilePath + "/" + file.getFilename() + ".json");
                if (uploadedFile.exists()) {
                    return "Uploaded";
                } else {
                    return "Upload Failed";
                }
            } else {
                return "Upload Failed";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

   /* public void activityCalenderJsonFileDownloadPc(Context ctx) { // 24July2023 Rakesh
        try {
            String filename = ctx.formParam("filename");
            String userType = ctx.formParam("userType");

            String path = "";
            if (userType != null) {
                if (userType.equalsIgnoreCase("ANM") || userType.equalsIgnoreCase("ACHF")) {
                    path = appConfig.achfActivityJsonFileDirectory;
                } else if (userType.equalsIgnoreCase("BC")) {
                    path = appConfig.WebServerActivityCalendarFilePath;
                }
            }

            File file = new File(path + "/" + filename + ".json");
            if (file.exists()) {
                // Set the necessary response headers for download
                ctx.header("Content-Disposition", "attachment; filename=" + filename + ".json");
                ctx.header("Content-Type", "application/json");

                // Send the file as the response
                ctx.result(String.valueOf(file));
            } else {
                ctx.status(404).result("JSON file not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            ctx.status(500).result("Internal server error");
        }
    }*/


    public InputStream activityCalenderJsonFileDownloadPc(Context ctx) { // 24July2023 Rakesh
        try {
            String filename = ctx.formParam("filename");
            String userType = ctx.formParam("userType");

//            System.out.println("file name: "+filename+" usertype:"+userType);

            String path = "";
            if (userType != null) {
                if (userType.equalsIgnoreCase("ANM") || userType.equalsIgnoreCase("ACHF")) {
                    path = appConfig.achfActivityJsonFileDirectory;
                } else if (userType.equalsIgnoreCase("BC")) {
                    path = appConfig.WebServerActivityCalendarFilePath;
                }
            }

//            System.out.println("path: "+path);

            File file = new File(path, filename + ".json");
            if (file.exists()) {
//                System.out.println("file: "+file.getPath());
                return new FileInputStream(file);
            } else {
//                System.out.println("file: not found");

                ctx.status(404).result("JSON file not found");
                return null;
            }
        } catch (FileNotFoundException e) {
//            System.out.println(e);
            ctx.status(404).result("JSON file not found");
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            ctx.status(500).result("Internal server error");
//            System.out.println(e);

            return null;
        }
    }


    //    09Feb2025 Arpitha
   public String checkAndGetReallocationDetails(Context ctx) {

       String dbName = ctx.formParam("dbName");
       String villageId = ctx.formParam("villageId");
        businessHandler.checkAndGetReallocationData(villageId,dbName);

        return "";
    }


}
