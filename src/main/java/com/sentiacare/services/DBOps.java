package com.sentiacare.services;

import com.sentiacare.entities.BatchItem;
import com.sentiacare.entities.DBStatus;
import com.sentiacare.entities.StatementType;
import com.sentiacare.repositories.DataHandler;
import com.sentiacare.utils.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class DBOps {

    private static final Logger LOGGER = LogManager.getLogger(DBOps.class.getName());
    private final DataHandler dataHandler;
    private final DocumentBuilderFactory dbf;
    private final File processedDir;
    private final File unprocessedDir;

    public DBOps(DataHandler dh, DocumentBuilderFactory dbf, AppConfig appConfig) {
        this.dataHandler = dh;
        this.dbf = dbf;
        processedDir = new File(appConfig.WebServerProcessedFolderName);
        unprocessedDir = new File(appConfig.WebServerUnprocessedFolderName);
    }

    public DBOpsStatus insertOrUpdate(File file, String requestId, String dbName) {
        try {
            List<BatchItem> statementsToRun = new ArrayList<>();
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            Document doc = documentBuilder.parse(file);
            NodeList tableNameNodes = doc.getElementsByTagName("tablename");
            for (int tableIndex = 0; tableIndex < tableNameNodes.getLength(); tableIndex++) {
                Node tableNameNode = tableNameNodes.item(tableIndex);
                Element tableNameParent = (Element) tableNameNode.getParentNode();

                String sqlAction = tableNameParent.getElementsByTagName("action").item(0).getTextContent().trim();
                switch (sqlAction) {

                    case "I": {
                        try {
                            String sQuery = "Insert into " + tableNameNode.getTextContent();
                            sQuery = sQuery + " (";
                            String columnValue = " (";

                            NodeList childNodes = tableNameParent.getChildNodes();
                            for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++) {
                                Node childNode1 = childNodes.item(childIndex);
                                if (childNode1.getNodeType() == Node.ELEMENT_NODE) {
                                    if (!childNode1.getNodeName().equals("tablename") && !childNode1.getNodeName().equals("action")) {
                                        sQuery += childNode1.getNodeName() + ",";
                                        columnValue = columnValue + "'" + childNode1.getTextContent() + "',";

                                    }
                                }
                            }
                            sQuery = sQuery.substring(0, sQuery.length() - 1);
                            columnValue = columnValue.substring(0, columnValue.length() - 1);
                            sQuery = sQuery + ") values " + columnValue + ")";
                            statementsToRun.add(new BatchItem(sQuery, StatementType.Insert));
                            break;
                        } catch (Exception e) {
                            LOGGER.error("Could not prepare Insert Statement in File {} ", file.getPath(), e);
                            break;
                        }
                    }

                    case "U": {
                        try {
                            String s = tableNameParent.getElementsByTagName("SQLSTATEMENT").item(0).getTextContent();
                            statementsToRun.add(new BatchItem(s, StatementType.Update));
                            break;
                        } catch (Exception ex) {
                            LOGGER.error("Could not prepare Update Statement in File {} ", file.getPath(), ex);
                            break;
                        }
                    }
                }
            }
            return executeStatements(statementsToRun, requestId, dbName, file);
        } catch (Exception ex) {
            LOGGER.error("File {} could not be processed", file.getPath(), ex);
            dataHandler.markFileErrorWithRollback(file.getName(), dbName);
            move(file, false);
            return null;
        }
    }

    private DBOpsStatus executeStatements(List<BatchItem> batchItems, String RequestId, String dbname, File file) throws Exception {
        DBOpsStatus dbOpsStatus = dataHandler.doBatch(batchItems, RequestId, dbname);
        if (dbOpsStatus.dbStatus == DBStatus.TransactionCommit) {
            move(file, true);
        } else if (dbOpsStatus.dbStatus == DBStatus.TransactionRollback) {
            move(file, false);
        }
        return dbOpsStatus;
    }

    private void move(File source, boolean processed) {
        try {
            File targetDir = processed ? processedDir : unprocessedDir;
            Files.move(source.toPath(), new File(targetDir, source.getName()).toPath());
        } catch (IOException e) {
            LOGGER.error("File {} could not be moved to {} folder", source, processed ? "processed" : "unprocessed", e);
        }
    }
}
