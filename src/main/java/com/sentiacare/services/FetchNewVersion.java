package com.sentiacare.services;

import com.google.gson.Gson;
import com.sentiacare.dtos.FetchRequest;
import com.sentiacare.repositories.FullSchemaRepository;
import com.sentiacare.utils.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


public class FetchNewVersion {
    final Logger LOGGER = LogManager.getLogger(FetchNewVersion.class);
    private FullSchemaRepository repository;
    private Exporter export;
    private AppConfig appConfig;
    Gson gson = new Gson();

    public FetchNewVersion(FullSchemaRepository repository,AppConfig appConfig) {
        this.appConfig = appConfig;
        this.repository = repository;
    }
    public FetchNewVersion(Exporter export) {
        this.export = export;
    }


    public String checktheRequest(String data, String mtdName) throws Exception{
        FetchRequest request = gson.fromJson(data,FetchRequest.class);
        return  export.exportNew(request,mtdName); //09Feb2025 Bindu - pass mtd name
    }

    public synchronized InputStream downloadDataFetch(String body) {
        try{
            FetchRequest fetchReq = gson.fromJson(body, FetchRequest.class);
            String fileName = export.getDownloadFile(fetchReq);
            if(fileName.length()>0){
                File file= new File(fileName);
                if(file.exists()){
                    return new FileInputStream(file);
                }else{
                    return new ByteArrayInputStream("File not Found".getBytes(StandardCharsets.UTF_8));
                }
            }
            return new ByteArrayInputStream("File not Found".getBytes(StandardCharsets.UTF_8));
        }catch (Exception e){
            e.printStackTrace();
            return new ByteArrayInputStream(e.getMessage().getBytes(StandardCharsets.UTF_8));
        }
    }




//    07Sep2024 Arpitha

    public String checktheRequestReallocation(String data, String mtdName) throws Exception{

        FetchRequest request = gson.fromJson(data,FetchRequest.class);
        return  export.exportNewRellocation(request,mtdName); //09Feb2025 Bindu - pass the mtdname
    }
}
