package com.sentiacare.services;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.sentiacare.dtos.ExportRequest;
import com.sentiacare.dtos.ExportResponse;
import com.sentiacare.dtos.FetchRequest;
import com.sentiacare.dtos.FetchResponse;
import com.sentiacare.entities.tblServerDataFetchLog;
import com.sentiacare.repositories.DataHandler;
import com.sentiacare.repositories.FullSchemaRepository;
import com.sentiacare.repositories.SentiaPooledDataSource;
import com.sentiacare.utils.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CompletableFuture;

public class Exporter {

    private static final Logger LOGGER = LogManager.getLogger(Exporter.class);
    private final FullSchemaRepository repository;
    private final Gson gson = new Gson();
    private final DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyyMMddHHmms");
    private final AppConfig appConfig;

    public Exporter(FullSchemaRepository repository, AppConfig appConfig) {
        this.repository = repository;
        this.appConfig = appConfig;
    }

    public synchronized InputStream export(String exportRequest) throws IOException {
        LOGGER.info("Export started");
        File f = new File(appConfig.exportDirectory, "sentiacare_mc_export_" + LocalDateTime.now().format(df1) + ".json");
//        File f = new File(appConfig.exportDirectory, "sentiacare_mc_export.json");
        try (JsonWriter jw = new JsonWriter(new FileWriter(f))) {
            ExportRequest r = gson.fromJson(exportRequest, ExportRequest.class);
            repository.export(jw, r, appConfig);
            jw.flush();
        } catch (Exception e) {
            LOGGER.error(String.format("Export error for request %s", exportRequest), e);
            return new ByteArrayInputStream(e.getMessage().getBytes(StandardCharsets.UTF_8));
        }
        LOGGER.info("Export complete");
        return new FileInputStream(f);
    }

    /*public synchronized InputStream exportMansiMitra(String exportRequest) throws IOException {
        LOGGER.info("Export started");
//        File f = new File(appConfig.exportDirectory, "sentiacare_mc_exportmansimitra_" + LocalDateTime.now().format(df1) + ".json");
        File f = new File(appConfig.exportDirectory, "sentiacare_mc_exportmansimitra.json");
        try (JsonWriter jw = new JsonWriter(new FileWriter(f))) {
            ExportRequest r = gson.fromJson(exportRequest, ExportRequest.class);
            repository.exportMansiMitra(jw, r);
            jw.flush();
        } catch (Exception e) {
            LOGGER.error(String.format("Export error for request %s", exportRequest), e);
            return new ByteArrayInputStream(e.getMessage().getBytes(StandardCharsets.UTF_8));
        }
        LOGGER.info("Export complete");
        return new FileInputStream(f);
    }*/

    public synchronized InputStream exportMansiMitra(String exportRequest) throws IOException {
        LOGGER.info("Export started");
        File f = new File(appConfig.exportDirectory,
                "sentiacare_mc_exportmansimitra_" + LocalDateTime.now().format(df1) + ".json");
        try (JsonWriter jw = new JsonWriter(new FileWriter(f))) {
            ExportRequest r = gson.fromJson(exportRequest, ExportRequest.class);
            repository.exportMansiMitra(jw, r,appConfig);
            jw.flush();
        } catch (Exception e) {
            LOGGER.error(String.format("Export error for request %s", exportRequest), e);
            return new ByteArrayInputStream(e.getMessage().getBytes(StandardCharsets.UTF_8));
        }
        LOGGER.info("Export complete");
        return new FileInputStream(f);
    }

    public String exportNew(FetchRequest r, String mtdName) throws IOException {
        File f = new File(appConfig.exportDirectory,
                r.userType+"_fetch_" + LocalDateTime.now().format(df1) + "_" + r.userId + ".json");
        return downloadInitiated(f, r,mtdName); //09Feb2025 Bindu - pass mtd name
    }
    private String downloadInitiated(File f, FetchRequest fetchRequest, String mtdName) {
        try {
            SentiaPooledDataSource ds = repository.getDataSource();
            DataHandler dataHandler = new DataHandler(ds);
            ExportRequest exportRequest = convertFetchtoExport(fetchRequest);
            String status = getStatusofFetch(fetchRequest, dataHandler);
            if (status.equals(FetchStatus.NotInitiated.name())) {
                dataHandler.createFetchInitRecord(ds, f, fetchRequest, exportRequest, mtdName); //09Feb2025 Bindu - pass mtd name
                createJsonData(f, exportRequest, fetchRequest);
                FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Data Fetch Initiated",
                        "Data fetch is Initiated, Please wait while data is generated and Downloaded","","");
                return gson.toJson(response);
            } else if (status.equals(FetchStatus.Success.name())) {
                tblServerDataFetchLog data = dataHandler.getDataofFile(ds, fetchRequest.requestId, fetchRequest.userId, fetchRequest.schemaName, exportRequest);
                FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Success",
                        "Data downloading, Please wait...",data.getFileSize(),data.getFileName());
                return gson.toJson(response);
            } else if (status.equals(FetchStatus.Failed.name())) {
                FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Failed",
                        "Server Error","","");
                return gson.toJson(response);
            } else {
                FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Data Fetch not yet Available",
                        "Data which was requested is not yet available to download, Please wait...","","");
                return gson.toJson(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Server Error", "Server is facing problem, Please try again later","","");
            return gson.toJson(response);
        }
    }

    private ExportRequest convertFetchtoExport(FetchRequest fetchRequest) {
        ExportRequest exportRequest = new ExportRequest();
        exportRequest.mcId = fetchRequest.bc_pcId;
        exportRequest.lastSyncDate = fetchRequest.lastSyncDate;
        exportRequest.schemaName = fetchRequest.schemaName;
        exportRequest.userType = fetchRequest.userType;
        return exportRequest;
    }

    private String getStatusofFetch(FetchRequest fetchRequest, DataHandler handler) {
        Connection conn = null;
        try {
            SentiaPooledDataSource ds = handler.getDataSource();
            FullSchemaRepository fullSchemaRepository = new FullSchemaRepository(ds);
            ExportRequest export = convertFetchtoExport(fetchRequest);
            conn = ds.getConnection(fetchRequest.schemaName);
            String clientDBName = fullSchemaRepository.getOrThrow(export, conn);
            conn.close();
            conn = ds.getConnection(clientDBName);
            String sql = "Select * from tblServerDatafetchlog where requestId = ? and userId = ? ;";
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setString(1, fetchRequest.requestId);
            stat.setString(2, fetchRequest.userId);
            ResultSet result = stat.executeQuery();
            if (result.next()) {
                String status = result.getString("status");
                if (!status.isEmpty()) {
                    if (status.equalsIgnoreCase(FetchStatus.Initiated.name())) {
                        conn.close();
                        return FetchStatus.Initiated.name();
                    } else if (status.equalsIgnoreCase(FetchStatus.Success.name())) {
                        conn.close();
                        return FetchStatus.Success.name();
                    } else if (status.equalsIgnoreCase(FetchStatus.Failed.name())) {
                        conn.close();
                        return FetchStatus.Failed.name();
                    }
                }
            }
            conn.close();
            return FetchStatus.NotInitiated.name();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            return FetchStatus.NotInitiated.name();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void createJsonData(File f, ExportRequest r, FetchRequest fetchRequest) {
        CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(() -> {
            try (JsonWriter jw = new JsonWriter(new FileWriter(f))) {
                System.out.printf("Fetch Started for %s%n", fetchRequest.requestId);
                repository.export(jw, r,appConfig);
                jw.flush();
                System.out.printf("Fetch Finished for %s%n", fetchRequest.requestId);
                MessageDigest md = MessageDigest.getInstance("MD5");
                FileInputStream fis = new FileInputStream(f);

                byte[] byteArray = new byte[1024];
                int bytesCount = 0;
                while ((bytesCount = fis.read(byteArray)) != -1) {
                    md.update(byteArray, 0, bytesCount);
                }
                ;
                fis.close();
                byte[] digest = md.digest();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < digest.length; i++) {
                    sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                }
                String checkSum = sb.toString();
                long sizeLong = Files.size(f.toPath());
                sizeLong = sizeLong / 1024;
                String size = String.valueOf(sizeLong);
                updateFetchLog(r, fetchRequest, FetchStatus.Success, "", size, checkSum);
            } catch (Exception e) {
                ExportResponse res = gson.fromJson(e.getMessage(), ExportResponse.class);
                LOGGER.error(String.format("Export error for request %s", r.toString()), e);
                try {
                    updateFetchLog(r, fetchRequest, FetchStatus.Failed, res.detail, "0", "");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    LOGGER.error(String.format("Export error for request %s", r.toString()), e);
                }
            }
            return false;
        });
    }

    private void updateFetchLog(ExportRequest exportRequest, FetchRequest fetchRequest, FetchStatus status, String ex,String size,String checkSum) throws Exception {
        SentiaPooledDataSource ds = repository.getDataSource();
        Connection conn = null;
        try {
            conn = ds.getConnection(fetchRequest.schemaName);
            String clientDbName = repository.getOrThrow(exportRequest, conn);
            conn.close();
            conn = ds.getConnection(clientDbName);
            String sql = "Select * from tblServerDatafetchlog where requestId =? and userId = ?;";
            PreparedStatement statment = conn.prepareStatement(sql);
            statment.setString(1, fetchRequest.requestId);
            statment.setString(2, fetchRequest.userId);
            ResultSet res = statment.executeQuery();
            if (res.next()) {
                tblServerDataFetchLog log = new tblServerDataFetchLog();
                log.setId(res.getString(1));
                log.setUserId(res.getString(2));
                log.setUserType(res.getString(3));
                log.setRequestId(res.getString(4));
                log.setRequestSentAt(res.getString(5));
                log.setRequestReceivedAt(res.getString(6));
                log.setRestMethodName(res.getString(7));
                log.setFileName(res.getString(8));
                log.setFileSize(res.getString(9));
                log.setCheckSum(res.getString(10));
                log.setFileGeneratedAt(res.getString(11));
                log.setFileCompletedAt(res.getString(12));
                log.setStatus(res.getString(13));
                log.setComments(res.getString(14));

                DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:s");
                LocalDateTime time = LocalDateTime.now();
                String currTime = sdf.format(time);
                log.setFileCompletedAt(currTime);
                log.setStatus(status.name());
                log.setComments(ex);
                log.setFileSize(size);
                log.setCheckSum(checkSum);

                String updateSql = "Update tblServerDatafetchlog set fileCompletedAt = ?, status = ?, comments =?, fileSize = ?,checkSum = ?  where requestId = ?;";
                PreparedStatement updateStat = conn.prepareStatement(updateSql);
                updateStat.setString(1, log.getFileCompletedAt());
                updateStat.setString(2, log.getStatus());
                updateStat.setString(3, log.getComments());
                updateStat.setString(4, log.getFileSize());
                updateStat.setString(5, log.getCheckSum());
                updateStat.setString(6, log.getRequestId());
                updateStat.executeUpdate();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), "Connection closure throw error");
            e.printStackTrace();
            throw e;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException exx) {
                    LOGGER.error(exx.getMessage(), "Connection closure throw error");
                    exx.printStackTrace();
                    throw exx;
                }
            }
        }
    }

    public String getDownloadFile(FetchRequest fetchReq) throws Exception {
        String fileName = "";
        SentiaPooledDataSource ds = repository.getDataSource();
        ExportRequest exportReq = convertFetchtoExport(fetchReq);
        Connection con = ds.getConnection(fetchReq.schemaName);
        String clientDb = repository.getOrThrow(exportReq, con);
        con.close();
        con = ds.getConnection(clientDb);
        String sql = "Select * from tblServerDatafetchlog where requestId = ?;";
        PreparedStatement stat = con.prepareStatement(sql);
        stat.setString(1, fetchReq.requestId);
        ResultSet res = stat.executeQuery();
        if (res.next()) {
            if (res.getString("status").equals(FetchStatus.Success.name())) {
                fileName = res.getString("fileName");
            }
        }
        con.close();
        System.out.printf("Downloading file for %s%n", fetchReq.requestId);
        File file = new File(appConfig.exportDirectory, fileName);
        return file.getPath();
    }

//    07sep2024 Arpitha
public String exportNewRellocation(FetchRequest r,String mtdName) throws IOException {
    File f = new File(appConfig.exportDirectory,
            r.userType+"_fetch_" + LocalDateTime.now().format(df1) + "_" + r.userId + ".json");
    return downloadInitiatedReallocation(f, r, mtdName); //09Feb2025 Bindu - Pass Mtdname var
}
    private String downloadInitiatedReallocation(File f, FetchRequest fetchRequest, String mtdName) {
        try {
            SentiaPooledDataSource ds = repository.getDataSource();
            DataHandler dataHandler = new DataHandler(ds);
            ExportRequest exportRequest = convertFetchtoExportReallocation(fetchRequest);
            String status = getStatusofFetchReallocation(fetchRequest, dataHandler);
            if (status.equals(FetchStatus.NotInitiated.name())) {
                dataHandler.createFetchInitRecord(ds, f, fetchRequest, exportRequest,mtdName);//09Feb2025 Bindu - Pass Mtdname
                createJsonDataReallocation(f, exportRequest, fetchRequest);
                FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Data Fetch Initiated",
                        "Data fetch is Initiated, Please wait while data is generated and Downloaded","","");
                return gson.toJson(response);
            } else if (status.equals(FetchStatus.Success.name())) {
                tblServerDataFetchLog data = dataHandler.getDataofFile(ds, fetchRequest.requestId, fetchRequest.userId, fetchRequest.schemaName, exportRequest);
                FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Success",
                        "Data downloading, Please wait...",data.getFileSize(),data.getFileName());
                return gson.toJson(response);
            } else if (status.equals(FetchStatus.Failed.name())) {
                FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Failed",
                        "Server Error","","");
                return gson.toJson(response);
            } else {
                FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Data Fetch not yet Available",
                        "Data which was requested is not yet available to download, Please wait...","","");
                return gson.toJson(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            FetchResponse response = new FetchResponse(fetchRequest.bc_pcId, fetchRequest.requestId, "Server Error", "Server is facing problem, Please try again later","","");
            return gson.toJson(response);
        }
    }

    private ExportRequest convertFetchtoExportReallocation(FetchRequest fetchRequest) {
        ExportRequest exportRequest = new ExportRequest();
        exportRequest.mcId = fetchRequest.bc_pcId;
        exportRequest.lastSyncDate = fetchRequest.lastSyncDate;
        exportRequest.schemaName = fetchRequest.schemaName; //todo - Bindu - 09Feb2025 - check if master or client
        exportRequest.userType = fetchRequest.userType;
        return exportRequest;
    }

    private String getStatusofFetchReallocation(FetchRequest fetchRequest, DataHandler handler) {
        Connection conn = null;
        try {
            SentiaPooledDataSource ds = handler.getDataSource();
            FullSchemaRepository fullSchemaRepository = new FullSchemaRepository(ds);
            ExportRequest export = convertFetchtoExportReallocation(fetchRequest);
            conn = ds.getConnection(fetchRequest.schemaName);
            String clientDBName = fullSchemaRepository.getOrThrow(export, conn);
            conn.close();
            conn = ds.getConnection(clientDBName);
            String sql = "Select * from tblServerDatafetchlog where requestId = ? and userId = ? ;";
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setString(1, fetchRequest.requestId);
            stat.setString(2, fetchRequest.userId);
            ResultSet result = stat.executeQuery();
            if (result.next()) {
                String status = result.getString("status");
                if (!status.isEmpty()) {
                    if (status.equalsIgnoreCase(FetchStatus.Initiated.name())) {
                        conn.close();
                        return FetchStatus.Initiated.name();
                    } else if (status.equalsIgnoreCase(FetchStatus.Success.name())) {
                        conn.close();
                        return FetchStatus.Success.name();
                    } else if (status.equalsIgnoreCase(FetchStatus.Failed.name())) {
                        conn.close();
                        return FetchStatus.Failed.name();
                    }
                }
            }
            conn.close();
            return FetchStatus.NotInitiated.name();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            return FetchStatus.NotInitiated.name();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    private void createJsonDataReallocation(File f, ExportRequest r, FetchRequest fetchRequest) {
        CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(() -> {
            try (JsonWriter jw = new JsonWriter(new FileWriter(f))) {
                System.out.printf("Fetch Started for %s%n", fetchRequest.requestId);
                repository.exportReallocation(jw, r,appConfig);
                jw.flush();
                System.out.printf("Fetch Finished for %s%n", fetchRequest.requestId);
                MessageDigest md = MessageDigest.getInstance("MD5");
                FileInputStream fis = new FileInputStream(f);

                byte[] byteArray = new byte[1024];
                int bytesCount = 0;
                while ((bytesCount = fis.read(byteArray)) != -1) {
                    md.update(byteArray, 0, bytesCount);
                }
                ;
                fis.close();
                byte[] digest = md.digest();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < digest.length; i++) {
                    sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                }
                String checkSum = sb.toString();
                long sizeLong = Files.size(f.toPath());
                sizeLong = sizeLong / 1024;
                String size = String.valueOf(sizeLong);
                updateFetchLog(r, fetchRequest, FetchStatus.Success, "", size, checkSum);
            } catch (Exception e) {
                ExportResponse res = gson.fromJson(e.getMessage(), ExportResponse.class);
                LOGGER.error(String.format("Export error for request %s", r.toString()), e);
                try {
                    updateFetchLog(r, fetchRequest, FetchStatus.Failed, res.detail, "0", "");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    LOGGER.error(String.format("Export error for request %s", r.toString()), e);
                }
            }
            return false;
        });
    }



}
