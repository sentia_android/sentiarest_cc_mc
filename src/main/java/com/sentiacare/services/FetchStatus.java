package com.sentiacare.services;

public enum FetchStatus {
    Initiated,
    Success,
    Failed,
    NotInitiated
}
