package com.sentiacare.domain;

import com.sentiacare.entities.FileStatus;
import com.sentiacare.entities.MessageStatus;

public class SentiatendException extends Throwable {

    public String message;
    public FileStatus fileStatus;
    public MessageStatus messageStatus;
    public String messageForLogs;

    public SentiatendException(String message) {
        super(message);
    }

    public SentiatendException(String message, Throwable t) {
        super(message, t);
    }

    public SentiatendException(String message, FileStatus fileStatus, MessageStatus messageStatus, String messageForLogs) {
        super(message);
        this.message = message;
        this.fileStatus = fileStatus;
        this.messageStatus = messageStatus;
        this.messageForLogs = messageForLogs;
    }

    public SentiatendException(String message, FileStatus fileStatus, MessageStatus messageStatus, String messageForLogs, Throwable e) {
        super(e);
        this.message = message;
        this.fileStatus = fileStatus;
        this.messageStatus = messageStatus;
        this.messageForLogs = messageForLogs;
    }
}
