package com.sentiacare.domain;

import com.sentiacare.dtos.synclogDots.ReqIDStatus;
import com.sentiacare.entities.AshaWorker;
import com.sentiacare.entities.Nurse;
import com.sentiacare.repositories.DataHandler;
import com.sentiacare.repositories.SentiaPooledDataSource;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class BusinessHandler {
    private final DataHandler dataHandler;
    private final SentiaPooledDataSource ds;

    public BusinessHandler(DataHandler dataHandler, SentiaPooledDataSource instance) {
        this.dataHandler = dataHandler;
        this.ds = instance;
    }

    public void updateLogToTranTable(String RequestId, String dbname, String TranId, String TranAction, String Comment, String LogDateTime, String FileStatus, String DBStatus, String msgstatus) {
        String sql = "Insert into trantable (requestid,transid,action,comment,logdatetime,filestatus,dbstatus,msgstatus) values ('" + RequestId + "','" + TranId + "','" + TranAction + "','" + Comment + "','" + LogDateTime + "','" + FileStatus + "','" + DBStatus + "','" + msgstatus + "')";
        dataHandler.putData(sql, dbname);
    }

    public String getRegistrationData(String thayiCardId, String aadhaarId, String dbName) {
        String query = "SELECT * FROM `anc fetch` where (ThayiCardNo!='' and ThayiCardNo='" + thayiCardId + "') or (PhoneNumber!='' and PhoneNumber='" + aadhaarId + "')";
        return dataHandler.getData(query, dbName);
    }

    public String checkForRequestIdExists(String requestId, String dbname) {
        String query = "Select count(1) as requestcount, requestid from trantable where requestid ='" + requestId +
                "' and slno = (select max(slno) from trantable where requestid ='" + requestId + "')";
        return dataHandler.getData(query, dbname);
    }

    public String getDataFromServer(String requestId, String dbname) {
        String sql = "Select * from trantable where requestid in ('" + requestId + "' )";
        return dataHandler.getData(sql, dbname);
    }


    public String getUserAndSettingsData(String emailid, String password, String userLastUpdatedDate,
                                         String settingsLastUpdatedDate, boolean isLoginCheck,
                                         String mainDbname, String deviceId) {
        String response = "";
        Nurse n = dataHandler.getNurse(emailid, password, mainDbname);
        if (n == null) {
            return "<RegisteredMain>0</RegisteredMain>";
        } else {
            response += "<RegisteredMain>1</RegisteredMain>";
            if (isLoginCheck) {
                if (n.validated == 0) {
                    String sqlstmt = "Update tblinstusers Set isValidated = 1 Where UserMobileNumber = '" + emailid + "' And Password = '" + password + "' ";
                    dataHandler.putData(sqlstmt, mainDbname);

                    response = response + "<ValidatedMain>1</ValidatedMain>";

                    response = response + "<UserMobileNumber>" + n.UserMobileNumber + "</UserMobileNumber>";
                    response = response + "<Password>" + n.password + "</Password>";
                    response = response + "<UserId>" + n.userId + "</UserId>";

                    String userSql = "Select * from tblinstusers where EmailId = '" + emailid + "' and Password = '" + password + "'";
                    if (userLastUpdatedDate != null && !userLastUpdatedDate.isEmpty())
                        userSql = userSql + " And LastModifiedDate > '" + userLastUpdatedDate + "'";

                    response = response + dataHandler.getData(userSql, mainDbname);

                    String settingSql = "Select * from tblinstitutiondetails where EmailId = '" + emailid + "' ";
                    if (settingsLastUpdatedDate != null && !settingsLastUpdatedDate.isEmpty())
                        settingSql = settingSql + " And LastModifiedDate > '" + settingsLastUpdatedDate + "'";

                    response = response + dataHandler.getData(settingSql, mainDbname);
                } else {
                    String returnFilename = "newsqlite" + n.userId + ".db";
                    if (n.deviceId != null && n.deviceId.equals(deviceId)) {
                        StringBuilder responseBuilder = new StringBuilder();
                        if (Files.exists(Paths.get("/home/inetpub/wwwroot/iStendWebSTest/UserDb/" + returnFilename))) {
                            String sqlstmt = "Update tbluser Set DeviceId = '' Where EmailId = '" + emailid + "' And Password = '" + password + "' ";
                            dataHandler.putData(sqlstmt, mainDbname);
                            responseBuilder.append("<ReadyToDownload>true</ReadyToDownload>")
                                    .append("<ReturnFileTag>" + returnFilename + "</ReturnFileTag>")
                                    .append("<InfoTag>Database is created and Ready to download</InfoTag>");
                        } else {
                            responseBuilder.append("<ReadyToDownload>false</ReadyToDownload>")
                                    .append("<InfoTag>Failed to create lost database</InfoTag>");
                        }
                        return "<ValidatedMain>0</ValidatedMain> <Losttab>" + false + "</Losttab> <InvalidDb>false</InvalidDb> " + responseBuilder.toString() + " " + response;
                    }
                }
            } else {
                response = response + "<ValidatedMain>" + n.validated + "</ValidatedMain>";
                String userSql = "Select emailId,facility,facility_name,taluk,state,validated,lastmodifieddate,district from tbluser where Emailid = '" + emailid + "' ";
                if (userLastUpdatedDate != null && !userLastUpdatedDate.isEmpty())
                    userSql = userSql + " And LastModifiedDate >= '" + userLastUpdatedDate + "'";

                response = response + dataHandler.getData(userSql, mainDbname);

                String settingSql = "Select * from tblSettings where Emailid = '" + emailid + "' ";
                if (settingsLastUpdatedDate != null && !settingsLastUpdatedDate.isEmpty())
                    settingSql = settingSql + " And LastModifiedDate > '" + settingsLastUpdatedDate + "'";

                response = response + dataHandler.getData(settingSql, mainDbname);
            }
        }
        return response;
    }

    public String getColorCodedValuesData(String lastadddate, String databasename) {
        String sqlstmt = "Select * from tblColorCodedValues where lastmodifieddate > '" + lastadddate + "'";
        return dataHandler.getData(sqlstmt, databasename);
    }


    public String getDeliveryInfo(String thayiID, String dbname, boolean womanId) {
        if (womanId) {
            String sqlCheckInDel = "SELECT * FROM fetchdelinfoold where WomanId = '" + thayiID + "'";
            String sqlDelData = dataHandler.getData(sqlCheckInDel, dbname);
            if (sqlDelData.length() > 0) {
                String postDelWoman = "select wasmt.WomanId, wasmt.aiwwomanincompl,"
                        + "  wasmt.aiwdateofdiagnosis,wasmt.aiwtimeofdiagnosis,wasmt.aiwdiagnosedby,cond.codesc,"
                        + " group_concat(comp.cdcomplname) as complicaitons,"
                        + " wasmt.aiwwomanadmitted,"
                        + " wasmt.aiwnameofthestaff,wasmt.aiwtransferredtoward,wasmt.aiwcomments,"
                        + " wasmt.aiwwomanadmitteddate,wasmt.aiwwomanadmittedtime,wasmt.aiwwomanipdno, wasmt.aiwmordatetime"
                        + " from tblassessmentinfowoman wasmt join tblconditions cond on wasmt.aiwwomanincond = cond.cocode"
                        + " left join tblcomplicationsdetails comp on wasmt.WomanId = comp.WomanId and wasmt.aiwgroupcode = comp.cdgroupcode where wasmt.aiwgroupcode = 'PDAM'"
                        + " and wasmt.WomanId ='" + thayiID + "' group by wasmt.WomanId";

                String postDelNb = "SELECT nbw.WomanId,nbw.ainnewbornno,"
                        + "  nbw.ainnewbornincompl,nbw.aindateofdiagnosis,nbw.aintimeofdiagnosis,nbw.aindiagnosedby,nbw.ainnewbornincond,cond.codesc,"
                        + " group_concat(comp.cdcomplname) as complicaitons ,nbw.ainnameofthestaff,nbw.ainncomments, nbw.ainmordatetime"
                        + " FROM scidlmskhptrpt.tblassessmentinfonewborn nbw join tblconditions cond on nbw.ainnewbornincond = cond.cocode"
                        + " left join tblcomplicationsdetails comp on nbw.WomanId = comp.WomanId and nbw.aingroupcode = comp.cdgroupcode"
                        + "  where nbw.WomanId ='" + thayiID + "' and aingroupcode = 'PDAN' group by nbw.WomanId,nbw.ainnewbornno";

                return sqlDelData + "" + dataHandler.getData(postDelWoman, dbname) + "" + dataHandler.getData(postDelNb, dbname);
            }
            return "Delivery details not found for ID";
        } else {
            List<String> duplicateRegistrations = dataHandler.getDuplicateRegistrations(thayiID, dbname);
            if (duplicateRegistrations.isEmpty()) {
                return "No registrations found";
            } else if (duplicateRegistrations.size() == 1) {
                String sqlCheckInDel = "SELECT * FROM fetchdelinfoold WHERE WomanId = '" + duplicateRegistrations.get(0) + "'";
                String sqlDeliveryData = dataHandler.getData(sqlCheckInDel, dbname);
                if (sqlDeliveryData.length() > 0) {
                    String postDelWoman = "select wasmt.WomanId, wasmt.aiwwomanincompl,"
                            + "  wasmt.aiwdateofdiagnosis,wasmt.aiwtimeofdiagnosis,wasmt.aiwdiagnosedby,cond.codesc,"
                            + " group_concat(comp.cdcomplname) as complicaitons,"
                            + " wasmt.aiwwomanadmitted,"
                            + " wasmt.aiwnameofthestaff,wasmt.aiwtransferredtoward,wasmt.aiwcomments,"
                            + " wasmt.aiwwomanadmitteddate,wasmt.aiwwomanadmittedtime,wasmt.aiwwomanipdno, wasmt.aiwmordatetime"
                            + " from tblassessmentinfowoman wasmt join tblconditions cond on wasmt.aiwwomanincond = cond.cocode"
                            + " left join tblcomplicationsdetails comp on wasmt.WomanId = comp.WomanId and wasmt.aiwgroupcode = comp.cdgroupcode where wasmt.aiwgroupcode = 'PDAM'"
                            + " and wasmt.WomanId ='" + duplicateRegistrations.get(0) + "' group by wasmt.WomanId";

                    String postDelNb = "SELECT nbw.WomanId,nbw.ainnewbornno,"
                            + "  nbw.ainnewbornincompl,nbw.aindateofdiagnosis,nbw.aintimeofdiagnosis,nbw.aindiagnosedby,nbw.ainnewbornincond,cond.codesc,"
                            + " group_concat(comp.cdcomplname) as complicaitons ,nbw.ainnameofthestaff,nbw.ainncomments, nbw.ainmordatetime"
                            + " FROM scidlmskhptrpt.tblassessmentinfonewborn nbw join tblconditions cond on nbw.ainnewbornincond = cond.cocode"
                            + " left join tblcomplicationsdetails comp on nbw.WomanId = comp.WomanId and nbw.aingroupcode = comp.cdgroupcode"
                            + "  where nbw.WomanId ='" + duplicateRegistrations.get(0) + "' and aingroupcode = 'PDAN' group by nbw.WomanId,nbw.ainnewbornno";

                    return sqlDeliveryData + "" + dataHandler.getData(postDelWoman, dbname) + "" + dataHandler.getData(postDelNb, dbname);
                } else {
                    return "Delivery details not found for ID";
                }
            } else {
//                List<String> duplicates = new ArrayList<>(5);
//                for (int i=0;i<duplicateRegistrations.size();i++){
//                    String sqlCheckDupl = "SELECT"
//                            + " r.WomanId,"
//                            + " r.regwName,"
//                            + " r.regAge,"
//                            + " d.delDate,"
//                            + " FUNC_DISPLAYVALUE(d.delplaceofdel, ',', 'delplaceofdel') as delplaceofdel,"
//                            + " r.regPhone,"
//                            + " r.regHusbandName"
//                            + " FROM tblregistration r"
//                            + " join tbldeliveryinfo d on r.WomanId = d.WomanId"
//                            + " WHERE r.regThayiCard = '" +
//                            thayiID + "' or r.regAadhar ='" + thayiID + "' or" +
//                            " regPhone = '" + thayiID + "'";
//                    duplicates.add(dataHandler.getData(sqlCheckDupl, dbname)) ;
//                }
//                   return String.valueOf(duplicates);

                String sqlCheckDupl = "SELECT"
                        + " r.WomanId,"
                        + " r.regwName,"
                        + " r.regAge,"
                        + " d.delDate,"
                        + " FUNC_DISPLAYVALUE(d.delplaceofdel, ',', 'delplaceofdel') as delplaceofdel,"
                        + " r.regPhone,"
                        + " r.regHusbandName"
                        + " FROM tblregistration r"
                        + " join tbldeliveryinfo d on r.WomanId = d.WomanId"
                        + " WHERE r.regThayiCard = '" +
                        thayiID + "' or r.regAadhar ='" + thayiID + "' or" +
                        " regPhone = '" + thayiID + "'";
                return "Duplicates" + dataHandler.getData(sqlCheckDupl, dbname);


            }
        }
    }

    public String getHRPComplicationActionData(String dbname, String lastModifiedDate, String userId) {
        String query = "Select * from tblhrpcomplicationmgmt where UserId = '" + userId + "' and ServerUpdatedDate='" + lastModifiedDate + "'";
        return dataHandler.getData(query, dbname);
    }

    public String getValidateUserForLoginData(String mobileNumber, String password, String dbname) {
        Boolean validated = false, lostTab = false;
        String readyToDownload = "";
        String returnFileTag = "";
        String infoTag = "";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String currentDate = dtf.format(now);

        boolean institutionsExist = dataHandler.institutionsExist(dbname);
        if (institutionsExist) {
            AshaWorker ashaWorker = dataHandler.get(mobileNumber, password, dbname);
            if (ashaWorker == null) {
                return "<maintag> <Validated>" + false + "</Validated> <Losttab>" + false + "</Losttab> <InvalidDb>false</InvalidDb> <InfoTag>User is not registered!</InfoTag> </maintag>";
            } else {
                if (!ashaWorker.password) { //7-6-2022 Ramesh to check the password is valid or not
                    return "<maintag> <Validated>" + ashaWorker.isValidated + "</Validated> <Losttab>" + ashaWorker.isTabLost + "</Losttab> <InvalidDb>false</InvalidDb> <InfoTag>Password is Incorrect</InfoTag> </maintag>";
                }
                if (ashaWorker.isDeactivated) {
                    return "<maintag> <Validated>" + ashaWorker.isValidated + "</Validated> <Losttab>" + ashaWorker.isTabLost + "</Losttab> <InvalidDb>false</InvalidDb> <InfoTag>User is Deactivated</InfoTag> </maintag>";
                } else {
                    if (ashaWorker.isTabLost) {
                        infoTag = "<InfoTag>Lost data is retrieved and creating new database to download</InfoTag>";
                        String returnFileName = "newsqlite" + mobileNumber + ".db";
                        // TODO Under which condition should this infoTag value be used?
                        // infoTag = "<InfoTag>Failed to retrieve and create database to download</InfoTag>";
                        return "<maintag> <Validated>" + ashaWorker.isValidated + "</Validated> <Losttab>" + true + "</Losttab> <InvalidDb>false</InvalidDb> " + readyToDownload + " " + returnFileName + " " + infoTag + " </maintag>";
                    }
                    if (ashaWorker.isValidated) { // Losttab element value is set to hardcoded false because it is true in the previous condition
                        return "<maintag> <Validated>" + false + "</Validated> <Losttab>" + false + "</Losttab> " +
                                "<InvalidDb>false</InvalidDb> <InfoTag>Sign-in not allowed. User already validated/signed-in from another device.</InfoTag> </maintag>";
                    } else {
                        String query = "Update tblinstusers Set isvalidated = 1 , UserValidatedDatetime = '" + currentDate + "' Where UserMobileNumber = '" + mobileNumber + "' And Password = '" + password + "' ";
                        dataHandler.putData(query, dbname);
                        return "<maintag> <Validated>" + true + "</Validated> <UserIdreturned>" + ashaWorker.UserId + "</UserIdreturned> <Losttab>" + ashaWorker.isTabLost + "</Losttab> <InvalidDb>false</InvalidDb> <InfoTag>First time validation is true</InfoTag> </maintag>";
                    }
                }
            }
        } else {
            return "<maintag> <Validated>" + validated + "</Validated> <Losttab>" + lostTab
                    + "</Losttab> <InvalidDb>false</InvalidDb> <InfoTag>Institution is Deactivated</InfoTag> </maintag>";
        }
    }

    public String getSettingsForUser(String userId, String lastUpdatedTime, String dbname, String mobileNumber) {
        String sqlstmtuser = "Select * from tblinstusers where userId='" + userId + "'";
        if (lastUpdatedTime.length() > 0) {
            sqlstmtuser = sqlstmtuser + " and dateupdated > '" + lastUpdatedTime + "'";
        }

        String sqlstmtinst = "Select * from tblinstitutiondetails where  InstituteId In (Select InstituteId "
                + " from tblinstusers "
                + " where UserMobileNumber ='" + mobileNumber + "') ";
        if (lastUpdatedTime.length() > 0) {
            sqlstmtinst = sqlstmtinst + "  and dateupdated >'" + lastUpdatedTime + "'";
        }

        return dataHandler.getData(sqlstmtuser, dbname) + "" + dataHandler.getData(sqlstmtinst, dbname);

    }

    public String getVillagesForUser(String userId, String lastUpdatedTime, String dbname) { //Ramesh 30-Sep-2021, getting facility irt userid
        String sqlstmt = " Select * from tblfacilitydetails ";
        if (lastUpdatedTime.length() > 0) {
            sqlstmt = sqlstmt + " where dateupdated>'" + lastUpdatedTime + "'";
        }

        return dataHandler.getData(sqlstmt, dbname);
    }

    //08Apr2021 Bindu - Get referral facility details
    public String getReferralcenters(String userId, String lastUpdatedTime, String dbname) {
        String sqlstmt = " Select * from tblreferralFacility ";
        if (lastUpdatedTime.length() > 0) {
            sqlstmt = sqlstmt + " where dateupdated>'" + lastUpdatedTime + "'";
        }

        return dataHandler.getData(sqlstmt, dbname);
    }

    public String getCaseMgmt(String userId,String lastServerDAte, String dbname){
        String sqlstmt = " Select * from tblCaseMgmt where  MMUserId = '"+userId+"';";
        if (lastServerDAte.length() > 0) {
            sqlstmt = sqlstmt + " where dateupdated>'" + lastServerDAte + "'";
        }
        String sqlRegDate = "Select WomanId,regRiskFactorsByCC,regOtherRiskByCC,regCCConfirmRisk from tblregisteredwomen";

        return dataHandler.getData(sqlstmt, dbname) +""+dataHandler.getData(sqlRegDate,dbname);
    }

    public String getUserDetailsForUser(String mobileNumber, String databaseName,String lastUpdatedTime) {
        String sqlstmt = "Select distinct tblinstusers.* from tblinstusers left join tblfacilitydetails on tblinstusers.UserId = tblfacilitydetails.UserId where tblfacilitydetails.MC_Id ='"+mobileNumber+"'" +
                "union " +
                "Select distinct tblinstusers.* from tblinstusers left join tblfacilitydetails on tblinstusers.UserId = tblfacilitydetails.CC_SysUID where tblfacilitydetails.MC_Id ='"+mobileNumber+"'";

        if (lastUpdatedTime.length() > 0) {
            sqlstmt = sqlstmt + " where dateupdated>'" + lastUpdatedTime + "'";
        }

        return dataHandler.getData(sqlstmt, databaseName);
    }

    public String getAchfData(String dbname,String userId, String userRole) {
        String sql = "";

        if(userRole!=null && userRole.trim().length()>0) {
            if(userRole.equalsIgnoreCase("BC"))
            sql = " Select * from tblinstusers where userId IN (Select distinct currentUserId from tblfacilitydetails where CC_SysUID ='" + userId + "') ";
            else if(userRole.equalsIgnoreCase("PC"))
                sql = " Select * from tblinstusers where userId IN (Select distinct currentUserId from tblfacilitydetails where MC_SysUID ='" + userId + "') ";

        }
        return dataHandler.getData(sql,dbname);
    }

    //8-Sep-2022 Ramesh
    public ReqIDStatus getSyncLog_MySQL(String reqId, String userId, Connection con) {
        try{
            String sql = "Select * from tblsyncservicelog_mysql where userId = ? and requestId = ? and (transStatus != 'Initiated' or transStatus != null) limit 1;";
            PreparedStatement prep = con.prepareStatement(sql);
            prep.setString(1,userId);
            prep.setString(2,reqId);
            ResultSet result = prep.executeQuery();
            if(result.next()){
                String status = result.getString("Status");
                String transStatus = result.getString("transStatus");
                String comments = result.getString("comments");
                return new ReqIDStatus(reqId,status,transStatus,comments);
            }
            result.close();
            return null;
        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }

    public DataHandler getdataHandle() {
        return dataHandler;
    }

    //    09Feb2025 Arpitha
    public String checkAndGetReallocationData(String villageId, String dbName) {

        String sqlStmt = "select * from tblreallocationsynctrack where (reallocationStatus is null or reallocationStatus!='Y') and facilityId IN ("+villageId+")";
        return dataHandler.getData(sqlStmt,dbName);

    }
}
