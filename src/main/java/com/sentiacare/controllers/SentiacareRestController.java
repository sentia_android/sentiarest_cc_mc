package com.sentiacare.controllers;

import com.google.gson.Gson;
import com.sentiacare.domain.BusinessHandler;
import com.sentiacare.dtos.CustomException;
import com.sentiacare.repositories.DataHandler;
import com.sentiacare.repositories.FullSchemaRepository;
import com.sentiacare.repositories.SentiaPooledDataSource;
import com.sentiacare.services.*;
import com.sentiacare.utils.AppConfig;
import com.sentiacare.utils.XMLUtils;
import io.javalin.Javalin;
import io.javalin.core.JavalinServer;
import io.javalin.http.ExceptionHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.client.HttpResponse;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConnectionFactory;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SentiacareRestController {

    private static final Logger LOGGER = LogManager.getLogger(SentiacareRestController.class.getName());
    private AppConfig appConfig;

    public SentiacareRestController() {
    }

    /*11Jun2021 Gururaja
     * Configure idleTimeout for all connection factories
     * Default is 30 seconds in current version of Jetty embedded in Javalin,
     * now it is raised to 5 minutes (5 minutes was the value in earlier Jetty versions)
     * */
    public static void main(String[] args) {
        try {
            saveProcessID();
            SentiacareRestController service = new SentiacareRestController();
            service.init();

            SentiaPooledDataSource dataSource = SentiaPooledDataSource.getInstance(service.appConfig);

            DataHandler dataHandler = new DataHandler(dataSource);

            dataHandler.initDOMToStringTransformer();

            BusinessHandler businessHandler = new BusinessHandler(dataHandler, dataSource);

            AppService appService = new AppService(
                    service.appConfig,
                    businessHandler,
                    dataSource);

            Exporter exporter = new Exporter(new FullSchemaRepository(dataSource), service.appConfig);

            Javalin app = Javalin.create(config -> {
                config.maxRequestSize = 50_00_000L;
                config.enableDevLogging();
            });
            app.start(service.appConfig.javalinPort);
            JavalinServer server = app.server();
            assert server != null;
            Connector[] connectors = server.server().getConnectors();
            connectors[0].getConnectionFactories().forEach(x -> {
                HttpConnectionFactory f = (HttpConnectionFactory) x;
                f.getHttpConfiguration().setIdleTimeout(75000);
            });
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(service.appConfig.WebServerXSDFilePath));

            app.post("/ValidUserForDownLoad", context -> context.result(appService.ValidUserForDownLoad(context.body())));
            app.post("/GetHRPComplicationActionData", ctx -> ctx.result(appService.getHRPComplicationActionData(ctx.body())));
            app.post("/GetSettingsForUser", ctx -> ctx.result(appService.getSettingsForUser(ctx.body())));
            app.post("/GetVillagesForUser", ctx -> ctx.result(appService.getVillagesForUser(ctx.body())));
            app.post("/getReferralcenters", ctx -> ctx.result(appService.getReferralcenters(ctx.body())));
            app.post("/logs", ctx -> ctx.result(appService.saveLogFile(ctx)));
            app.get("/health", ctx -> ctx.result(""));
            app.post("/fetchfromserver", ctx -> ctx.result(appService.getFetchfromServer(ctx)));
            app.post("/export", ctx -> ctx.result(exporter.export(ctx.body())));
            app.post("/getUserforMCUser", ctx -> ctx.result(appService.getUserDetailsforMCUser(ctx.body())));
            app.post("/exportMansiMitra", ctx -> ctx.result(exporter.exportMansiMitra(ctx.body())));
            app.post("/imagedownload", ctx -> {
                ctx.result(appService.imageDownload(ctx));
            });

            //Ramesh 20-Jun-2022
            FetchNewVersion fetchNewVersion = new FetchNewVersion(exporter);
            app.post("/fetchV2", ctx -> ctx.result(fetchNewVersion.checktheRequest(ctx.body(),"fetchV2"))); //09Feb2025 Bindu - pass mtd name
            app.post("/downloadDataFile", ctx -> ctx.result(fetchNewVersion.downloadDataFetch(ctx.body())));
            app.post("/getAchfData", ctx -> ctx.result(appService.getACHFData(ctx.body())));
            app.post("/tablost", ctx -> ctx.result(appService.tablost2(ctx.body(), dataHandler)));
            app.post("/downloadTablostData", ctx -> ctx.result(appService.downloadDataFetch(ctx.body(), dataHandler)));
            app.post("/syncLogStatus", ctx -> ctx.result(appService.getSyncLogStatus(ctx)));
            app.get("/hikariStatus", ctx -> ctx.result(appService.getSyncLogStatus(ctx)));

            app.post("/fetchV3", ctx -> ctx.result(fetchNewVersion.checktheRequestReallocation(ctx.body(),"fetchV3"))); //09Feb2025 Bindu - pass mtd name





            app.post("/activityCalenderJsonFileNames", ctx -> ctx.result(appService.getStoredActivityCalenderFilesNames().toString()));//Rakesh 06June2=23


            app.post("/activityCalenderJsonDownload", ctx -> {
                ctx.result(appService.activityCalenderJsonFileDownload(ctx));
            });//Rakesh 06June2=23

            app.post("/activityCalenderJson", ctx -> ctx.result(appService.uploadJsonFile(ctx))); //Rakesh 24July2023

            app.post("/activityCalenderJsonDownloadPc", ctx -> {
                ctx.result(appService.activityCalenderJsonFileDownloadPc(ctx));
            });//Rakesh 25July2023

//            09Feb2025 Arpitha - check and fetch reallocation pending data
            app.post("/checkAndGetReallocationDetails", ctx -> {
                ctx.result(appService.checkAndGetReallocationDetails(ctx));
            });

            Runnable runnableTask = () -> {
                try {
                    new ScheduleFileProcessing(dataSource, service.appConfig).doFileProcessing();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };
            Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(runnableTask, 1, 30, TimeUnit.SECONDS);
            System.out.print("Please Ctrl+C to Stop the Service..");
        } catch (IOException | TransformerConfigurationException e) {
            System.out.println(e.getMessage());
            LOGGER.error("One of three exception", e);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            LOGGER.error("Thrown by server configuration", e);
        }
    }

    public void init() throws IOException {
        appConfig = new AppConfig();
        appConfig.loadAppConfig();
    }

    private static void saveProcessID() {
        String vmName = ManagementFactory.getRuntimeMXBean().getName();
        int p = vmName.indexOf("@");
        String pid = vmName.substring(0, p);

        try (FileWriter fw = new FileWriter("info.txt")) {
            fw.write("pid=" + pid);
            fw.write("\n");
            CodeSource codeSource = SentiacareRestController.class.getProtectionDomain().getCodeSource();
            File jarFile = new File(codeSource.getLocation().toURI());
            fw.write("jarPath=" + jarFile.getPath());
            fw.write("\n");
            fw.flush();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
