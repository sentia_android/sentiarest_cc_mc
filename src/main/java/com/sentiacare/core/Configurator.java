package com.sentiacare.core;

import com.sentiacare.utils.AppConfig;

import java.io.InputStream;
import java.util.Properties;

public class Configurator {

    public static void configure(AppConfig appConfig) throws Exception {
        String propertyFileName = "appconfig.properties";
        InputStream is = Configurator.class.getClassLoader().getResourceAsStream(propertyFileName);
        Properties properties = new Properties();
        if(is == null) {
            throw new Exception("Could not create InputStream for properties file");
        }
        properties.load(is);
        appConfig.javalinPort = Integer.parseInt(properties.getProperty("javalinPort"));
//        appConfig.exportDirectory = properties.getProperty("exportDirectory");
    }
}
